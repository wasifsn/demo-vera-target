import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';
import { withTranslate } from 'react-redux-multilingual';
import reactCSS from 'reactcss';

export default class NavBarCategory extends Component {
	constructor(props) {
		super(props);

		this.state = {
			navClose: { right: '0px' },
			type: this.props.type,
			category: 'tops'
		};
	}

	setCategory = (e) => {
		let category = e.target.innerText.toLowerCase();
		console.log(category);
		this.props.handleCategoryChange(category);
		this.setState({ category: category });
	};

	changeStyle1 = () => {
		if (this.props.type === 'gender') {
			if (this.state.gender === 'women') {
				return 'linkSelectedGender';
			} else {
				return 'linkNotSelectedGender';
			}
		} else if (this.props.type === 'category') {
			if (this.state.category === 'tops') {
				return 'linkSelected';
			} else {
				return 'linkNotSelected';
			}
		}
	};

	changeStyle2 = () => {
		if (this.props.type === 'gender') {
			if (this.state.category === 'men') {
				return 'linkSelectedGender';
			} else {
				return 'linkNotSelectedGender';
			}
		} else if (this.props.type === 'category') {
			if (this.state.category === 'bottoms') {
				return 'linkSelected';
			} else {
				return 'linkNotSelected';
			}
		}
	};
	render() {
		let type = this.props.type;

		return (
			<div>
				<div>
					<div id='mainnav'>
						<ul className='nav-menu' style={this.state.navClose}>
							{/* WOMEN,TOPS SECTION */}
							<li>
								<div
									onClick={type === 'category' ? this.setCategory : this.setGender}
									className={`nav-link gender-link  ${this.changeStyle1()}`}>
									{type === 'gender' ? 'WOMEN' : 'Tops'}
								</div>
							</li>
							{/* MEN,BOTTOMS SECTION */}
							<li>
								<div
									onClick={type === 'category' ? this.setCategory : this.setGender}
									className={`nav-link gender-link ${this.changeStyle2()}`}>
									{type === 'gender' ? 'MEN' : 'Bottoms'}
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		);
	}
}
