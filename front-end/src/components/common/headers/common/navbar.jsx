import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';
import { withTranslate } from 'react-redux-multilingual';
import reactCSS from 'reactcss';

export default class NavBarGender extends Component {
	constructor(props) {
		super(props);

		this.state = {
			navClose: { right: '0px' },
			type: this.props.type,
			gender: 'women'
		};
	}

	setCategory = (e) => {
		let category = e.target.innerText.toLowerCase();
		// console.log(category);
		this.props.handleCategoryChange(category);
		this.setState({ category: category });
	};

	setGender = (e) => {
		let gender = e.target.innerText.toLowerCase();
		// console.log(gender);
		this.props.handleGenderChange(gender);
		this.setState({ gender: gender });
	};

	changeStyle1 = () => {
		if (this.props.type === 'gender') {
			if (this.state.gender === 'women') {
				return 'linkSelectedGender';
			} else {
				return 'linkNotSelectedGender';
			}
		} else if (this.props.type === 'category') {
			if (this.state.category === 'tops') {
				return 'linkSelected';
			} else {
				return 'linkNotSelected';
			}
			// this.state.category === 'tops' ? styles.linkSelected : styles.linkNotSelected
		}
	};

	changeStyle2 = () => {
		if (this.props.type === 'gender') {
			if (this.state.category === 'men') {
				return 'linkSelectedGender';
			} else {
				return 'linkNotSelectedGender';
			}
		} else if (this.props.type === 'category') {
			if (this.state.category === 'bottoms') {
				return 'linkSelected';
			} else {
				return 'linkNotSelected';
			}
			// this.state.category === 'tops' ? styles.linkSelected : styles.linkNotSelected
		}
	};
	render() {
		let type = this.props.type;

		return (
			<div>
				<div>
					<div id='mainnav'>
						<ul className='nav-menu' style={this.state.navClose}>
							{/* WOMEN,TOPS SECTION */}
							<li>
								<div
									onClick={type === 'category' ? this.setCategory : this.setGender}
									className={`nav-link gender-link ${this.state.gender === 'women'
										? 'linkSelectedGender'
										: 'linkNotSelectedGender'} `}>
									{type === 'gender' ? 'WOMEN' : 'TOPS'}
								</div>
							</li>
							{/* MEN,BOTTOMS SECTION */}
							<li>
								<div
									onClick={type === 'category' ? this.setCategory : this.setGender}
									className={`nav-link gender-link ${this.state.gender === 'men'
										? 'linkSelectedGender'
										: 'linkNotSelectedGender'}`}>
									{type === 'gender' ? 'MEN' : 'BOTTOMS'}
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		);
	}
}
