import React, { Component } from 'react';
// import { Link, NavLink } from 'react-router-dom';
import { IntlActions } from 'react-redux-multilingual';
import Pace from 'react-pace-progress';
import reactCSS from 'reactcss';

// Import custom components
import store from '../../../store';
// import NavBar from './common/navbar';
// import SideBar from './common/sidebar';
// import CartContainer from './../../../containers/CartContainer';
// import TopBar from './common/topbar';
// import LogoImage from './common/logo';
import { changeCurrency } from '../../../actions';
import { connect } from 'react-redux';

class HeaderOne extends Component {
	constructor(props) {
		super(props);

		this.state = {
			isLoading: false
		};
	}
	/*=====================
         Pre loader
         ==========================*/
	componentDidMount() {
		setTimeout(function() {
			document.querySelector('.loader-wrapper').style = 'display: none';
			let redundant = document.querySelector('.mobile-fix-option');
			if (redundant) redundant.remove();
		}, 2000);

		this.setState({ open: true });
	}

	// componentWillMount() {
	// 	window.addEventListener('scroll', this.handleScroll);
	// }
	// componentWillUnmount() {
	// 	window.removeEventListener('scroll', this.handleScroll);
	// }

	// handleScroll = () => {
	// 	let number = window.pageXOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
	// 	if (number >= 300) {
	// 		if (window.innerWidth < 576) {
	// 			document.getElementById('sticky').classList.remove('fixed');
	// 		} else document.getElementById('sticky').classList.add('fixed');
	// 	} else {
	// 		document.getElementById('sticky').classList.remove('fixed');
	// 	}
	// };

	changeLanguage(lang) {
		store.dispatch(IntlActions.setLocale(lang));
	}

	openNav() {
		var openmyslide = document.getElementById('mySidenav');
		if (openmyslide) {
			openmyslide.classList.add('open-side');
		}
	}
	openSearch() {
		document.getElementById('search-overlay').style.display = 'block';
	}

	closeSearch() {
		document.getElementById('search-overlay').style.display = 'none';
	}

	load = () => {
		this.setState({ isLoading: true });
		fetch().then(() => {
			// deal with data fetched
			this.setState({ isLoading: false });
		});
	};

	render() {
		const styles = reactCSS({
			default: {
				'main-menu': { height: '3.5vw' },
				headerOne: {
					// boxSizing: 'border-box',
					backgroundColor: 'black'
					// height: '3rem'
				},
				poweredBy: {
					color: 'white'
					// maxWidth: '50%',
					// height: 'auto',
					// margin: 'auto'
				},
				croppedImg: {
					maxWidth: '50%',
					height: 'auto',
					margin: 'auto'
				}
			}
		});
		return (
			<div>
				<header style={styles.headerOne}>
					{this.state.isLoading ? <Pace color='#27ae60' /> : null}
					{/*Top Header Component*/}
					<div className='container-fluid'>
						<div className='row'>
							<div className='col-sm-12'>
								<div className='powered-by' style={styles['main-menu']}>
									<div className='menu-right pull-right'>
										<div>
											<div className='icon-nav'>
												<ul>
													<li className='onhover-div'>
														<div>
															<span style={styles.poweredBy}>Powered by Vera</span>
														</div>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</header>

				{/* <div id='search-overlay' className='search-overlay'>
					<div>
						<span className='closebtn' onClick={this.closeSearch} title='Close Overlay'>
							×
						</span>
						<div className='overlay-content'>
							<div className='container'>
								<div className='row'>
									<div className='col-xl-12'>
										<form>
											<div className='form-group'>
												<input
													type='text'
													className='form-control'
													id='exampleInputPassword1'
													placeholder='Search a Product'
												/>
											</div>
											<button type='submit' className='btn btn-primary'>
												<i className='fa fa-search' />
											</button>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div> */}
			</div>
		);
	}
}

export default connect(null, { changeCurrency })(HeaderOne);
