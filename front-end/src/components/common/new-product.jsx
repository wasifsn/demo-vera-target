import React, { Component } from 'react';
import Slider from 'react-slick';
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import axios from 'axios';
import Loader from 'react-loader-spinner';
import { getBestSeller } from '../../services';

let a = [
	{
		id: '52934563',
		title:
			'Womenssss&#39;s Plus Size Star Wars: The Last Jedi Criss Cross V-Neck Short Sleeve Graphic T-Shirt (Juniors&#39;) - Black 2X',
		class: 'TOPS',
		category: 'Tee shirts',
		redirect_url:
			'https://www.target.com/p/women-39-s-plus-size-star-wars-the-last-jedi-criss-cross-v-neck-short-sleeve-graphic-t-shirt-juniors-39-black-2x/-/A-52934563',
		upc: '490060605367',
		__order: 0,
		image_url: 'https://target.scene7.com/is/image/Target/GUEST_9914d966-60fe-401a-a60c-8a3a6da16727'
	},
	{
		id: '51980436',
		title: "Women's Embroidered Tie Neck Top Navy S - Cliche",
		class: 'TOPS',
		category: 'Blouses',
		redirect_url: 'https://www.target.com/p/women-s-embroidered-tie-neck-top-navy-s-cliche/-/A-51980436',
		upc: '849871032372',
		__order: 1,
		image_url: 'https://target.scene7.com/is/image/Target/51980436'
	},
	{
		id: '17136836',
		title: "Women's Plus Size Cardigan Black Print 2X - Mossimo Supply Co.&#153;",
		class: 'TOPS',
		category: 'Cardigans',
		redirect_url:
			'https://www.target.com/p/women-s-plus-size-cardigan-black-print-2x-mossimo-supply-co-153/-/A-17136836',
		upc: '490210830182',
		__order: 2,
		image_url: 'https://target.scene7.com/is/image/Target/17136836'
	},
	{
		id: '52300578',
		title: "Women's Plus Size Striped V-Neck T-Shirt - Ava & Viv&#153; Black 3X",
		class: 'TOPS',
		category: 'Tee shirts',
		redirect_url:
			'https://www.target.com/p/women-s-plus-size-striped-v-neck-t-shirt-ava-viv-153-black-3x/-/A-52300578',
		upc: '490210152970',
		__order: 3,
		image_url: 'https://target.scene7.com/is/image/Target/GUEST_b2dea12c-4191-400c-be7c-af6925dd2fc4'
	},
	{
		id: '52300576',
		title: "Women's Plus Size V-Neck T-Shirt - Ava & Viv&#153; Opulent Red 1X",
		class: 'TOPS',
		category: 'Tee shirts',
		redirect_url:
			'https://www.target.com/p/women-s-plus-size-v-neck-t-shirt-ava-viv-153-opulent-red-1x/-/A-52300576',
		upc: '490210153113',
		__order: 4,
		image_url: 'https://target.scene7.com/is/image/Target/52300576'
	},
	{
		id: '51114351',
		title: "Women's Woven Popover Zipper Neck Black S - Zac and Rachel",
		class: 'TOPS',
		category: 'Blouses',
		redirect_url: 'https://www.target.com/p/women-s-woven-popover-zipper-neck-black-s-zac-and-rachel/-/A-51114351',
		upc: '884148534609',
		__order: 5,
		image_url: 'https://target.scene7.com/is/image/Target/51114351'
	},
	{
		id: '53183269',
		title: "Women's Plus Size Printed V-Neck Short Sleeve T-Shirt - Ava & Viv&#153; Cream 3X",
		class: 'TOPS',
		category: 'Tee shirts',
		redirect_url:
			'https://www.target.com/p/women-s-plus-size-printed-v-neck-short-sleeve-t-shirt-ava-viv-153-cream-3x/-/A-53183269',
		upc: '490210155933',
		__order: 6,
		image_url: 'https://target.scene7.com/is/image/Target/53183269'
	},
	{
		id: '75559140',
		title: "Women's Short Sleeve Woven T-Shirt with Hardware - Lux II - Purple M",
		class: 'TOPS',
		category: 'Blouses',
		redirect_url:
			'https://www.target.com/p/women-s-short-sleeve-woven-t-shirt-with-hardware-lux-ii-purple-m/-/A-75559140',
		upc: '191906004916',
		__order: 7,
		image_url: 'https://target.scene7.com/is/image/Target/GUEST_7fb408ae-3b11-4a70-8298-1de1affece86'
	}
];

class NewProduct extends Component {
	constructor(props) {
		super(props);
		this.state = {
			item: this.props.item,
			itemsArr: [],
			mainImg: this.props.mainImg,
			similar_items: []
		};
		this.loadSimilarData();
		this.processSimilarItems(this.state.item.image_url);
	}

	loadSimilarData() {
		var data = {
			category: this.state.item.category,
			image_url: this.state.item.image_url
		};
		try {
			axios
				.post('imagesearch', {
					params: {
						source: 'SAMPLE',
						category: this.state.item.category,
						nitems: 8,
						vendor: 'Walmart'
					},

					data: data
				})
				.then((response) => {
					if (!response.data.error && Array.isArray(response.data.data) && response.data.data.length)
						this.setState({ itemsArr: [ ...response.data.data ] });
					else this.setState({ itemsArr: a });
				});
		} catch (err) {
			console.log(err);
			console.log(err.response);
			this.props.history.push('/');
		}
	}
	mashupImages(e, index) {
		e.persist();
		console.log(e.target.src);
		console.log('CALLED', this.props.bottoms, this.props.tops);
		let newIndex = index;
		if (this.props.title === 'SIMILAR') {
			let image_src = `http://127.0.0.1:8000/try?top=${this.props.tops[newIndex].id}&bottom=${1}&target=1`;
			this.props.handleMashUpData(image_src);
		} else if (this.props.title === 'PAIR WITH') {
			let image_src = `http://127.0.0.1:8000/try?top=${1}&bottom=${this.props.bottoms[newIndex].id}&target=1`;
			this.props.handleMashUpData(image_src);
		}
	}
	renderSlider = (arrays) => {
		const settings = {
			infinite: true,
			speed: 500,
			slidesToShow: 3,
			slidesToScroll: 3,
			// adaptiveHeight: true,
			responsive: [
				{
					breakpoint: 1024,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 3,
						infinite: true
					}
				},
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 2,
						initialSlide: 2
					}
				},
				{
					breakpoint: 480,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
				}
			]
		};
		let category;
		if (this.props.title === 'PAIR WITH') {
			category = 'bottoms';
		} else {
			category = 'tops';
		}
		return (
			<Slider {...settings}>
				{arrays.length > 0 ? (
					this.props[category].map((products, index) => (
						<div key={index}>
							{/* {products.map((product, i) => ( */}
							<div
								className='media newproduct-slider-inner-wraper image-outerwrapper lightgrey-border p-1 mt-2 ml-auto mr-auto'
								key={index}>
								<div
									onClick={(e) => this.mashupImages(e, index)}
									to={`${process.env.PUBLIC_URL}/left-sidebar/product/`}>
									<img
										className='img-fluid'
										src={`http://127.0.0.1:8000${products.img_url}`}
										alt=''
									/>
								</div>
								{/* <Link to={`${process.env.PUBLIC_URL}/left-sidebar/product/`}>
									<img className='img-fluid' src={products.img_url} alt='' />
								</Link> */}
								{/* <div className='media-body align-self-center'>
									<Link to={`${process.env.PUBLIC_URL}/left-sidebar/product/${product.id}`}>
										<h6>{product.title}</h6>
									</Link>
								</div> */}
							</div>
							{/* ))} */}
						</div>
					))
				) : (
					this.renderLoader()
				)}
			</Slider>
		);
	};

	renderLoader = () => {
		return (
			<div style={{ display: 'flex', justifyContent: 'center' }}>
				<Loader
					type='Puff'
					color='red'
					height={100}
					width={100}
					// timeout={3000} //3 secs
				/>
			</div>
		);
	};

	async processSimilarItems(image) {
		// setTimeout(async () => {
		// console.log('image from processSimilaritems', image);
		axios.defaults.headers.post['Content-Type'] = 'application/json';
		let response = await axios.post(
			`https://apis.vera.ml/p1/imagesearch?nitems=9&gender=${this.state.item
				.gender}&mode=center&vendor=social_crops`,
			{
				image_url: image
			}
		);
		let data;
		console.log('response from new_product', response);
		// if (response.data.similar_data.n0.similar_items.length > 0) {
		// 	console.log('similarItems', response);
		// 	let data = response.data.similar_data.n0.similar_items.map((el) => {
		// 		return el;
		// 	});
		// 	this.setState({ similar_items: data });
		// } else {
		// 	this.setState({ similar_items: null });
		// }
		if (Array.isArray(response.data.similar_data)) {
			let redirect = window.confirm('SImilar Image: press OK to Redirect to Main Page');
			if (!redirect) {
				this.props.history.push('/');
			} else {
				this.props.history.push('/');
				// this.props.history.push(document.url);
			}
		} else if (response.request.status === 200 && response.data.similar_data.n0.similar_items.length > 0) {
			for (let i in response.data.similar_data) {
				if (response.data.similar_data[i].meta.category === this.props.category) {
					data = response.data.similar_data[i].similar_items;
				} else {
					data = response.data.similar_data.n0.similar_items;
				}
			}
			// console.log('similarItems', response);
			// console.log('sort similar data', data);
			let sortedData = data.map((el) => {
				return el;
			});
			this.setState({ similar_items: sortedData });
		} else {
			this.setState({ similar_items: null });
		}
		// }, 500);
	}
	// renderSimilarRecommendation = ()=>{
	// 	if(this.props.title === 'PAIR WITH'){

	// 	} else-if(this.props.title === 'SIMILAR'){

	// 	}
	// }
	render() {
		const { mainImg } = this.props;
		const { itemsArr, symbol, similar_items } = this.state;
		const items = [ ...itemsArr ];
		const newItems = [ ...similar_items ];
		console.log('newItems', newItems);
		var arrays = newItems;
		// while (newItems.length > 0) {
		// 	arrays.push(newItems.splice(0, 1));
		// }
		// console.log(arrays);
		return (
			<div className='theme-card'>
				<div className='pl-xl-4 pl-lg-3 ml-lg-0 m-xl-1'>
					<h4 className='p-2 mt-2'>{this.props.title}</h4>
				</div>

				{this.props.title === 'PAIR WITH' ? (
					this.renderSlider(this.props.recommendation_new_data)
				) : this.props.title === 'SIMILAR' && arrays.length ? (
					this.renderSlider(arrays)
				) : (
					this.renderLoader()
				)}
				{/* {this.props.title === 'SIMILAR' && arrays.length ? this.renderSlider(arrays) : this.renderLoader()} */}
			</div>
		);
	}
}

// function mapStateToProps(state) {
//   return {
//     items: getBestSeller(state.data.products),
//     symbol: state.data.symbol
//   };
// }

export default NewProduct;
