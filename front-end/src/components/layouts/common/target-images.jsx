import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Modal from 'react-responsive-modal';
import reactCSS from 'reactcss';
import Popup from 'reactjs-popup';
import axios from 'axios';

export default class TargetItem extends Component {
	static defaultProps = {
		top: 1,
		bottom: 1
	};
	constructor(props) {
		super(props);
		this.state = {
			top: 1,
			bottom: 1,
			target: 1,
			targets: [],
			mainImg: null
		};
		this.getTargets();
	}
	getTargets = async () => {
		try {
			let response = await axios.get(`http://127.0.0.1:8000/targets`);
			console.log('newTargetsResponse', response);
			this.setState({ targets: response.data });
		} catch (err) {
			console.log(err);
			console.log(err.response);
			this.setState({ targets: null });
		}
	};

	// http://127.0.0.1:8000/targets\static\data\targets\2\image.jpg
	// http://127.0.0.1:8000\static\data\tops\1\image.jpg
	get_tryon = async () => {
		console.log();
		let self = this;
		let response = await axios.get(`http://127.0.0.1:8000/try?top=${1}&bottom=${2}&target=${2}`);
		console.log(response);
		console.log(typeof response);
		this.setState({ mainImg: response.data });
		// axios
		// 	.get(`http://127.0.0.1:8000/try?top=${1}&bottom=${2}&target=${2}`)
		// 	.then((response) => (self.setState({ image: img })).catch(err){
		//         console.log(err);
		//     }
	};

	render() {
		const targetImgs = this.state.targets.map((target, index) => {
			return (
				<div style={{ textAlign: 'center', height: '0.2rem' }}>
					<img
						onClick={this.get_tryon}
						className='img-fluid'
						src={`http://127.0.0.1:8000${target.img_url}`}
						alt=''
					/>
				</div>
			);
		});
		return (
			<div style={{ display: 'flex', flexFlow: 'row wrap' }}>
				{targetImgs}
				{/* <div>{this.state.mainImg}</div> */}
				<div>
					<img
						src={`http://127.0.0.1:8000/try?top=${this.props.top}&bottom=${this.props.bottom}&target=${this
							.state.target}`}
						alt=''
					/>
				</div>
			</div>
		);
	}
}
