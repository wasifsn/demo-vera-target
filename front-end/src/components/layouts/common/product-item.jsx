import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Modal from 'react-responsive-modal';
import reactCSS from 'reactcss';
import Popup from 'reactjs-popup';
import TargetImages from './target-images';
class ProductItem extends Component {
	constructor(props) {
		super(props);
		this.state = {
			top: 1,
			bottom: 1,
			open: false,
			stock: 'InStock',
			quantity: 1,
			image: '',
			isFull: false
		};
	}

	onClickHandle(img) {
		this.setState({ image: img });
	}
	onOpenModal = () => {
		this.setState({ open: true });
	};

	onCloseModal = () => {
		this.setState({ open: false });
	};

	minusQty = () => {
		if (this.state.quantity > 1) {
			this.setState({ stock: 'InStock', quantity: this.state.quantity - 1 });
		}
	};

	plusQty = () => {
		if (this.props.product.stock >= this.state.quantity) {
			this.setState({ quantity: this.state.quantity + 1 });
		} else {
			this.setState({ stock: 'Out of Stock !' });
		}
	};
	changeQty = (e) => {
		this.setState({ quantity: parseInt(e.target.value) });
	};

	goFull = () => {
		this.setState({ isFull: true });
	};
	render() {
		const styles = reactCSS({
			default: {
				'product-outerwrapper': {
					boxSizing: 'border-box',
					display: 'flex',
					// flexDirection: 'column',
					alignItems: 'center',
					justifyContent: 'center',
					margin: '0 3rem',
					height: '16rem',
					// width: '15rem',
					// padding: '0 1rem',
					border: '1px solid lightgrey'
				},
				'image-outerwrapper': {
					transition: ' all 0.5s ease-out',
					display: 'flex',
					alignItems: 'center',
					justifyContent: 'center',
					width: '12.5rem',
					padding: '1rem'
					// margin: '0 1rem'
				},
				'product-detail-outerwrapper': {
					margin: '0 0 1rem 0'
				}
			}
		});

		const { product, symbol, onAddToCartClicked, onAddToWishlistClicked, onAddToCompareClicked } = this.props;

		let RatingStars = [];
		for (var i = 0; i < product.rating; i++) {
			RatingStars.push(<i className='fa fa-star' key={i} />);
		}
		if (this.props.type == 'top collection') {
			return (
				<div className='product-box ' style={styles['product-outerwrapper']}>
					<div className='image-outerwrapper' style={styles['image-outerwrapper']}>
						{/* <div className='lable-block'>
							{product.new == true ? <span className='lable3'>new</span> : ''}
							{product.sale == true ? <span className='lable4'>on sale</span> : ''}
						</div> */}
						<div className='front'>
							<Popup
								onClick={this.goFull}
								trigger={
									<img src={`http://127.0.0.1:8000${product.img_url}`} className='img-fluid' alt='' />
								}
								modal
								position='center'
								closeOnDocumentClick>
								<TargetImages top={this.state.top} bottom={this.state.bottom} />
								{/* <div>WASIF</div> */}
								{/* <img src={product.img_url} className='img-fluid' alt='' /> */}
							</Popup>

							{/* <Link
								category={this.props.category}
								to={`${process.env.PUBLIC_URL}/left-sidebar/product/${product._id}/${this.props
									.category}`}>
								<img src={product.img_url} className='img-fluid' alt='' />
							</Link> */}
						</div>
					</div>
					{/* <div className='product-detail' style={styles['product-detail-outerwrapper']}>
						<div>
							<Link to={`${process.env.PUBLIC_URL}/left-sidebar/product/${product._id}`}>
								<h6>{product.title ? product.title.substring(0, 30) : ''}</h6>
							</Link>
							<h4>
								{symbol}
								{product.meta.price}
							</h4>
						</div>
					</div> */}
				</div>
			);
		} else {
			return (
				<div className='product-box'>
					<div className='img-wrapper'>
						<div className='lable-block'>
							{product.new == true ? <span className='lable3'>new</span> : ''}
							{product.sale == true ? <span className='lable4'>on sale</span> : ''}
						</div>
						<div className='front'>
							<Link to={`${process.env.PUBLIC_URL}/left-sidebar/product/${product.id}`}>
								<img
									src={`${product.variants
										? this.state.image ? this.state.image : product.variants[0].images
										: product.pictures[0]}`}
									className='img-fluid'
									alt=''
								/>
							</Link>
						</div>
						<div className='cart-info cart-wrap'>
							<button title='Add to cart' onClick={onAddToCartClicked}>
								<i className='fa fa-shopping-cart' aria-hidden='true' />
							</button>
							<a href='javascript:void(0)' title='Add to Wishlist' onClick={onAddToWishlistClicked}>
								<i className='fa fa-heart' aria-hidden='true' />
							</a>
							<a
								href='javascript:void(0)'
								data-toggle='modal'
								data-target='#quick-view'
								title='Quick View'
								onClick={this.onOpenModal}>
								<i className='fa fa-search' aria-hidden='true' />
							</a>
							<Link
								to={`${process.env.PUBLIC_URL}/compare`}
								title='Compare'
								onClick={onAddToCompareClicked}>
								<i className='fa fa-refresh' aria-hidden='true' />
							</Link>
						</div>
						{product.variants ? (
							<ul className='product-thumb-list'>
								{product.variants.map((vari, i) => (
									<li
										className={`grid_thumb_img ${vari.images === this.state.image ? 'active' : ''}`}
										key={i}>
										<a href='javascript:void(0)' title='Add to Wishlist'>
											<img
												src={`${vari.images}`}
												onClick={() => this.onClickHandle(vari.images)}
											/>
										</a>
									</li>
								))}
							</ul>
						) : (
							''
						)}
					</div>
					<div className='product-detail'>
						<div>
							<div className='rating'>{RatingStars}</div>
							<Link to={`${process.env.PUBLIC_URL}/left-sidebar/product/${product.id}`}>
								<h6>{product.name}</h6>
							</Link>
							<h4>
								{symbol}
								{product.price - product.price * product.discount / 100}
								<del>
									<span className='money'>
										{symbol}
										{product.price}
									</span>
								</del>
							</h4>
							{product.variants ? (
								<ul className='color-variant'>
									{product.variants.map((vari, i) => {
										return (
											<li
												className={vari.color}
												key={i}
												title={vari.color}
												onClick={() => this.onClickHandle(vari.images)}
											/>
										);
									})}
								</ul>
							) : (
								''
							)}
						</div>
					</div>
				</div>
			);
		}
	}
}

export default ProductItem;
