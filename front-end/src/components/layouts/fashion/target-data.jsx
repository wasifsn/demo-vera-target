import React, { useState, useEffect } from 'react';

// import targetImgs from '../../../assets/json/target_dummy.json';
import targetImgs from '../../../assets/json/target.json';

const TargetData = ({ history }) => {
	let [ targetData, setTargetData ] = useState([]);
	let [ pageNo, setPageNo ] = useState(0);
	let [ size, setSize ] = useState(100);
	let [ limit, setlimit ] = useState(10);
	let sessionPageNum = sessionStorage.getItem('pageNum') ? sessionStorage.getItem('pageNum') : 0;
	let [ globalData, setGlobalData ] = useState(targetImgs);

	let scroll = () => {
		let el = document.querySelector('.powered-by');
		if (el) el.scrollIntoView(true);
	};

	const ViewProduct = (item) => {
		history.push({
			pathname: `/productdetail/${item[Object.keys(item)[1]].post.urlId}`,
			data: item,
			mainUrl: item[Object.keys(item)[1]].post.imageUrl,
			id: item[Object.keys(item)[1]].post.urlId
		});
	};

	// let renderData = (start, end) => {
	// 	let [ similar_tops, similar_bottoms ] = [ [], [] ];
	// 	let finalImgs = [];
	// 	let endT = 0;
	// 	for (let i of targetData.splice(0, 10)) {
	// 		let temp = Object.keys(i);
	// 		let key = temp[1];

	// 		if (i[key].similar_tops) similar_tops = i[key].similar_tops;
	// 		if (i[key].similar_bottoms) similar_bottoms = i[key].similar_bottoms;
	// 	}
	// 	finalImgs = [ ...similar_tops, ...similar_bottoms ];
	// 	console.log(finalImgs);
	// };

	let renderData = targetData.map((el, i) => {
		return (
			<div
				onClick={() => {
					ViewProduct(el);
				}}
				className='TS-images-section-outer-wrapper'>
				{/* {console.log(el[Object.keys(el)[1]].post.imageUrl)}
				{console.log(el)} */}
				<img className='TS-images' src={el[Object.keys(el)[1]].post.imageUrl} key={i} />
			</div>
		);
	});

	useEffect(() => {
		if (targetData.length === 0) {
			if (!sessionStorage.getItem('pageNum')) {
				sessionStorage.setItem('pageNum', 0);
				setTargetData(targetImgs.slice(0, size));
			} else {
				let newPage = +sessionStorage.getItem('pageNum');
				let start = newPage * size,
					end = start + size;
				setTargetData(targetImgs.slice(start, end));
			}
		}
	});

	const prev = () => {
		if (+sessionStorage.getItem('pageNum') !== 0) {
			let prevPage = +sessionStorage.getItem('pageNum');
			sessionStorage.setItem('pageNum', prevPage - 1);
			let newPage = sessionStorage.getItem('pageNum');
			// setPageNo(pageNo - 1);
			let start = newPage * size,
				end = start + size;
			setTargetData(globalData.slice(start, end));
			// console.log('START: ', start, 'END: ', end);
			scroll();
			// console.log(sessionStorage);
		}
	};

	const next = () => {
		let prevPage = +sessionStorage.getItem('pageNum');
		// setPageNo(pageNo + 1);
		sessionStorage.setItem('pageNum', +prevPage + 1);
		let newPage = +sessionStorage.getItem('pageNum');
		let start = newPage * size,
			end = start + size;
		setTargetData(globalData.slice(start, end));
		// console.log('START: ', start, 'END: ', end);
		scroll();
		// console.log(sessionStorage);
	};

	return (
		<div>
			<div className='flex wrap '>
				{/* {targetData.map((el, i) => (
					<div
						onClick={() => {
							ViewProduct(el);
						}}
						className='TS-images-section-outer-wrapper'> */}
				{/* {console.log(el[Object.keys(el)[1]].post.imageUrl)}
						{console.log(el)} */}
				{/* <img className='TS-images' src={el[Object.keys(el)[1]].post.imageUrl} key={i} />
					</div>
				))} */}
				{renderData}
			</div>
			<div>
				{console.log(sessionPageNum)}
				<button
					style={sessionPageNum === 0 ? { opacity: '0.5' } : { opacity: '1' }}
					id='PS-inner-container-btn-prev'
					className='PS-inner-container-btn m-3 p-2 pl-3 pr-3'
					onClick={(e) => {
						e.persist();
						prev();
					}}>
					Prev
				</button>
				<button
					className='PS-inner-container-btn m-3 p-2 pl-3 pr-3'
					onClick={(e) => {
						e.persist();
						next();
					}}>
					Next
				</button>
			</div>
		</div>
	);
};

export default TargetData;
