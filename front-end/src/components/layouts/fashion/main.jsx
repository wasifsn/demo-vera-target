import React, { useState, useEffect } from 'react';

// import images data from assets
// import { womens_bottoms } from '../../../assets/json/womens_new_design_bottoms';
// import { womens_tops } from '../../../assets/json/womens_new_design_tops';
// import { mens_tops } from '../../../assets/json/mens_new_design_tops';
// import { mens_bottoms } from '../../../assets/json/mens_new_design_bottoms';

// import { womens_bottomNew } from '../../../assets/json/api_data_womens';
// import { womens_topsNew } from '../../../assets/json/api_data_womens';
// import { women_targetsNew } from '../../../assets/json/api_data_womens';

import SimpleBarReact from 'simplebar-react';
import HeaderOne from '../../common/headers/header-one';
import 'simplebar/src/simplebar.css';
import '../../common/index.scss';
import axios from 'axios';

// Import custom components
import NavBar from '../../common/headers/common/navbar.jsx';
import NavBarCategory from '../../common/headers/common/navbar-category.jsx';
import MashedImage from '../../products/mashed-image';
import AccordionMain from './accordion-main';
import ProductDetails from './product-detail';
import TrendSpotter from './trendSpotter';

export default function Fashion({ history }) {
	useEffect(() => {
		fetchWomenData();
		document.getElementById('color').setAttribute('href', `#`);
	}, []);

	const [ gender, setgender ] = useState('women');
	const [ category, setcategory ] = useState('tops');
	const [ pairWithID, setpairWithID ] = useState('');
	const [ categoryID, setcategoryID ] = useState('');
	const [ womens_topsNew, setWomensTop ] = useState([]);
	const [ womens_bottomNew, setWomensBottom ] = useState([]);
	const [ women_targetsNew, setWomensTarget ] = useState([]);
	const [ mens_topsNew, setMensTop ] = useState([]);
	const [ mens_bottomNew, setMensBottom ] = useState([]);
	const [ men_targetsNew, setMensTarget ] = useState([]);
	// const [ mashedSrc, setMashedSrc ] = useState('');
	const Authorization = {
		auth: {
			username: 'vera',
			password: 'swordfish'
		}
	};

	const onIdSelect = (id) => {
		setpairWithID(id);
	};

	const onIdCategory = (id) => {
		setcategoryID(id);
	};

	const setGender = (data) => {
		console.log(data);
		if (data === 'women') {
			if (category === 'tops') {
				setpairWithID(womens_topsNew[0].pairwith[0].id);
				setcategoryID(womens_topsNew[0].id);
			} else {
				setpairWithID(womens_bottomNew[0].pairwith[0].id);
				setcategoryID(womens_bottomNew[0].id);
			}
		} else {
			if (category === 'tops') {
				setpairWithID(mens_topsNew[0].pairwith[0].id);
				setcategoryID(mens_topsNew[0].id);
			} else {
				setpairWithID(mens_bottomNew[0].pairwith[0].id);
				setcategoryID(mens_bottomNew[0].id);
			}
		}
		setgender(data);
	};

	const setCategory = (data) => {
		if (gender === 'women') {
			if (data === 'tops') {
				setpairWithID(womens_topsNew[0].pairwith[0].id);
				setcategoryID(womens_topsNew[0].id);
			} else {
				setpairWithID(womens_bottomNew[0].pairwith[0].id);
				setcategoryID(womens_bottomNew[0].id);
			}
		} else {
			if (data === 'tops') {
				setpairWithID(mens_topsNew[0].pairwith[0].id);
				setcategoryID(mens_topsNew[0].id);
			} else {
				setpairWithID(mens_bottomNew[0].pairwith[0].id);
				setcategoryID(mens_bottomNew[0].id);
			}
		}
		setcategory(data);
	};

	const getWomenTops = async () => {
		// axios.defaults.headers.common['auth'] = auth;
		return axios.get(`/women/tops`, Authorization);
	};

	const getWomenBottoms = async () => {
		// axios.defaults.headers.common['auth'] = auth;
		return axios.get(`/women/bottoms`, Authorization);
	};

	const getWomenTargets = async () => {
		return axios.get(`/women/targets`, Authorization);
	};

	const getMenTops = async () => {
		// axios.defaults.headers.common['auth'] = auth;
		return axios.get(`/men/tops`, Authorization);
	};

	const getMenBottoms = async () => {
		// axios.defaults.headers.common['auth'] = auth;
		return axios.get(`/men/bottoms`, Authorization);
	};

	const getMenTargets = async () => {
		return axios.get(`/men/targets`, Authorization);
	};

	const fetchWomenData = async () => {
		axios
			.all([
				getWomenTops(),
				getWomenBottoms(),
				getWomenTargets(),
				getMenTops(),
				getMenBottoms(),
				getMenTargets()
			])
			.then(
				axios.spread(
					(
						womensTopsdata,
						womensBottomsdata,
						womentsTargetData,
						mensTopsdata,
						mensBottomsdata,
						mentsTargetData
					) => {
						setWomensTop(womensTopsdata.data);
						setWomensBottom(womensBottomsdata.data);
						setWomensTarget(womentsTargetData.data);
						setMensTop(mensTopsdata.data);
						setMensBottom(mensBottomsdata.data);
						setMensTarget(mentsTargetData.data);

						if (category === 'tops') {
							setpairWithID(womensTopsdata.data[0].pairwith[0].id);
							setcategoryID(womensTopsdata.data[0].id);
						} else {
							setpairWithID(womensBottomsdata.data[0].pairwith[0].id);
							setcategoryID(womensBottomsdata.data[0].id);
						}
					}
				)
			)
			.catch((err) => {
				console.log(err);
				console.log(err.response);
			});
	};

	let setDynamicHeightTryOn = () => {
		let container = document.querySelector('.container-fluid');
		let tryonContainer = document.querySelector('#tryOnContainer');
		let height = container.getBoundingClientRect().top;
		if (height < -50) {
			tryonContainer.style.height = '100vh';
			tryonContainer.style.top = '0';
		} else {
			tryonContainer.style.height = '93vh';
			tryonContainer.style.top = '7.5vh';
		}
	};

	return (
		<div>
			<SimpleBarReact
				onScroll={(e) => {
					e.persist();
					setDynamicHeightTryOn();
				}}
				style={{ maxHeight: '100vh' }}>
				<HeaderOne />
				<div className='app-container'>
					<header className='sticky'>
						<div className='container-fluid'>
							<div className='row'>
								<div className='col-sm-12' style={{ paddingLeft: 0 }}>
									<div className='main-menu'>
										{/* <div className='menu-left '> */}
										{/* <NavBar type={'gender'} handleGenderChange={setGender} /> */}
										{/* <div className='brand-logo' />
										</div> */}
									</div>
								</div>
							</div>
						</div>
					</header>
					<TrendSpotter history={history} />
					{/* <ProductDetails /> */}
				</div>
			</SimpleBarReact>
		</div>
	);
}
