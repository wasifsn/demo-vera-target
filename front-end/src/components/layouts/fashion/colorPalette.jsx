import React, { useState, forwardRef, useImperativeHandle } from 'react';
import './styles.css';

const ColorPalette = forwardRef(({selectColor}, ref) => {

    const [availableColors, setAvailableColors] = useState([]) 

    useImperativeHandle(ref, () => ({
		filterColorPalette(data) {
			setAvailableColors(data)
		}
	}));

	return (
		<ul className='flex wrap'>
            {availableColors.map((el, i) => {
                return (
                    <button
                        className='tagTick'
                        onClick={(e) => {
                            e.persist();
                            selectColor(e, el);
                        }}
                        key={i}
                        type='button'
                        style={{ backgroundColor: el, color: el }}
                        class='trendHeader-searchbar-btn'>
                        S
                    </button>
                );
            })}
        </ul>
	);
})

export default ColorPalette
