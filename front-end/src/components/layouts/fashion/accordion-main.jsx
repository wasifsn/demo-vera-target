import React, { useState, useEffect } from 'react';

// import images data from static assets
// import { womens_bottoms } from '../../../assets/json/womens_new_design_bottoms';
// import { womens_tops } from '../../../assets/json/womens_new_design_tops';
import { mens_tops } from '../../../assets/json/mens_new_design_tops';
import { mens_bottoms } from '../../../assets/json/mens_new_design_bottoms';

import PairSimilar from './pair-similar';

export default function AccordionMain({ category, gender, dummyData, onIdSelect, onIdCategory }) {
	let { womens_topsNew, womens_bottomNew, mens_topsNew, mens_bottomNew } = dummyData;

	let dNames = {
		women: {
			tops: [
				{
					name: 'cream full sleeve',
					pair: [ 'white shorts', 'denim shorts', 'black skirt', 'denim pant' ],
					similar: [ 'cream full sleeve', 'cream half sleeve', 'cream tee', 'cream full sleeve' ]
				},
				{
					name: 'black top',
					pair: [ 'black shorts', 'black skirt', 'denim pant', 'black pant' ],
					similar: [ 'black top', 'black half sleeve', 'black tee', 'black dress' ]
				},
				{
					name: 'dotted full sleeve',
					pair: [ 'denim shorts', 'black skirt', 'denim pant', 'black pant' ],
					similar: [ 'pink top', 'pink half sleeve', 'dotted tee', 'dotted full sleeve' ]
				},
				{
					name: 'black tee',
					pair: [ 'denim shorts', 'denim pant', 'white pant', 'black pant' ],
					similar: [ 'black tee', 'black tee', 'black tee', 'black v-neck' ]
				}
			],
			bottoms: [
				{
					name: 'white shorts',
					pair: [ 'cream full sleeve', 'black tee', 'white tee', 'floral shirt' ],
					similar: [ 'white shorts', 'white shorts', 'white shorts', 'white shorts' ]
				},
				{
					name: 'denim shorts',
					pair: [ 'black sleeveless', 'white sleeveless', 'grey sleeveless', 'white tee' ],
					similar: [ 'denim shorts', 'denim shorts', 'denim shorts', 'denim shorts' ]
				},
				{
					name: 'black skirt',
					pair: [ 'cream full sleeve', 'cream tee', 'black half sleeve', 'pink half sleeve' ],
					similar: [ 'black skirt', 'black skirt', 'black skirt', 'black skirt' ]
				},
				{
					name: 'denim pant',
					pair: [ 'dotted shirt', 'black tee', 'white tee', 'grey tee' ],
					similar: [ 'denim pant', 'denim pant', 'denim pant', 'denim pant' ]
				}
			]
		},
		men: {
			tops: [
				{
					name: 'blue half sleeve',
					pair: [ 'grey pant', 'white pant', 'white shorts' ],
					similar: [ 'cream full sleeve', 'cream half sleeve', 'cream tee', 'cream full sleeve' ]
				},
				{
					name: 'black half sleeve',
					pair: [ 'white shorts', 'denim pant', 'light blue jeans' ],
					similar: [ 'black top', 'black half sleeve', 'black tee', 'black dress' ]
				},
				{
					name: 'grey half sleeve',
					pair: [ 'grey pant', 'white pant', 'white shorts' ],
					similar: [ 'pink top', 'pink half sleeve', 'dotted tee', 'dotted full sleeve' ]
				},
				{
					name: 'black full sleeve',
					pair: [ 'white pant', 'denim pant', 'light blue jeans' ],
					similar: [ 'black tee', 'black tee', 'black tee', 'black v-neck' ]
				},
				{
					name: 'beige half sleeve',
					pair: [ 'white pant', 'grey pant', 'grey Shorts' ],
					similar: [ 'black tee', 'black tee', 'black tee', 'black v-neck' ]
				},
				{
					name: 'black full sleeve',
					pair: [ 'grey pant', 'white shorts', 'blue jeans', 'dark blue jeans' ],
					similar: [ 'black tee', 'black tee', 'black tee', 'black v-neck' ]
				},
				{
					name: 'checkered shirt',
					pair: [ 'grey pant', 'black pant', 'dark grey pants', 'beige pant' ],
					similar: [ 'black tee', 'black tee', 'black tee', 'black v-neck' ]
				},
				{
					name: 'blue checkered shirt',
					pair: [ 'white pant', 'denim pant', 'dark grey pants', 'beige pant' ],
					similar: [ 'black tee', 'black tee', 'black tee', 'black v-neck' ]
				},
				{
					name: 'Brown Half sleeve',
					pair: [ 'grey pant', 'dark grey pant', 'blue denim', 'dark blue denim' ],
					similar: [ 'black tee', 'black tee', 'black tee', 'black v-neck' ]
				},
				{
					name: 'grey half sleeve',
					pair: [ 'grey pant', 'dark grey pant', 'black shorts', 'dark grey shorts' ],
					similar: [ 'black tee', 'black tee', 'black tee', 'black v-neck' ]
				},
				{
					name: 'Blue shirt',
					pair: [ 'white pant', 'dark grey pant', 'white shorts', 'black shorts' ],
					similar: [ 'black tee', 'black tee', 'black tee', 'black v-neck' ]
				}
			],
			bottoms: [
				{
					name: 'grey pant',
					pair: [
						'white full sleeve',
						'white half sleeve',
						'black full sleeve tee',
						'black half sleeve tee'
					],
					similar: [ 'white shorts', 'white shorts', 'white shorts', 'white shorts' ]
				},
				{
					name: 'white pant',
					pair: [ 'blue half sleeve tee', 'blue full sleeve', 'brown half sleeve', 'black full sleeve tee' ],
					similar: [ 'denim shorts', 'denim shorts', 'denim shorts', 'denim shorts' ]
				},
				{
					name: 'grey shorts',
					pair: [ 'blue half sleeve', 'Red tee', 'white Tee', 'grey Tee' ],
					similar: [ 'black skirt', 'black skirt', 'black skirt', 'black skirt' ]
				},
				{
					name: 'light blue jeans',
					pair: [ 'black tee', 'beige tee', 'white shirt', 'blue Shirt' ],
					similar: [ 'denim pant', 'denim pant', 'denim pant', 'denim pant' ]
				},
				{
					name: 'blue jeans',
					pair: [ 'black full sleeve tee', 'beige tee', 'grey tee', 'blue tee' ],
					similar: [ 'denim pant', 'denim pant', 'denim pant', 'denim pant' ]
				},
				{
					name: 'beige pant',
					pair: [ 'checkered shirt', 'light purple shirt', 'blue checkered shirt', 'checks shirt' ],
					similar: [ 'denim pant', 'denim pant', 'denim pant', 'denim pant' ]
				},
				{
					name: 'dark grey pant',
					pair: [ 'checkered shirt', 'blue checkered shirt', 'checks shirt', 'black shirt' ],
					similar: [ 'denim pant', 'denim pant', 'denim pant', 'denim pant' ]
				},
				{
					name: 'Black pant',
					pair: [ 'checkered shirt', 'white shirt', 'black shirt', 'white shirt' ],
					similar: [ 'denim pant', 'denim pant', 'denim pant', 'denim pant' ]
				}
			]
		}
	};

	const [ data, setData ] = useState([]);
	const [ stack, setStack ] = useState([]);
	const [ restStack, setRestStack ] = useState([]);
	const [ rowArray, setRowArray ] = useState([]);

	const [ dummyName, setDummyName ] = useState(dNames.women.top);

	const [ currentRow, setCurrentRow ] = useState(0);
	const [ currentItem, setCurrentItem ] = useState(0);

	const [ unselectAll, setUnselectAll ] = useState(false);

	useEffect(
		() => {
			setCurrentItem(0);
			setCurrentRow(0);
			setDummyName(dNames[gender][category]);
			setUnselectAll(false);
			gender === 'women'
				? category === 'tops' ? setData(womens_topsNew) : setData(womens_bottomNew)
				: category === 'tops' ? setData(mens_topsNew) : setData(mens_bottomNew);
		},
		[ category, gender, womens_topsNew, womens_bottomNew, mens_topsNew, mens_bottomNew ]
	);

	//set data to stack & rest initially
	useEffect(
		() => {
			if (data && data.length !== 0) {
				let tempRowArray = [];
				let tempData = [ ...data ];
				for (let i = 0; i < Math.ceil(tempData.length / 4); i + 4) {
					let row = tempData.splice(i, i + 4);
					tempRowArray.push(row);
				}
				setRowArray(tempRowArray);
				let newStack = new Array(tempRowArray.length).fill(null);
				newStack[0] = tempRowArray[0];
				setStack(newStack);
				let tempRowArray2 = [ ...tempRowArray ];
				tempRowArray2[0] = null;
				setRestStack(tempRowArray2);
			}
		},
		[ data ]
	);

	const actions = {
		//executes when user clicks on any apparel
		clothClicked: (rowIndex, itemIndex, restClicked, id) => {
			if (!unselectAll && rowIndex === currentRow && itemIndex === currentItem) {
				let tempRowArray = [ ...rowArray ];
				setStack(tempRowArray);
				setRestStack([]);
				setCurrentRow(0);
				setCurrentItem(0);
				setUnselectAll(true);
				onIdCategory('');
			} else {
				setUnselectAll(false);
				onIdCategory(id);
				//check if stack is clicked or the rest and handle accordingly
				if (restClicked) {
					let tempRowArray = [ ...rowArray ];
					let newStack = new Array(tempRowArray.length).fill(null);
					newStack[rowIndex] = tempRowArray[rowIndex];
					setStack(newStack);

					let tempRowArray2 = [ ...tempRowArray ];
					tempRowArray2[rowIndex] = null;
					setRestStack(tempRowArray2);

					setCurrentRow(rowIndex);
					setCurrentItem(itemIndex);
				} else {
					let tempRowArray = [ ...rowArray ];
					let newStack = new Array(tempRowArray.length).fill(null);
					newStack[rowIndex] = tempRowArray[rowIndex];
					setStack(newStack);

					let tempRowArray2 = [ ...tempRowArray ];
					tempRowArray2[rowIndex] = null;
					setRestStack(tempRowArray2);

					setCurrentRow(rowIndex);
					setCurrentItem(itemIndex);
				}
				//make clicked item results visible on viewport
				const elmnt = document.getElementById('pairWithPosition');
				if (elmnt) elmnt.scrollIntoView({ block: 'end', behavior: 'smooth' });
			}
		}
	};

	const isWindows = () => navigator.platform.indexOf('Win') > -1;

	const getStack = (rows, isRestStack) => {
		return (
			<div className='d-flex flx-wrap'>
				{rows.map(
					(rowItems, i) =>
						rowItems !== null &&
						rowItems.map((el, index) => (
							<div
								key={index}
								className={
									!unselectAll &&
									isRestStack === false &&
									currentRow === i &&
									currentItem === index ? (
										'PS-clothingStack active'
									) : (
										'PS-clothingStack'
									)
								}
								onClick={() => actions.clothClicked(i, index, isRestStack, el.id)}>
								{!(currentRow === i && currentItem === index) &&
								(index === 1 || index === 3) && <span className='PS-apparel-new'>{'New!'}</span>}
								<img
									src={
										isWindows() ? (
											require(`../../../assets${el.img_url.replace(/\\/g, '/')}`)
										) : (
											require(`../../../assets${el.img_url}`)
										)
									}
									alt='main Item'
								/>
								<div className='PS-clothingStack-details'>
									<span
										style={{
											color:
												!unselectAll &&
												isRestStack === false &&
												currentRow === i &&
												currentItem === index
													? '#fd6861'
													: ''
										}}
										className='PS-apparel-name'>
										{dummyName[i * 4 + index] !== undefined && dummyName[i * 4 + index].name}
									</span>
									<span
										style={{
											color:
												!unselectAll &&
												isRestStack === false &&
												currentRow === i &&
												currentItem === index
													? '#fd6861'
													: ''
										}}
										className='PS-apparel-price'>
										${el.apparel_price || Math.floor(Math.random() * 31) + 20}
									</span>
								</div>
							</div>
						))
				)}
			</div>
		);
	};

	return (
		<div>
			{rowArray.length > 0 && (
				<div className='accordian-item-panel'>
					<div>
						{getStack(stack, false)}

						{!unselectAll && <hr id='pairWithPosition' />}

						{restStack.length !== 0 && (
							<div>
								<PairSimilar
									title='Complete the look with'
									onIdSelect={onIdSelect}
									nameData={dummyName[currentRow * 4 + currentItem].pair}
									imgs_Arr={rowArray[currentRow][currentItem].pairwith}
								/>

								<PairSimilar
									title='Similar'
									nameData={dummyName[currentRow * 4 + currentItem].similar}
									imgs_Arr={rowArray[currentRow][currentItem].similars}
								/>
							</div>
						)}

						{getStack(restStack, true)}
					</div>
				</div>
			)}
		</div>
	);
}
