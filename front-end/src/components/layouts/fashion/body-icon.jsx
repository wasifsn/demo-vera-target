import React from 'react';
import body1 from '../../../assets/icons/BodyType_1_Icon.svg';
import body2 from '../../../assets/icons/BodyType_2_Icon.svg';
import body3 from '../../../assets/icons/BodyType_3_Icon.svg';

const BodyIcon = ({ target, selected }) => {
	let img;
	if (target === 1)
		img = (
			<svg width='15' height='60' viewBox='0 0 15 60' fill='none' xmlns='http://www.w3.org/2000/svg'>
				<circle
					cx='7.34961'
					cy='6'
					r='5.5'
					fill={selected === 1 ? '#F85454' : '#C4C4C4'}
					stroke={selected === 1 ? '#F85454' : '#C4C4C4'}
				/>
				<path
					d='M2.88304 15H11.8648C13.1416 15 14.0915 16.1801 13.8186 17.4274L10.9538 30.5236C10.8854 30.8366 10.8931 31.1615 10.9764 31.4709L13.6712 41.4801C14.0133 42.7508 13.056 44 11.7399 44H3.0215C1.6866 44 0.726379 42.7172 1.10256 41.4364L4.01788 31.5104C4.11611 31.1759 4.12522 30.8216 4.04431 30.4826L0.937666 17.4642C0.637698 16.2072 1.59074 15 2.88304 15Z'
					fill={selected === 1 ? '#F85454' : '#C4C4C4'}
					stroke={selected === 1 ? '' : '#C4C4C4'}
				/>
				<rect x='4.34961' y='46' width='2' height='14' fill={selected === 1 ? '#F85454' : '#C4C4C4'} />
				<rect x='8.34961' y='46' width='2' height='14' fill={selected === 1 ? '#F85454' : '#C4C4C4'} />
			</svg>
		);

	if (target === 2)
		img = (
			<svg width='25' height='60' viewBox='0 0 25 60' fill='none' xmlns='http://www.w3.org/2000/svg'>
				<circle
					cx='12.3496'
					cy='6'
					r='6'
					fill={selected === 2 ? '#F85454' : '#C4C4C4'}
					stroke={selected === 2 ? '#F85454' : '#C4C4C4'}
				/>
				<path
					d='M3.32868 15H21.4636C22.86 15 23.8266 16.3947 23.3363 17.7022L18.6429 30.218C18.4552 30.7184 18.4756 31.2731 18.6996 31.7583L23.0397 41.1619C23.6514 42.4871 22.6834 44 21.2238 44H3.59581C2.1066 44 1.13974 42.4307 1.80946 41.1006L6.48542 31.8136C6.74566 31.2967 6.76927 30.6925 6.55016 30.1569L1.47758 17.7573C0.939313 16.4415 1.90707 15 3.32868 15Z'
					fill={selected === 2 ? '#F85454' : '#C4C4C4'}
					stroke={selected === 2 ? '' : '#C4C4C4'}
				/>
				<rect x='9.34961' y='46' width='2' height='14' fill={selected === 2 ? '#F85454' : '#C4C4C4'} />
				<rect x='13.3496' y='46' width='2' height='14' fill={selected === 2 ? '#F85454' : '#C4C4C4'} />
			</svg>
		);

	if (target === 3)
		img = (
			<svg width='35' height='60' viewBox='0 0 35 60' fill='none' xmlns='http://www.w3.org/2000/svg'>
				<circle
					cx='18.3496'
					cy='6'
					r='6'
					fill={selected === 3 ? '#F85454' : '#C4C4C4'}
					stroke={selected === 3 ? '#F85454' : '#C4C4C4'}
				/>
				<path
					d='M3.99593 15H31.2916C32.7339 15 33.702 16.4803 33.1239 17.8016L27.74 30.1077C27.4936 30.6709 27.5199 31.3162 27.8114 31.8575L32.7621 41.0518C33.4796 42.3843 32.5145 44 31.0012 44H4.31243C2.77311 44 1.81083 42.3339 2.58006 41.0006L7.82826 31.9037C8.15494 31.3374 8.18427 30.6473 7.90681 30.0554L2.18502 17.8489C1.56334 16.5226 2.53121 15 3.99593 15Z'
					fill={selected === 3 ? '#F85454' : '#C4C4C4'}
					stroke={selected === 3 ? '' : '#C4C4C4'}
				/>
				<rect x='15.3496' y='46' width='2' height='14' fill={selected === 3 ? '#F85454' : '#C4C4C4'} />
				<rect x='19.3496' y='46' width='2' height='14' fill={selected === 3 ? '#F85454' : '#C4C4C4'} />
			</svg>
		);

	return <span>{img}</span>;
};

export default BodyIcon;
