import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';
import Slider from 'react-slick';

import ProductItem from '../common/product-item';
export default class TopsBottom extends Component {
	static defaultProps = {};
	constructor(props) {
		super(props);
		this.state = {
			category: 'tops',
			tops: [],
			bottoms: [],
			loader: true
		};
	}
	setCategory = (data) => {
		this.setState({ category: data });
	};
	render() {
		const settings = {
			// dots: true,
			infinite: true,
			speed: 500,
			slidesToShow: 3,
			slidesToScroll: 3
		};
		return (
			<div>
				<Slider {...settings}>
					{this.state.category === 'tops' ? (
						this.state.tops.length > 0 &&
						this.state.tops.map((product, index) => (
							<div key={index}>
								<ProductItem
									category={this.props.category}
									targets={this.state.tops}
									type='top collection'
								/>
							</div>
						))
					) : (
						this.state.bottoms.length > 0 &&
						this.state.bottoms.map((product, index) => (
							<div key={index}>
								<ProductItem
									category={this.props.category}
									targets={this.state.bottoms}
									type='top collection'
								/>
							</div>
						))
					)}
				</Slider>
			</div>
		);
	}
}
