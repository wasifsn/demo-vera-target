import React, { useEffect, useState } from 'react';

import './styles.css';
import { translate } from 'react-redux-multilingual/lib/utils';

export default function PairSimilar({ imgs_Arr, type, title, onIdSelect, nameData }) {
	const [ slideIndex, setSlideIndex ] = useState(0);
	const [ selectIndex, setSelectIndex ] = useState(0);

	const [ scrollDistance, setScrollDistance ] = useState(0);

	const prev = () => {
		let dist = scrollDistance;
		if (scrollDistance > 0) {
			let scrollContainer = document.getElementById(`scrollContainer${title}`);
			scrollContainer.style.transform = `translateX(${20 - scrollDistance}%)`;
			setScrollDistance(scrollDistance - 20);
		}
	};

	const next = () => {
		let dist = scrollDistance;
		if (scrollDistance < (imgs_Arr.length - 4) * 20) {
			let scrollContainer = document.getElementById(`scrollContainer${title}`);
			scrollContainer.style.transform = `translateX(-${20 + scrollDistance}%)`;
			setScrollDistance(scrollDistance + 20);
		}
	};

	return (
		<div className='PS-main-container' style={{ opacity: title === 'Similar' ? 1 : 1 }}>
			<div className='flex pb-3'>
				<span
					className={
						title === 'Similar Items' ? type === 'tops' ? (
							'similar_icon'
						) : (
							'recom_icon'
						) : type === 'tops' ? (
							'recom_icon'
						) : (
							'similar_icon'
						)
					}
				/>
				<h6 className='PS-sectionTitle pl-3 '>{title}</h6>
			</div>
			<div style={{ overflowX: 'scroll', width: '110%' }}>
				<div id={`scrollContainer${title}`} className='TS-outer-container'>
					{imgs_Arr.map((el, index) => (
						<div
							onClick={() => [ onIdSelect(el), setSelectIndex(index) ]}
							className={'TS-images-section-outer-wrapper TS-images-section-outer-wrapper-pairs'}>
							<img className='TS-images' key={`item${index}`} src={el.img_url} />
							{title !== 'Similar Items' && (
								<img
									className='TS-images-tryon-button'
									src='../../../assets/icons/Product_Tag.svg'
									alt=''
								/>
							)}
							<div className='TS-images-details'>
								<div className='TS-images-name'>{el.name}</div>
								<div className='TS-images-price'>${Math.floor(Math.random() * 31) + 20}.99</div>
							</div>
						</div>
					))}
				</div>
			</div>
			{imgs_Arr.length > 4 && (
				<div className='PD-carausal-container'>
					<div onClick={() => prev()}>{'<'}</div>
					<div onClick={() => next()}>{'>'}</div>
				</div>
			)}
		</div>
	);
}
