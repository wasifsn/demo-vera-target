import React, { useEffect, useState, forwardRef, useImperativeHandle } from 'react';
import MashedImage from '../../products/mashed-image';
import SavedOutfits from './saved-outfits';
import './styles.css';

const tryon = forwardRef(({ id, pairId, tryonTAB }, ref) => {
	const [ topId, setTopId ] = useState(id);
	const [ bottomId, setBottomId ] = useState(pairId);
	const [ gender, setGender ] = useState(
		sessionStorage.getItem('gender') ? sessionStorage.getItem('gender') : 'women'
	);
	const [ tryTopData, setTryTopData ] = useState(
		sessionStorage.getItem('top') ? JSON.parse(sessionStorage.getItem('top')) : { name: 'BLACK TOP', price: 44 }
	);
	const [ tryBottomData, setTryBottomData ] = useState(
		sessionStorage.getItem('bottom')
			? JSON.parse(sessionStorage.getItem('bottom'))
			: { name: 'BLACK DENIM', price: 41 }
	);

	const targets = [ '', '', '' ];
	const tryonTabs = [ 'Try on', 'Saved Outfits' ];
	const [ tryOnTab, setTryOnTab ] = useState(tryonTAB);

	const delfaultMensTop = {
		id: 'c9615c98-2499-4f99-95e8-60b9dd32c0d0',
		data: { name: 'GREY FULL SLEVE TEE', price: 29 }
	};
	const delfaultMensBottom = { id: '8a599378-ec99-4aa4-8f43-2be521722f6f', data: { name: 'BLUE JEANS', price: 29 } };
	const delfaultWomensTop = { id: '71d12ff0-c70f-41f5-af9c-19d7f24b6050', data: { name: 'BLACK TOP', price: 29 } };
	const delfaultWomensBottom = {
		id: 'f0909e54-f621-411c-bda1-4fa92bb26c0c',
		data: { name: 'BLACK PANT', price: 29 }
	};

	useEffect(
		() => {
			setTryOnTab('Try on');
		},
		[ id, pairId, gender ]
	);

	useImperativeHandle(ref, () => ({
		changeTryItem(item) {
			if (item.category === 'tops') {
				setTopId(item.id);
				setTryTopData({ name: item.name, price: item.price });
			} else {
				setBottomId(item.id);
				setTryBottomData({ name: item.name, price: item.price });
			}
		},

		changeGender(gender) {
			setGender(gender);
			setTopId(gender === 'women' ? delfaultWomensTop.id : delfaultMensTop.id);
			setBottomId(gender === 'women' ? delfaultWomensBottom.id : delfaultMensBottom.id);
			setTryTopData(gender === 'women' ? delfaultWomensTop.data : delfaultMensTop.data);
			setTryBottomData(gender === 'women' ? delfaultWomensBottom.data : delfaultMensBottom.data);
		}
	}));

	const ModalActions = {
		closeTryOn: (e) => {
			setTryOnTab(tryonTabs[0]);
			let modal = document.getElementById('tryOnContainer');
			modal.style.transform = 'translateX(115%)';
			if (Array.from(modal.classList).includes('fullPD-try-container')) modal.classList = 'PD-try-container';
		},

		openTryOn: (e) => {
			setTryOnTab(tryonTabs[0]);
			let header = document.querySelector('.powered-by').getBoundingClientRect().top;
			let modal = document.getElementById('tryOnContainer');
			if (header < -79) {
				modal.classList = 'fullPD-try-container';
			}
			modal.style.transform = 'translateX(0%)';
		}
	};

	return (
		<div>
			<div
				style={{ zIndex: 2 }}
				className='PD-try-button'
				onClick={(e) => {
					e.persist();
					ModalActions.openTryOn(e);
				}}
			/>
			<div className='PD-try-container' id='tryOnContainer'>
				<div
					className='PD-try-button-close'
					onClick={(e) => {
						e.persist();
						ModalActions.closeTryOn(e);
					}}>
					<img src='https://img.icons8.com/ios-glyphs/90/000000/multiply.png' />
				</div>
				<h3 className='Virtual-Fitting-Room-Header boldFont'>Virtual Fitting Room</h3>
				<ul className='Virtual-Fitting-Room-Header-tab'>
					{tryonTabs.map((el, i) => {
						return (
							<li
								className={
									el === tryOnTab ? (
										'Virtual-Fitting-Room-Header-tab-sel'
									) : (
										'Virtual-Fitting-Room-Header-tab-unsel'
									)
								}
								onClick={(e) => {
									e.persist();
									setTryOnTab(el);
								}}>
								{el}
							</li>
						);
					})}
				</ul>

				{tryOnTab === tryonTabs[0] ? (
					<>
						<MashedImage
							id={{ tops: topId === '' ? id : topId, bottoms: bottomId === '' ? pairId : bottomId }}
							targets={targets}
							gender={gender}
							top={tryTopData}
							bottom={tryBottomData}
						/>
						<div className='ADD-TRY-ITEM-TO-CART mediumFont'>ADD ITEM TO CART</div>
					</>
				) : (
					<SavedOutfits />
				)}
			</div>
		</div>
	);
});

export default tryon;
