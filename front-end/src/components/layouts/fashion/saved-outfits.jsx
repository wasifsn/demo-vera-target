import React, { useEffect, useState } from 'react';

import SimpleBarReact from 'simplebar-react';

// const imgs = sessionStorage.getItem('savedItems') ? JSON.parse(sessionStorage.getItem('savedItems')).items : [];
// console.log(imgs);

const SavedOutfits = () => {
	const [ imgs, setImgs ] = useState(
		sessionStorage.getItem('savedItems') ? JSON.parse(sessionStorage.getItem('savedItems')).items : []
	);

	useEffect(
		() => {
			setImgs(
				sessionStorage.getItem('savedItems')
					? JSON.parse(sessionStorage.getItem('savedItems')).items !== null
						? JSON.parse(sessionStorage.getItem('savedItems')).items
						: []
					: []
			);
		},
		[ sessionStorage.getItem('savedItems') ]
	);
	return (
		<SimpleBarReact style={{ maxHeight: '79vh' }}>
			<div className='saved-outfits-wrapper mt-2'>
				{imgs.map((el, index) => (
					<div key={index} className='saved-outfits-inner-wrapper'>
						<img src={el} alt='' />
					</div>
				))}
				{/* <div className='saved-outfits-inner-wrapper'>
					<img
						src='/women/try/?top=07bddc54-0f28-4ff9-ac82-084fb5eddbc1&bottom=f0909e54-f621-411c-bda1-4fa92bb26c0c&target=1'
						alt=''
					/>
				</div> */}
			</div>
		</SimpleBarReact>
	);
};

export default SavedOutfits;
