import React, { useState, useEffect, useRef } from 'react';

import SimpleBarReact from 'simplebar-react';
import 'simplebar/src/simplebar.css';

import HeaderOne from '../../common/headers/header-one';
import clothsData from '../../../assets/json/newDemo.json';
import PairSimilar from './pair-similar';
import './styles.css';
import TryOn from './try-on';

export default function ProductDetail(props) {
	const { history } = props;

	const sizeArray = [ 'XS', 'S', 'M', 'L', 'XL', 'XXL' ];

	const [ pairId, setPairId ] = useState('');
	const [ itemId, setItemId ] = useState('');
	const [ selectedSize, setSelectedSize ] = useState(sizeArray[0]);

	let data;

	const tryOnRef = useRef();

	const gender = sessionStorage.getItem('gender') ? sessionStorage.getItem('gender') : 'women';

	if (props.location.data) data = props.location.data;
	else {
		let d = clothsData.filter((el) => el.id === history.location.pathname.slice(15));
		d.length === 0 ? history.push('/') : (data = d[0]);
	}

	const onIdSelect = (item) => {
		tryOnRef.current.changeTryItem({
			id: item.id,
			category: data.category === 'tops' ? 'bottoms' : 'tops',
			name: item.name,
			price: 29
		});
		setDataToTryOn(item);
	};

	const tryVirtually = () => {
		tryOnRef.current.changeTryItem(data);
		setDataToTryOn(data);
	};

	const setDataToTryOn = (item) => {
		sessionStorage.setItem(data.category === 'tops' ? 'bottomId' : 'topId', item.id);
		sessionStorage.setItem(
			data.category === 'tops' ? 'bottom' : 'top',
			JSON.stringify({ name: item.name, price: item.price || 29 })
		);
		openTryOn();
	};

	const openTryOn = () => {
		document.querySelector('.Virtual-Fitting-Room-Header-tab li:first-child').click();
		let header = document.querySelector('.powered-by').getBoundingClientRect().top;
		let modal = document.getElementById('tryOnContainer');
		if (header < -79) {
			modal.classList = 'fullPD-try-container';
		}
		modal.style.transform = 'translateX(0%)';
	};

	useEffect(
		() => {
			let pairId = sessionStorage.getItem('bottomId');
			let itemId = sessionStorage.getItem('topId');
			setPairId(pairId);
			setItemId(itemId);
		},
		[ data ]
	);

	const productInfo = (el) => {
		return (
			<div style={{ display: 'flex', height: '75vh' }}>
				<div style={{ width: '50%' }}>
					<img
						style={{ height: '100%', width: '100%', objectFit: 'contain' }}
						// src={`https://d28m5bx785ox17.cloudfront.net/v1/img/Mx8PfeivaoEvSbt4gfOB4UNS6HXNvJZeMGt3aNy1Pwg=`}
						src={props.location.mainUrl}
						alt='main Item'
					/>
				</div>
				<div style={{ width: '50%', padding: '2% 0' }}>
					<div>
						{/* <p className='PD-title'>{`Cloth`}™</p> */}
						<p className='PD-title'>
							{props.location.data[props.location.id].post.products[0].targetInfo.productName}™
						</p>
						<div>
							<p className='PD-price-after'>
								{props.location.data[props.location.id].post.products[0].targetInfo.displayPrice}
							</p>
						</div>
					</div>

					{/* {data.color_pallete &&
					data.color_pallete.length > 0 && (
						<div>
							<p className='PD-color-title'>Color : blue</p>
							<ul className='PD-color-tile-container'>{data.color_pallete.map((color) => <li />)}</ul>
						</div>
					)} */}

					{/* <div> */}
					{/* <p className='PD-size-title'>
							Size :{' '}
							<span className='boldFont' style={{ fontWeight: 900 }}>
								{selectedSize}
							</span>
						</p> */}
					{/* <ul className='PD-size-tile-container'>
							{sizeArray.map((size) => (
								<li
									onClick={() => setSelectedSize(size)}
									className={size === selectedSize ? 'sizeSelectedClass' : ''}>
									{size}
								</li>
							))}
						</ul> */}
					{/* </div> */}

					<br />
					<br />

					{/* <div>
						{data.pair_with && (
							<div className='TRY-ON-VIRTUALLY' onClick={() => tryVirtually()}>
								TRY ON VIRTUALLY!
							</div>
						)}
						<div className='ADD-TO-CART'>ADD TO CART</div>
					</div> */}
				</div>
			</div>
		);
	};
	const scrollEnd = () => {
		const elmnt = document.querySelector('.scrollEnd');
		if (elmnt) elmnt.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'center' });
	};

	return (
		<div>
			<SimpleBarReact style={{ maxHeight: '100vh' }}>
				<HeaderOne />
				<div className='app-container'>
					{productInfo()}
					<div
						style={{
							position: 'fixed',
							width: '3%',
							height: '3vw',
							left: '1.5vw',
							top: '5vw',
							cursor: 'pointer'
						}}
						onClick={() => history.go(-1)}>
						<img
							style={{ width: '100%', height: '100%', objectFit: 'cover' }}
							src='https://img.icons8.com/cotton/100/000000/circled-left-2.png'
						/>
					</div>

					<div style={{ display: 'flex', flexFlow: 'row wrap', justifyContent: 'flex-end' }}>
						<button
							style={{ position: 'fixed', zIndex: '10', padding: '0.5%', top: '9.5%', right: '5%' }}
							className=' mt-2 PS-inner-container-btn'
							onClick={(e) => {
								e.persist();
								scrollEnd(e);
							}}>
							Scroll To End
						</button>
					</div>
					{/* <div className='pt-4 pb-4'>
						<h2 style={{ textAlign: 'centeleftr' }}>Similar Tops And Bottoms</h2>
					</div> */}

					<div className='flex pt-4 pb-4'>
						<img className='similar_icon' alt='' />
						<h6 className='PS-sectionTitle pl-3 '>{`Similar Tops And Bottoms`}</h6>
					</div>

					<div className='flex wrap'>
						{props.location.data[props.location.id].similar_tops ? (
							props.location.data[props.location.id].similar_tops.map((el, i) => (
								<div className='TS-images-section-outer-wrapper'>
									<img className='TS-images' src={el} key={i} />
								</div>
							))
						) : (
							''
						)}
					</div>
					<hr style={{ paddingBottom: '3%' }} />
					<div className='flex wrap'>
						{props.location.data[props.location.id].similar_bottoms ? (
							props.location.data[props.location.id].similar_bottoms.map((el, i) => (
								<div className='TS-images-section-outer-wrapper'>
									<img className='TS-images' src={el} key={i} />
								</div>
							))
						) : (
							''
						)}
					</div>
					<span className='scrollEnd' />
				</div>
			</SimpleBarReact>
		</div>
	);
}
