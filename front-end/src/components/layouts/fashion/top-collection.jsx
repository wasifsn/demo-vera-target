import React, { Component } from 'react';
import Slider from 'react-slick';
import { connect } from 'react-redux';
import axios from 'axios';
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';
import Loader from 'react-loader-spinner';

import { getTrendingCollection } from '../../../services/index';
import { Product4, Product5 } from '../../../services/script';
import { addToCart, addToWishlist, addToCompare } from '../../../actions/index';
import ProductItem from '../common/product-item';

function SampleNextArrow(props) {
	const { className, style, onClick } = props;
	return (
		<div
			className={className}
			style={{ ...style, display: 'block', backgroundColor: 'lightgrey', borderRadius: '50%' }}
			onClick={onClick}
		/>
	);
}

function SamplePrevArrow(props) {
	const { className, style, onClick } = props;
	return (
		<div
			className={className}
			style={{ ...style, display: 'block', backgroundColor: 'lightgrey', borderRadius: '50%' }}
			onClick={onClick}
		/>
	);
}

class TopCollection extends Component {
	// static defaultProps = {
	// 	location: {
	// 		state: {
	// 			imageProps: { type: 'shirt', gender: 'female' }
	// 		}
	// 	}
	// };
	constructor(props) {
		super(props);
		this.state = {
			products: [],
			productData: null,
			loader: true,
			images: [],
			targets: []
		};
		this.loadData();
		this.getNewImages();
		this.getTargets();
	}
	showLoader = () => {
		return (
			<div className='pt-5 pb-5' style={{ display: 'flex', justifyContent: 'center', height: '15rem' }}>
				<Loader
					type='Puff'
					color='red'
					height={100}
					width={100}
					// timeout={3000} //3 secs
				/>
			</div>
		);
	};

	getNewImages = async () => {
		// let axiosInstance = (axios.create = {
		// 	baseUrl: 'http://127.0.0.1:8000/'
		// });
		try {
			axios.defaults.headers.post['Content-Type'] = 'application/json';
			let category = this.props.category;
			let response = await axios.get(`http://127.0.0.1:8000/${category}`);
			console.log('newImgsResponse', response);
			this.setState({ images: response.data });
		} catch (err) {
			console.log(err.response);
			this.setState({ images: null });
		}
	};

	getTargets = async () => {
		try {
			let response = await axios.get(`http://127.0.0.1:8000/targets`);
			console.log('newTargetsResponse', response);
			this.setState({ targets: response.data });
		} catch (err) {
			console.log(err);
			console.log(err.response);
			this.setState({ targets: null });
		}
	};

	loadData = () => {
		setTimeout(() => {
			this.setState({ loader: true });
			if (this.state.productData === null) {
				console.log('images loaded 1st time');
				let [ subcategory, gender, source, page ] = [ 'tshirt', 'female', 'wallmart', 0 ];
				let sendPromise = axios.get('/products', { params: { source, subcategory, gender, page } });
				sendPromise.then((result) => {
					console.log(result);
					this.setState({ products: result.data.products });
					this.setState({ loader: false });
				});
			} else if (this.state.productData.imageProps.gender) {
				console.log('images loaded when props are available');
				let [ subcategory, gender, source, page ] = [
					this.state.productData.imageProps.subcategory,
					this.state.productData.imageProps.gender,
					this.state.productData.imageProps.source,
					this.state.productData.imageProps.page
				];
				let sendPromise = axios.get('/products', { params: { source, subcategory, gender, page } });
				sendPromise.then((result) => {
					console.log(result);
					this.setState({ products: result.data.products });
					this.setState({ loader: false });
				});
			} else {
				console.log(this.state);
				console.log(this.props);
				let [ subcategory, gender, source, page ] = [
					this.props.imageProps.imageProps.subcategory,
					this.props.imageProps.imageProps.gender,
					this.props.imageProps.imageProps.source,
					this.props.imageProps.imageProps.page
				];
				let sendPromise = axios.get('/products', { params: { source, subcategory, gender, page } });
				sendPromise.then((result) => {
					console.log(result);
					this.setState({ products: result.data.products });
					this.setState({ loader: false });
				});
			}
		}, 200);
	};

	componentWillReceiveProps(nextProps) {
		console.log('componentWillReceiveProps');
		const { imageProps } = this.props;
		this.setState({ products: [] });
		if (nextProps.imageProps !== imageProps) {
			this.loadData();
		}
	}
	componentDidUpdate(nextProps) {
		console.log('componentDidUpdate');
		const { imageProps } = this.props;
		if (nextProps.imageProps !== imageProps) {
			this.setState({ productData: imageProps });
		}
	}
	render() {
		const { symbol, addToCart, addToWishlist, addToCompare, type } = this.props;
		// this.loadData();
		const properties = {
			infinite: true,
			speed: 300,
			slidesToShow: 3,
			slidesToScroll: 3,
			// autoplay: true,
			// autoplaySpeed: 3000,
			nextArrow: <SampleNextArrow />,
			prevArrow: <SamplePrevArrow />,
			responsive: [
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 3
					}
				},
				{
					breakpoint: 991,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 2
					}
				},
				{
					breakpoint: 430,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
				}
			]
		};
		return (
			<div className='box-size'>
				{/*Paragraph*/}
				{/* <div className='title1  section-t-space'>
					<h2 className='title-inner1'>collection</h2>
				</div> */}
				{/*Paragraph End*/}
				<section className='section-b-space p-t-0' style={{ paddingBottom: '1rem' }}>
					<div className='container'>
						<div className='row'>
							<div className='col'>
								{this.state.products ? (
									<Slider
										{...properties}
										className='product-2 product-m justify-content-center align-items-center'>
										{this.state.loader ? (
											this.showLoader()
										) : (
											this.state.products.length > 0 &&
											this.state.images.map((product, index) => (
												<div key={index}>
													<ProductItem
														category={this.props.category}
														product={product}
														targets={this.state.targets}
														type='top collection'
														symbol={symbol}
														onAddToCompareClicked={() => addToCompare(product)}
														onAddToWishlistClicked={() => addToWishlist(product)}
														onAddToCartClicked={() => addToCart(product, 1)}
														key={index}
													/>
												</div>
											))
										)}
									</Slider>
								) : (
									<h1>REQUESTED DATA NOT AVAILABLE</h1>
								)}
							</div>
						</div>
					</div>
				</section>
			</div>
		);
	}
}

const mapStateToProps = (state, ownProps) => ({
	items: getTrendingCollection(state.data.products, ownProps.type),
	symbol: state.data.symbol
});

export default connect(mapStateToProps, {
	addToCart,
	addToWishlist,
	addToCompare
})(TopCollection);
