import React, { useState, useEffect, useRef } from 'react';
// import womenData from '../../../assets/json/newDemo.json';
// import menData from "../../../assets/json/men-top.json"

import clothData from '../../../assets/json/backup3.json';
import TryOn from './try-on';

import './styles.css';
import Isotope from 'isotope-layout';
import ColorPalette from './colorPalette';

import TargetData from './target-data';

const TrendSpotter = ({ history }) => {
	const Tags = [
		// { gender: 'women', tag: 'Summer Sheek' },
		// { gender: 'women', tag: 'Work from Home Outfit' },
		// { gender: 'women', tag: 'Business Casual' },
		// { gender: 'women', tag: 'Simple Basic' },
		// { gender: 'women', tag: 'Evening Wear' },
		// { gender: 'women', tag: 'Weekend Outfit' },
		// { gender: 'women', tag: 'Paris Fashion Week' },
		// { gender: 'women', tag: 'Holiday Outfit' },
		// { gender: 'women', tag: 'Casual Summer' }
		// { gender: 'men', tag: 'Mens casual' },
		// { gender: 'men', tag: 'Mens Tag' },
		// { gender: 'men', tag: 'Mens Tag' },
		// { gender: 'men', tag: 'Mens Tag' },
		// { gender: 'men', tag: 'Mens Tag' },
		// { gender: 'men', tag: 'Mens Tag' },
		// { gender: 'men', tag: 'Mens Tag' },
		// { gender: 'men', tag: 'Mens Tag' },
		// { gender: 'men', tag: 'Mens Tag' },
		// { gender: 'men', tag: 'Mens Tag' }
	];

	let elem = document.querySelector('.grid');
	const iso = new Isotope(elem, {});

	let TagElem = document.querySelector('.tagGrid');
	const Tagiso = new Isotope(TagElem, { layoutMode: 'fitRows' });

	const colors = [ 'black', 'pink', '#FFDAB9', 'white', 'brown', 'blue', 'beige', '#676A57', 'grey', 'red' ];

	let selectedTags = [];

	const [ data, setData ] = useState([]);
	const [ tryTopId, setTryTopId ] = useState('');
	const [ tryBottomId, setTryBottomId ] = useState('');
	const [ color, setColor ] = useState('');
	const [ tags, setTags ] = useState([]);
	const [ tagObj, settagObj ] = useState({});
	let gender = sessionStorage.getItem('gender') ? sessionStorage.getItem('gender') : 'women';

	const [ genderCheck, setGenderCheck ] = React.useState(gender);

	useEffect(
		() => {
			if (color === '' || gender === 'men') {
				document.querySelector('.color-pallete').style.display = 'none';
			} else {
				document.querySelector('.color-pallete').style.display = '';
			}
		},
		[ color, gender ]
	);

	const delfaultMensTop = {
		id: 'c9615c98-2499-4f99-95e8-60b9dd32c0d0',
		data: { name: 'GREY FULL SLEVE TEE', price: 29 }
	};
	const delfaultMensBottom = { id: '8a599378-ec99-4aa4-8f43-2be521722f6f', data: { name: 'BLUE JEANS', price: 29 } };
	const delfaultWomensTop = { id: '71d12ff0-c70f-41f5-af9c-19d7f24b6050', data: { name: 'BLACK TOP', price: 29 } };
	const delfaultWomensBottom = {
		id: 'f0909e54-f621-411c-bda1-4fa92bb26c0c',
		data: { name: 'BLACK PANT', price: 29 }
	};

	const setClothsDataToTryOn = () => {
		if (!sessionStorage.getItem('topId')) {
			setTryTopId(gender === 'women' ? delfaultWomensTop.id : delfaultMensTop.id);
			sessionStorage.setItem('topId', gender === 'women' ? delfaultWomensTop.id : delfaultMensTop.id);
			sessionStorage.setItem(
				'top',
				JSON.stringify(gender === 'women' ? delfaultWomensTop.data : delfaultMensTop.data)
			);
		} else setTryTopId(sessionStorage.getItem('topId'));

		if (!sessionStorage.getItem('bottomId')) {
			setTryBottomId(gender === 'women' ? delfaultWomensBottom.id : delfaultMensBottom.id);
			sessionStorage.setItem('bottomId', gender === 'women' ? delfaultWomensBottom.id : delfaultMensBottom.id);
			sessionStorage.setItem(
				'bottom',
				JSON.stringify(gender === 'women' ? delfaultWomensBottom.data : delfaultMensBottom.data)
			);
		} else setTryBottomId(sessionStorage.getItem('bottomId'));
	};

	const shuffle = (array) => array.sort(() => Math.random() - 0.5);

	const setDefaultClothOnGenderChange = (gen) => {
		sessionStorage.setItem('gender', gen);
		sessionStorage.setItem('topId', gen === 'women' ? delfaultWomensTop.id : delfaultMensTop.id);
		sessionStorage.setItem('top', JSON.stringify(gen === 'women' ? delfaultWomensTop.data : delfaultMensTop.data));
		sessionStorage.setItem('bottomId', gen === 'women' ? delfaultWomensBottom.id : delfaultMensBottom.id);
		sessionStorage.setItem(
			'bottom',
			JSON.stringify(gen === 'women' ? delfaultWomensBottom.data : delfaultMensBottom.data)
		);
		tryOnRef.current.changeGender(gen);
		iso.arrange({
			filter: (itemElem) => {
				const g = itemElem.getAttribute('data-gender');
				return g === gen;
			}
		});
	};

	const filterGender = () => {
		if (data.length > 0) {
			let grid = document.getElementById('grid');
			let Tgrid = document.getElementById('tagGrid');
			iso.arrange({
				filter: (itemElem) => {
					const g = itemElem.getAttribute('data-gender');
					setTimeout(() => {
						grid.style.opacity = 1;
					}, 100);
					return g === gender;
				}
			});

			Tagiso.arrange({
				filter: (itemElem) => {
					const t = itemElem.getAttribute('data-tag-gender');
					setTimeout(() => {
						Tgrid.style.opacity = 1;
					}, 500);
					return t === gender;
				}
			});
		}
	};

	useEffect(
		() => {
			if (data.length > 0) {
				Tagiso.arrange({
					filter: (itemElem) => {
						const t = itemElem.getAttribute('data-tag-gender');
						return t === gender;
					}
				});
			}
		},
		[ data ]
	);

	const changeGender = (gen) => {
		[ 'bottom', 'bottomId', 'gender', 'top', 'topID', 'selectedColor' ].forEach((key) =>
			sessionStorage.removeItem(key)
		);

		let elements = document.querySelectorAll('.trendHeader-searchbar-btn');
		elements.forEach((el) => (el.style.border = '1px solid lightgray'));
		elements.forEach((el) => (el.style.boxShadow = 'none'));

		let heading = document.getElementById('headingText');
		heading.innerText = `${gen}'s`;

		let tagTick = document.getElementsByClassName('tagTick');
		for (let i = 0; i < tagTick.length; i++) tagTick[i].style.display = 'none';

		let tagsContainer = document.getElementsByClassName('TS-tags-container');
		for (let i = 0; i < tagsContainer.length; i++) tagsContainer[i].classList.remove('TS-Activetags-container');
		selectedTags = [];

		Tagiso.arrange({
			filter: (itemElem) => {
				const t = itemElem.getAttribute('data-tag-gender');
				return t === gen;
			}
		});
		setDefaultClothOnGenderChange(gen);
	};

	let computeAllImgs = () => {
		setClothsDataToTryOn();
		let tempData = [ ...shuffle(clothData) ];
		let filteredData = [ ...tempData ];
		let similarData = [];

		for (let i = 0; i < tempData.length; i++) {
			for (let j = 0; j < tempData[i].similar.length; j++) {
				tempData[i].similar[j].gender = tempData[i].gender;
				similarData.push(tempData[i].similar[j]);
			}
		}

		const uniqueSimilarData = RemoveDuplicatesFromArray(similarData);
		const tData = [ ...filteredData, ...uniqueSimilarData ];
		const uniqueData = RemoveDuplicatesFromArray(tData);
		setData(uniqueData);
	};

	const RemoveDuplicatesFromArray = (arr) => {
		let unq = Array.from(new Set(arr.map((a) => a.id))).map((id) => arr.find((a) => a.id === id));
		return unq;
	};

	useEffect(() => {
		computeAllImgs();
	}, []);

	const ViewProduct = (item) => {
		history.push({
			pathname: `/productdetail/${item.id}`,
			data: item
		});
	};

	const openTryOn = () => {
		document.querySelector('.Virtual-Fitting-Room-Header-tab li:first-child').click();
		let header = document.querySelector('.powered-by').getBoundingClientRect().top;
		let modal = document.getElementById('tryOnContainer');
		if (header < -79) {
			modal.classList = 'fullPD-try-container';
		}
		modal.style.transform = 'translateX(0%)';
	};

	let selectColor = (e, el) => {
		let elements = document.querySelectorAll('.trendHeader-searchbar-btn');
		elements.forEach((el) => (el.style.border = '1px solid lightgray'));
		elements.forEach((el) => (el.style.boxShadow = 'none'));
		e.target.style.border = '1px solid #fff';
		e.target.style.boxShadow = ' 0 0 0 1.5px #f85454';
		sessionStorage.setItem('selectedColor', el);
		iso.arrange({
			filter: (itemElem) => {
				const tags = itemElem.getAttribute('data-tags');
				const colors = itemElem.getAttribute('data-color');
				const g = itemElem.getAttribute('data-gender');
				return g === gender && colors.includes(el) && findCommonElements(JSON.parse(tags), selectedTags);
			}
		});
	};

	const colorRef = useRef();

	const filterByTagIso = (tag, e) => {
		let selectedColor = '';
		let elements = document.querySelectorAll('.trendHeader-searchbar-btn');
		elements.forEach((el) => (el.style.border = '1px solid lightgray'));
		elements.forEach((el) => (el.style.boxShadow = 'none'));

		let tagTick = document.getElementsByClassName('tagTick');
		for (let i = 0; i < tagTick.length; i++) tagTick[i].style.display = 'none';

		if (selectedTags.includes(tag)) {
			e.currentTarget.classList.remove('TS-Activetags-container');
			const index = selectedTags.indexOf(tag);
			if (index > -1) selectedTags.splice(index, 1);
		} else {
			e.currentTarget.classList.add('TS-Activetags-container');
			document.getElementById(tag).style.display = 'block';
			selectedTags.push(tag);
		}

		sessionStorage.setItem('selectedTags', JSON.stringify(selectedTags));
		for (let i = 0; i < selectedTags.length; i++) document.getElementById(selectedTags[i]).style.display = 'block';
		if (selectedTags.length === 0) {
			document.querySelector('.color-pallete').style.display = 'none';
		} else {
			document.querySelector('.color-pallete').style.display = '';
		}

		let availableColors = [];

		for (let j = 0; j < selectedTags.length; j++) {
			let itms = document.getElementsByClassName('TS-images-section-outer-wrapper');
			let items = Array.from(itms);
			let filetredItems = items.filter((el) => el.getAttribute('data-tags').includes(selectedTags[j]));
			for (let i = 0; i < filetredItems.length; i++) {
				const Itemcolors = filetredItems[i].getAttribute('data-color');
				availableColors.push(JSON.parse(Itemcolors)[0]);
			}
		}
		colorRef.current.filterColorPalette([ ...new Set(availableColors) ]);

		iso.arrange({
			filter: (itemElem) => {
				const tags = itemElem.getAttribute('data-tags');
				const colors = itemElem.getAttribute('data-color');
				const g = itemElem.getAttribute('data-gender');

				let result;

				if (selectedTags.length === 0) {
					result = g === (sessionStorage.getItem('gender') ? sessionStorage.getItem('gender') : 'women');
					return result;
				} else {
					result =
						(selectedColor === '' ? true : colors.includes(selectedColor)) &&
						findCommonElements(JSON.parse(tags), selectedTags);
					return result;
				}
			}
		});
	};

	const findCommonElements = (arr1, arr2) => {
		let obj = {};
		for (let i = 0; i < arr1.length; i++) {
			if (!obj[arr1[i]]) {
				const element = arr1[i];
				obj[element] = true;
			}
		}
		for (let j = 0; j < arr2.length; j++) {
			if (obj[arr2[j]]) return true;
		}
		return false;
	};

	const tryOnRef = useRef();
	const tryonItem = (item) => {
		tryOnRef.current.changeTryItem(item);
		sessionStorage.setItem(item.category === 'tops' ? 'topId' : 'bottomId', item.id);
		sessionStorage.setItem(
			item.category === 'tops' ? 'top' : 'bottom',
			JSON.stringify({ name: item.name, price: item.price })
		);
		openTryOn();
	};

	const scrollEnd = () => {
		const elmnt = document.getElementById('PS-inner-container-btn-prev');
		if (elmnt) elmnt.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'center' });
	};

	return (
		<div>
			<section className='trendHeader'>
				<div style={{ display: 'flex', flexFlow: 'row wrap', justifyContent: 'flex-end' }}>
					<button
						style={{ position: 'fixed', zIndex: '10', padding: '0.5%', top: '9%', right: '5%' }}
						className=' mt-2 PS-inner-container-btn'
						onClick={(e) => {
							e.persist();
							scrollEnd(e);
						}}>
						Scroll To End
					</button>
				</div>
				<TryOn ref={tryOnRef} id={tryTopId} pairId={tryBottomId} gender={gender} tryonTAB={'Try on'} />
				<div className='trendHeader-outer-wrapper'>
					<div className='trendHeader-filter boldFont'>Filters</div>
					<div
						className='trendHeader-gender-category boldFont'
						id='headingText'
						style={{ textTransform: 'capitalize' }}>
						{gender}'s
					</div>
					<div className='trendHeader-searchbar' style={{ display: 'flex' }}>
						<span
							className='trendHeader-searchbar-wrapper'
							style={{ display: 'flex', alignItems: 'center' }}>
							<input type='text' className='boldFont' placeholder='search' />
							<button type='button' class='trendHeader-search-btn' />
						</span>
						<span style={{ marginLeft: '7%' }}>
							<img src={require('../../../assets/icons/ShoppingCart2.svg')} alt='' />
							{/* <button type='button' class='trendHeader-cart-btn' /> */}
						</span>
					</div>
				</div>
			</section>

			<div className='trendsetter-container'>
				<div className='trendsetter-clothing-section'>
					<div className='trendsetter-clothing-section-tab-outer'>
						<div className='trendsetter-clothing-section-tab-inner'>
							<span
								className='gender-tab-wrapper'
								onClick={() => {
									setGenderCheck('women');
									changeGender('women');
								}}>
								<input
									type='checkbox'
									name='womens-tab'
									id='womens-tab'
									value='women'
									checked={genderCheck === 'women' ? true : false}
								/>
								<span class='custom-checkbox' />
								<label className='boldFont' for='womens-tab'>
									Womens
								</label>
							</span>
						</div>
					</div>
					<div className='trendsetter-clothing-section-tab-outer'>
						<div className='trendsetter-clothing-section-tab-inner'>
							<span
								className='gender-tab-wrapper'
								onClick={() => {
									setGenderCheck('men');
									changeGender('men');
								}}>
								<input
									type='checkbox'
									name='mens-tab'
									id='mens-tab'
									value='men'
									checked={genderCheck === 'men' ? true : false}
								/>
								<span class='custom-checkbox' />
								<label className='boldFont' for='mens-tab'>
									Men's
								</label>
							</span>
						</div>
					</div>
				</div>

				<section className='flex col paddingZero'>
					<section id='TS-tags-section' className='TS-tags-section'>
						<div className='TS-tags-section-header'>Trend spotter</div>
						<ul className='flex wrap tagGrid' id='tagGrid'>
							{Tags.map((tag, index) => (
								<li
									className={'TS-tags-container'}
									data-tag-gender={tag.gender}
									onClick={(e) => filterByTagIso(tag.tag, e)}
									key={`tag${index}`}>
									<img
										className='tagTick'
										id={tag.tag}
										src={require('../../../assets/icons/tick.svg')}
										alt=''
									/>
									{tag.tag}
								</li>
							))}
						</ul>

						<div className='color-pallete'>
							<hr style={{ width: '90%' }} />
							<ColorPalette ref={colorRef} selectColor={(e, el) => selectColor(e, el)} />
						</div>
					</section>

					<section style={{ display: 'none' }} className='TS-images-section'>
						<div className='flex wrap grid' id='grid'>
							{/* {data.map((item, index) => {
								return (
									<div
										data-color={JSON.stringify(item.color_pallete)}
										data-tags={JSON.stringify(item.tags)}
										data-gender={item.gender}
										onClick={(e) => {
											e.persist();
											ViewProduct(item);
										}}
										className='TS-images-section-outer-wrapper'>
										<img
											className='TS-images'
											key={`item${index}`}
											src={item.main_url || item.img_url}
										/>
										{item.similar && (
											<img
												onClick={(e) => {
													e.stopPropagation();
													tryonItem(item);
												}}
												className='TS-images-tryon-button'
												src='../../../assets/icons/Product_Tag.svg'
												alt=''
											/>
										)}

										<div className='TS-images-details'>
											<div className='TS-images-name'>{item.name}</div>
											<div className='TS-images-price'>${item.price}.99</div>
										</div>
									</div>
								);
							})} */}
						</div>
					</section>
					<section>
						<TargetData history={history} />
					</section>
				</section>
			</div>
			{filterGender()}
		</div>
	);
};

export default TrendSpotter;
