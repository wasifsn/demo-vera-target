import React, { Component } from 'react';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import top from '../../assets/json/dummy';
import Slider from 'react-slick';
import reactCSS from 'reactcss';
import img from '../../assets/demoimgs/Screenshot_1585766016.png';
export default class SimpleSlider extends Component {
	constructor(props) {
		super(props);
		this.state = {
			recomSimilardata: null || this.props.recomSimilardata,
			newRecom: null
		};
	}

	computeRecomData = (data) => {
		let RecomArr = [];
		let key = Object.keys(data);
		for (let i in data[key]) {
			let subKey = Object.keys(data[key][i]);
			let metaKey = subKey[0];
			RecomArr.push({
				category: data[key][i][metaKey].category,
				similar_items: data[key][i][metaKey].similar_items
			});
		}
		console.log('RecomArr', RecomArr);
		// this.setState((prevState, props) => ({ newRecom: prevState.newRecom }));
		return RecomArr;
	};

	render() {
		var settings = {
			dots: true,
			infinite: true,
			speed: 500,
			slidesToShow: 1,
			variableWidth: true,
			slidesToScroll: 1
		};
		const styles = reactCSS({
			default: {
				dummy: {
					height: '30rem'
				},
				cutoutImg: {
					maxWidth: '50%',
					height: 'auto',
					margin: 'auto'
				},
				croppedImg: {
					maxWidth: '50%',
					height: 'auto',
					margin: 'auto'
				}
			}
		});
		// console.log(top);
		let obj = top.recommendationImages['1040010664_outerwear_1'];
		// let obj = this.computeRecomData(this.state.recomSimilardata);
		let newObj = this.computeRecomData(this.state.recomSimilardata);

		console.log('obj from carousel', obj);
		console.log('props for carousel', this.props);
		return (
			<Slider {...settings}>
				{newObj ? (
					newObj.map((item, index) => {
						return (
							<div key={index}>
								<div style={styles.dummy} className='container'>
									<div style={{ width: 500, zIndex: 2 }}>
										<div className='row justify-content-start align-items-end mb-3 mt-3'>
											<div className='col-3'>
												<img
													style={styles.cutoutImg}
													src={obj[3].similar_items[index].img_url}
													alt=''
												/>
											</div>
											<div className='col-6'>
												<img style={styles.croppedImg} src={this.props.mainImg} alt='' />
											</div>
										</div>
										<div className='row'>
											<div className='col-3 align-self-end'>
												<img
													style={styles.cutoutImg}
													src={newObj[1].similar_items[index].img_url}
													alt=''
												/>
											</div>
											<div className='col-6 justify-content-center'>
												<img
													style={styles.cutoutImg}
													src={newObj[2].similar_items[index].img_url}
													alt=''
												/>
											</div>
											<div className='col-3 align-self-end'>
												<img
													style={styles.cutoutImg}
													src={newObj[0].similar_items[index].img_url}
													alt=''
												/>
											</div>
										</div>
									</div>
								</div>
							</div>
						);
					})
				) : (
					<div>check</div>
				)}
				{/* <div>
					<div style={styles.dummy} className='container'>
						<div>
							<div className='row justify-content-start align-items-end mb-3 mt-3'>
								<div className='col-3'>
									<img style={styles.cutoutImg} src={obj[3].similar_items[0].img_url} alt='' />
								</div>
								<div className='col-6'>
									<img style={styles.croppedImg} src={img} alt='' />
								</div>
							</div>
							<div className='row'>
								<div className='col-3 align-self-end'>
									<img style={styles.cutoutImg} src={obj[1].similar_items[0].img_url} alt='' />
								</div>
								<div className='col-6 justify-content-center'>
									<img style={styles.cutoutImg} src={obj[2].similar_items[0].img_url} alt='' />
								</div>
								<div className='col-3 align-self-end'>
									<img style={styles.cutoutImg} src={obj[0].similar_items[0].img_url} alt='' />
								</div>
							</div>
						</div>
					</div>
				</div>
				<div>
					<div style={styles.dummy} className='container'>
						<div>
							<div className='row justify-content-start align-items-center mb-3 mt-3'>
								<div className='col-3'>
									<img style={styles.cutoutImg} src={obj[3].similar_items[1].img_url} alt='' />
								</div>
								<div className='col-6'>
									<img style={styles.croppedImg} src={img} alt='' />
								</div>
							</div>
							<div className='row'>
								<div className='col-3 align-self-end'>
									<img style={styles.cutoutImg} src={obj[1].similar_items[1].img_url} alt='' />
								</div>
								<div className='col-6 justify-content-center'>
									<img style={styles.cutoutImg} src={obj[2].similar_items[1].img_url} alt='' />
								</div>
								<div className='col-3 align-self-end'>
									<img style={styles.cutoutImg} src={obj[0].similar_items[1].img_url} alt='' />
								</div>
							</div>
						</div>
					</div>
				</div>
				<div>
					<div style={styles.dummy} className='container'>
						<div>
							<div className='row justify-content-start align-items-center mb-3 mt-3'>
								<div className='col-3'>
									<img style={styles.cutoutImg} src={obj[3].similar_items[2].img_url} alt='' />
								</div>
								<div className='col-6'>
									<img style={styles.cutoutImg} src={img} alt='' />
								</div>
							</div>
							<div className='row'>
								<div className='col-3 align-self-end'>
									<img style={styles.cutoutImg} src={obj[1].similar_items[2].img_url} alt='' />
								</div>
								<div className='col-6 justify-content-center'>
									<img style={styles.cutoutImg} src={obj[2].similar_items[2].img_url} alt='' />
								</div>
								<div className='col-3 align-self-end'>
									<img style={styles.cutoutImg} src={obj[0].similar_items[2].img_url} alt='' />
								</div>
							</div>
						</div>
					</div>
				</div>
				<div>
					<div style={styles.dummy} className='container'>
						<div>
							<div className='row justify-content-start align-items-center mb-3 mt-3'>
								<div className='col-3'>
									<img style={styles.cutoutImg} src={obj[3].similar_items[3].img_url} alt='' />
								</div>
								<div className='col-6'>
									<img style={styles.cutoutImg} src={img} alt='' />
								</div>
							</div>
							<div className='row'>
								<div className='col-3 align-self-end'>
									<img style={styles.cutoutImg} src={obj[1].similar_items[3].img_url} alt='' />
								</div>
								<div className='col-6 justify-content-center'>
									<img style={styles.cutoutImg} src={obj[2].similar_items[3].img_url} alt='' />
								</div>
								<div className='col-3 align-self-end'>
									<img style={styles.cutoutImg} src={obj[0].similar_items[3].img_url} alt='' />
								</div>
							</div>
						</div>
					</div>
				</div>
				<div>
					<div style={styles.dummy} className='container'>
						<div>
							<div className='row justify-content-start align-items-center mb-3 mt-3'>
								<div className='col-3'>
									<img style={styles.cutoutImg} src={obj[3].similar_items[4].img_url} alt='' />
								</div>
								<div className='col-6'>
									<img style={styles.cutoutImg} src={img} alt='' />
								</div>
							</div>
							<div className='row'>
								<div className='col-3 align-self-end'>
									<img style={styles.cutoutImg} src={obj[1].similar_items[4].img_url} alt='' />
								</div>
								<div className='col-6 justify-content-center'>
									<img style={styles.cutoutImg} src={obj[2].similar_items[4].img_url} alt='' />
								</div>
								<div className='col-3 align-self-end'>
									<img style={styles.cutoutImg} src={obj[0].similar_items[4].img_url} alt='' />
								</div>
							</div>
						</div>
					</div>
				</div>
				<div>
					<div style={styles.dummy} className='container'>
						<div>
							<div className='row justify-content-start align-items-center mb-3 mt-3'>
								<div className='col-3'>
									<img style={styles.cutoutImg} src={obj[3].similar_items[5].img_url} alt='' />
								</div>
								<div className='col-6'>
									<img style={styles.croppedImg} src={img} alt='' />
								</div>
							</div>
							<div className='row'>
								<div className='col-3 align-self-end'>
									<img style={styles.cutoutImg} src={obj[1].similar_items[5].img_url} alt='' />
								</div>
								<div className='col-6 justify-content-center'>
									<img style={styles.cutoutImg} src={obj[2].similar_items[5].img_url} alt='' />
								</div>
								<div className='col-3 align-self-end'>
									<img style={styles.cutoutImg} src={obj[0].similar_items[5].img_url} alt='' />
								</div>
							</div>
						</div>
					</div>
				</div> */}
			</Slider>
		);
	}
}
