import React, { Component } from 'react';
// import { Helmet } from 'react-helmet';
import Slider from 'react-slick';
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';
import Loader from 'react-loader-spinner';
import '../common/index.scss';
import { connect } from 'react-redux';
import axios from 'axios';
// import custom Components
// import Service from './common/service';
// import BrandBlock from './common/brand-block';
import NewProduct from '../common/new-product';
// import Breadcrumb from '../common/breadcrumb';
import DetailsWithPrice from './common/product/details-price';
// import DetailsTopTabs from './common/details-top-tabs';
// import { addToCart, addToCartUnsafe, addToWishlist } from '../../actions';
import ImageZoom from './common/product/image-zoom';
import SmallImages from './common/product/small-image';
import { recommendation } from '../../services/recommendations.js';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import _ from 'lodash';
import Popup from 'reactjs-popup';
import SimpleSlider from '../inspirations/carousel';
import Fullscreen from 'react-full-screen';
import MashedImage from './mashed-image';

import SimpleBarReact from 'simplebar-react';
import 'simplebar/src/simplebar.css';
import { Link, NavLink } from 'react-router-dom';
function SampleNextArrow(props) {
	const { className, style, onClick } = props;
	return (
		<div
			className={className}
			style={{
				...style,
				display: 'block',
				background: '#cfd2d4',
				borderRadius: '16px',
				fontSize: '41px'
			}}
			onClick={onClick}
		/>
	);
}

function SamplePrevArrow(props) {
	const { className, style, onClick } = props;
	return (
		<div
			className={className}
			style={{
				...style,
				display: 'block',
				background: '#cfd2d4',
				borderRadius: '16px'
			}}
			onClick={onClick}
		/>
	);
}

var settings = {
	dots: false,
	infinite: true,
	slidesToShow: 2,
	speed: 500,
	slidesPerRow: 2,
	slidesToScroll: 2,
	nextArrow: <SampleNextArrow />,
	prevArrow: <SamplePrevArrow />
};

class LeftSideBar extends Component {
	constructor(props) {
		super(props);
		this.state = {
			parentTextBoxValue: null,
			recommendation_new_data: [],
			category: 'top',
			mainImg: null,
			open: false,
			nav1: null,
			nav2: null,
			item: null,
			productId: this.props.match.params,
			recommendation: null,
			loader: true,
			newRecommendation: null,
			parsedData: null,
			isFull: false,
			tops: [],
			bottoms: [],
			targets: []
		};

		this.getProductDetail();
		this.getNewImagesBottom();
		this.getNewImagesTop();
		this.getNewImagestarget();
	}

	setmainImage = (img) => {
		this.state.mainImg = img;
	};

	goFull = () => {
		this.setState({ isFull: true });
	};

	async fetchRecommendationML() {
		try {
			const gender = this.state.item.data.result.gender;
			const mainImg = this.state.item.data.result.image_url;
			const category = this.state.category;
			console.log(mainImg);
			// const gender = this.state.item.data.result.gender;
			axios.defaults.headers.post['Content-Type'] = 'application/json';
			const response = await axios.post(
				// `https://apis.vera.ml/p1/recommendations?nitems=6&nsocial=5&gender=${gender}`,
				`https://apis.vera.ml/p1/recommendations?nitems=9&vendor=demo&social=social_crops&nsocial=1&gender=${gender}&mode=center&alt_categories=top,bottom`,
				{
					image_url: mainImg
				}
			);
			console.log('response from recommendation', response);
			let recommendation_new_data = [];
			if (response.data.recommendation_data) {
				for (let i in response.data.recommendation_data) {
					for (let j in response.data.recommendation_data[i]) {
						let loopScope = response.data.recommendation_data[i];
						console.log('loopScope', loopScope[j]);
						for (let k in loopScope[j]) {
							let innerLoopScope = loopScope[j];
							console.log(`innerLoopScope ${k}`, innerLoopScope[k]['item_data'].category);
							if (category !== innerLoopScope[k]['item_data'].category) {
								recommendation_new_data = innerLoopScope[k]['similar_items'];
							} else {
								recommendation_new_data = innerLoopScope[k]['similar_items'];
							}
						}
					}
				}
			}
			console.log('recommendation_new_data', recommendation_new_data);
			this.setState({ recommendation_new_data: recommendation_new_data });
			// ------------------- old code ------------------------------------------
			// this.setState({ newRecommendation: response.data.recommendation_data.n0 });
			// let recomData = this.state.newRecommendation;
			// let intermediateObjArr = [];
			// let secintermediateObjArr = [];
			// let finalArr = [];
			// let uniqueKeys = [];
			// for (let i in recomData) {
			// 	// console.log(i);
			// 	finalArr.push({ [i]: [] });
			// 	intermediateObjArr.push(Object.entries(recomData[i]));
			// }
			// for (let i of intermediateObjArr) {
			// 	for (let j of i) {
			// 		let temp = {
			// 			[j[0]]: {}
			// 		};
			// 		temp[j[0]].category = j[1].item_data.category;
			// 		temp[j[0]].similar_items = j[1].similar_items;
			// 		secintermediateObjArr.push(temp);
			// 	}
			// }
			// for (let getKey of finalArr) {
			// 	let data = Object.keys(getKey);
			// 	uniqueKeys.push(data[0]);
			// }

			// function printPhilosopherStatus(str1, str2) {
			// 	let str1Spl = str1.split('_');
			// 	let str2Spl = str2.split('_');
			// 	// console.log(str1Spl, str2Spl);
			// 	if (str1Spl[0] === str2Spl[0]) {
			// 		return true;
			// 	} else {
			// 		return false;
			// 	}
			// }

			// for (let i in finalArr) {
			// 	let key = Object.keys(finalArr[i]);
			// 	for (let j of secintermediateObjArr) {
			// 		if (printPhilosopherStatus(key[0], Object.keys(j)[0])) {
			// 			if (finalArr[i][key[0]].length < 6) {
			// 				finalArr[i][key[0]].push(j);
			// 			}
			// 		} else {
			// 			console.log('not pushed');
			// 		}
			// 	}
			// }
			// this.setState({ parsedData: finalArr });
			// console.log(finalArr);
		} catch (err) {
			this.setState({ newRecommendation: 'error' });
			console.log(err);
			this.props.history.push('/');
		}
	}

	getProductRecommendation = () => {
		let item = this.state.item.data.result;
		let body = { id: item._id, image_url: item.image_url };
		axios.post('/recommendation', body).then((res) => {
			if (res.data.error) {
				this.setState({ recommendation: recommendation });
			} else {
				this.setState({ recommendation: res.data.data });
			}
			console.log('old recommendation', this.state.recommendation);
			console.log('parsedData', this.state.parsedData);
		});
	};

	getProductDetail = () => {
		let sendPromise = axios.get('/product', {
			params: { id: this.state.productId.id }
		});
		sendPromise.then((itemDetail) => {
			this.setState({ item: itemDetail }, () => {
				this.getProductRecommendation();
			});
			this.setState({ mainImg: this.state.item.data.result.image_url });
			this.fetchRecommendationML();
		});
	};

	// document.getElementById('idOfElement').classList.add('newClassName');

	componentDidMount() {
		this.setState({
			nav1: this.slider1,
			nav2: this.slider2
		});
	}
	componentDidUpdate(prevProps, prevState) {
		if (prevProps.match.params.category !== this.state.category) {
			//Perform some operation here
			this.setState({ category: this.props.match.params.category });
			//   this.classMethod();
		}
	}

	filterClick() {
		document.getElementById('filter').style.left = '-15px';
	}
	backClick() {
		document.getElementById('filter').style.left = '-365px';
	}

	showLoader = () => {
		return (
			<div style={{ display: 'flex', justifyContent: 'center' }}>
				<Loader
					type='Puff'
					color='red'
					height={100}
					width={100}
					// timeout={3000} //3 secs
				/>
			</div>
		);
	};

	handleNewProductData = (e) => {
		this.setState({ parentTextBoxValue: e });
	};

	getNewImagesTop = async () => {
		let response = await axios.get('http://127.0.0.1:8000/tops');
		console.log(response);
		this.setState({ tops: response.data });
	};
	getNewImagesBottom = async () => {
		let response = await axios.get('http://127.0.0.1:8000/bottoms');
		this.setState({ bottoms: response.data });
	};
	getNewImagestarget = async () => {
		let response = await axios.get('http://127.0.0.1:8000/targets');
		this.setState({ targets: response.data });
	};
	render() {
		const {
			symbol,

			addToCart,
			addToCartUnsafe,
			addToWishlist
		} = this.props;
		let item;
		if (this.state.item != null) {
			item = this.state.item.data.result;
		}
		var products = {
			slidesToShow: 1,
			slidesToScroll: 1,
			dots: false,
			arrows: true,
			fade: true
		};
		var productsnav = {
			slidesToShow: 3,
			swipeToSlide: true,
			arrows: false,
			dots: true,
			focusOnSelect: true,
			nextArrow: <SampleNextArrow />,
			prevArrow: <SamplePrevArrow />
		};
		const contentStyle = {
			height: '90vh',
			width: '100%'
		};
		return (
			<SimpleBarReact style={{ maxHeight: '92vh' }}>
				<div>
					{item ? (
						<div>
							<div className='mt-3 mb-3'>
								<div className='collection-wrapper' style={{ height: '92vh' }}>
									<div className='container'>
										{/* NEW DESIGN */}
										<div className='row justify-content-start'>
											<NavLink
												to={{
													pathname: '/',
													state: {
														imageProps: {
															gender: 'female',
															page: 0,
															source: 'wallmart' // "wallmart" is missplelled
															// subcategory: el.replace(/-/gi, '').toString().toLowerCase()
														}
													}
												}}
												className='nav-link nav-link-2 m-2'>
												GO BACK
											</NavLink>
										</div>
										<div className='row box-size'>
											<div className='col-sm-9 '>
												<div className='row  justify-content-xl-around justify-content-lg-around justify-content-md-start justify-content-sm-start'>
													<div className='col-sm-3 subpage-mainimg-outer-wraper'>
														<ImageZoom
															image={item.image_url}
															onImageZoom={this.setmainImage}
														/>
													</div>
													{/* PAIR WITH */}
													<div className='col-sm-8 align-self-end'>
														<NewProduct
															bottoms={this.state.bottoms}
															handleMashUpData={this.handleNewProductData}
															history={this.props.history}
															title={'PAIR WITH'}
															recommendation_new_data={this.state.recommendation_new_data}
															item={item}
															mainImg={this.state.mainImg}
														/>
													</div>
												</div>
												{/* SIMILAR */}
												<div className='row justify-content-end'>
													<div className='col-sm-8 mr-xl-3 mr-lg-2 mr-md-4'>
														<NewProduct
															tops={this.state.tops}
															handleMashUpData={this.handleNewProductData}
															history={this.props.history}
															category={this.state.category}
															title={'SIMILAR'}
															item={item}
															mainImg={this.state.mainImg}
														/>
													</div>
												</div>
											</div>
											{/* MASHED UP IMAGE */}
											<div className='col-sm-3 align-self-stretch'>
												<MashedImage
													img={this.state.parentTextBoxValue}
													bottoms={this.state.targets}
												/>
												{/* <div className='row justify-content-center align-items-center mashupimage-outer-wraper'>
													<div className='mashupimage-inner-wraper'>
														<img
															className='img-fluid'
															src='https://i.pinimg.com/originals/e8/54/3b/e8543b438c985e06267c38a39035d2fb.jpg'
															alt=''
														/>
													</div>
													<div className='buynowbtn-outer-wraper'>
														<button class='btn btn-primary btn-lg'> Buy This Look</button>
													</div>
												</div> */}
											</div>
										</div>
										{/* <div className='row '> */}
										{/* BELOW is the main Image and its small images  */}
										{/* <div className='col-9 product-thumbnail'>
											<div
												style={{
													display: 'flex',
													justifyContent: 'center'
												}}>
												<ImageZoom image={item.image_url} onImageZoom={this.setmainImage} />
												<DetailsWithPrice
													symbol={symbol}
													item={item}
													navOne={this.state.nav1}
													addToCartClicked={addToCart}
													BuynowClicked={addToCartUnsafe}
													addToWishlistClicked={addToWishlist}
												/>
											</div>
											<div className='row'>
												<div className='col-6'>
													<SmallImages
														item={item}
														settings={productsnav}
														avOne={this.state.nav1}
													/>
												</div>
											</div>
										</div> */}
										{/* BELOW is the Similar items component */}
										{/* <div className='col-3 collection-filter' id='filter'>
											<div className='collection-mobile-back pl-5'>
												<span onClick={this.backClick} className='filter-back'>
													<i className='fa fa-angle-left' aria-hidden='true' /> back
												</span>
											</div> */}

										{/* <BrandBlock/> */}
										{/* <Service /> */}
										{/*side-bar single product slider start*/}
										{/* <NewProduct item={item} mainImg={this.state.mainImg} /> */}
										{/*side-bar single product slider end*/}
										{/* </div>
									</div>
									<div className='row'>
										{' '}
										<div className='col-lg-9 mt-5'> */}
										{/* <DetailsTopTabs item={item} /> */}
										{/* <div style={{ padding: '12px', fontSize: '24px' }}>Pair With-</div> */}
										{/* <Slider {...settings}>
												<Popup
													onClick={this.goFull}
													trigger={<img src={this.state.mainImg} />}
													modal
													position='center'
													closeOnDocumentClick>
													<SimpleSlider
														recomSimilardata={item}
														mainImg={this.state.mainImg}
													/>
												</Popup>
											</Slider> */}

										{/* {this.state.parsedData ? (
												<Slider {...settings} mainImg={this.state.mainImg}>
													{_.map(this.state.parsedData, (item, index) => {
														return (
															<div
																style={{
																	display: 'flex',
																	padding: '8px',
																	justifyContent: 'top center',
																	flexDirection: 'column'
																}}> */}
										{/* <img src={this.state.mainImg} alt='' /> */}
										{/* <Slider {...settings}>
																	<Popup
																		onClick={this.goFull}
																		trigger={<img src={this.state.mainImg} />}
																		modal
																		position='center center'
																		closeOnDocumentClick>
																		<SimpleSlider
																			recomSimilardata={item}
																			mainImg={this.state.mainImg}
																		/>
																	</Popup>
																</Slider> */}
										{/* {index} */}
										{/* </div> */}
										{/* );
													})}
												</Slider>
											) : (
												this.showLoader()
											)} */}
										{/* </div> */}
										{/* </div> */}
									</div>
								</div>
							</div>
						</div>
					) : (
						''
					)}
					{/*Section End*/}
				</div>
			</SimpleBarReact>
		);
	}
}

// const mapStateToProps = (state, ownProps) => {
//   return {
//     ownProps: ownProps
//   };
// };

export default LeftSideBar;
