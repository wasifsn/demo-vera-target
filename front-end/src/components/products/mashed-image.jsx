import React, { useState } from 'react';
import MashedImageAvatars from './mashed-image-avatars';
import { useEffect } from 'react';
import './styles.css';

export default function MashedImage({ id, targets, gender, top, bottom }) {
	const [ targetIndex, setTargetIndex ] = useState(1);
	const [ randomTagsWomen, setRandomTagsWomen ] = useState([ 'Casual Style!!!', 'Effortless Chic!!!', 'Basics!!!' ]);
	const [ randomTagsMen, setRandomTagsMen ] = useState([ 'Casual Style!!!', 'Effortless Dude!!!', 'Basics!!!' ]);
	const [ mashedSrc, setMashedSrc ] = useState('');
	const [ randomStyle, setRandomStyle ] = useState(randomTagsWomen[1]);
	const [ bookmark, setBookmark ] = useState(false);
	const [ savedImgs, setSavedImgs ] = useState(
		sessionStorage.getItem('savedItems') ? JSON.parse(sessionStorage.getItem('savedItems')).items : []
	);
	useEffect(
		() => {
			gender === 'women'
				? setMashedSrc(`/${gender}/try/?top=${id['tops']}&bottom=${id['bottoms']}&target=${targetIndex}`)
				: setMashedSrc(
						`/${gender}/try/?top=${id['tops']}&bottom=${id['bottoms']}&target=${targetIndex !== 3
							? targetIndex
							: 1}`
					);
			console.log(`/${gender}/try/?top=${id['tops']}&bottom=${id['bottoms']}&target=${targetIndex}`);
			setRandomStyle(
				gender === 'women'
					? randomTagsWomen[Math.floor(Math.random() * Math.floor(3))]
					: randomTagsMen[Math.floor(Math.random() * Math.floor(3))]
			);
		},
		[ id, targetIndex, gender ]
	);
	let addSavedOutfit = (e) => {
		if (!window.sessionStorage.getItem('savedItems')) {
			window.sessionStorage.setItem('savedItems', JSON.stringify({ items: [ mashedSrc ] }));
			setSavedImgs(JSON.parse(window.sessionStorage.getItem('savedItems')).items);
		} else {
			let temp = JSON.parse(window.sessionStorage.getItem('savedItems'));
			let newItems = [];
			let oldItems = temp.items;
			console.log(mashedSrc);
			newItems.push(...oldItems, mashedSrc);
			console.log(newItems);
			window.sessionStorage.setItem('savedItems', JSON.stringify({ items: newItems }));
			setSavedImgs(newItems);
		}
	};
	// if (document.querySelector('.mashedimg img')) document.querySelector('.mashedimg img').classList.add('transparent');
	return (
		<div>
			<div className='mashedimg-outer-wrapper' style={{ padding: '3% 0' }}>
				<div className='mashedimg' style={{ height: '55vh' }}>
					<div
						className='dummyHoverTop'
						onMouseEnter={() => (document.getElementById('detailPopupTop').style.display = 'block')}
						onMouseLeave={() => (document.getElementById('detailPopupTop').style.display = 'none')}>
						<div id='detailPopupTop' className='detailPopupTop'>
							<p className='popupName'>{top.name}</p>
							<p className='popupPrice'>${top.price}.99</p>
						</div>
					</div>
					<div
						className='dummyHoverBottom'
						onMouseEnter={() => (document.getElementById('detailPopupBottom').style.display = 'block')}
						onMouseLeave={() => (document.getElementById('detailPopupBottom').style.display = 'none')}>
						<div id='detailPopupBottom' className='detailPopupTop'>
							<p className='popupName'>{bottom.name}</p>
							<p className='popupPrice'>${bottom.price}.99</p>
						</div>
					</div>
					{id['tops'] !== '' && id['bottoms'] !== '' ? (
						<img src={mashedSrc} alt='' />
					) : (
						<img
							className='img-fluid'
							src={
								gender === 'women' ? (
									`https://i.pinimg.com/originals/81/97/b6/8197b611975badcbb718f62906862e7f.jpg`
								) : (
									`https://images-na.ssl-images-amazon.com/images/I/718hZ5nw2TL._SY500_.jpg`
								)
							}
							alt=''
						/>
					)}
					<span
						onClick={(e) => {
							e.persist();
							setBookmark(!bookmark);
							addSavedOutfit(e);
						}}
						// className={bookmark === true ? 'mashedimg-bookmark-icon-sel' : 'mashedimg-bookmark-icon-unsel'}>
						className={
							savedImgs.includes(mashedSrc) ? (
								'mashedimg-bookmark-icon-sel'
							) : (
								'mashedimg-bookmark-icon-unsel'
							)
						}>
						<svg width='16' height='22' viewBox='0 0 16 22' fill='none' xmlns='http://www.w3.org/2000/svg'>
							<path
								d='M13.7341 0.230469H2.96488C1.78027 0.230469 0.821805 1.31601 0.821805 2.64278L0.811035 21.9412L8.3495 18.3228L15.888 21.9412V2.64278C15.888 1.31601 14.9187 0.230469 13.7341 0.230469ZM13.7341 18.3228L8.3495 15.6934L2.96488 18.3228V2.64278H13.7341V18.3228Z'
								fill={savedImgs.includes(mashedSrc) ? '#FFFFFF' : '#F85454'}
							/>
						</svg>
					</span>

					{/* <i
						onClick={(e) => {
							e.persist();
							setBookmark(!bookmark);
						}}
						className={bookmark === true ? 'mashedimg-bookmark-icon-sel' : 'mashedimg-bookmark-icon-unsel'}
					/> */}
				</div>
				<div className='mashedimg-avatars-outer-wrapper'>
					<MashedImageAvatars
						gender={gender}
						updateAvatar={(el) => {
							// setBookmark(!bookmark);
							setTargetIndex(el);
						}}
						targets={targets}
					/>
				</div>
			</div>
		</div>
	);
}
