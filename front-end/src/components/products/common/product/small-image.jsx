import React, { Component } from 'react';
import Slider from 'react-slick';
import axios from 'axios';
class SmallImages extends Component {
	constructor(props) {
		super(props);
		this.state = {
			nav2: null
		};
	}
	componentDidMount() {
		this.setState({
			nav2: this.slider2
		});
	}

	render() {
		const { item, settings, mainImg } = this.props;

		var productsnav = settings;
		// console.log(item);

		return (
			<div className='row'>
				<div className='col-12 p-0'>
					<Slider
						{...productsnav}
						asNavFor={this.props.navOne}
						ref={(slider) => (this.slider2 = slider)}
						className='slider-nav'>
						{item.meta.alternate_image ? (
							item.meta.alternate_image.map((vari, index) => (
								<div key={index}>
									<img
										src={`http://${vari}`}
										key={index}
										alt=''
										className='img-fluid img-thumbnail'
									/>
								</div>
							))
						) : (
							item.pictures.map((vari, index) => (
								<div key={index}>
									<img src={`http://${vari}`} key={index} alt='' className='img-fluid' />
								</div>
							))
						)}
					</Slider>
				</div>
			</div>
		);
	}
}

export default SmallImages;
