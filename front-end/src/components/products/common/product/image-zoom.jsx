import React, { Component } from 'react';
import Popup from 'reactjs-popup';
import SimpleSlider from '../../../inspirations/carousel';
import top from '../../../../assets/json/dummy';
import axios from 'axios';
export default class ImageZoom extends Component {
	constructor(props) {
		super(props);
		this.state = {
			image: null,
			similar_items: []
		};
	}
	handleClick(e) {
		e.preventDefault();
		console.log('The link was clicked.');
	}
	sendImgToParent(image) {
		this.props.onImageZoom(image);
	}

	// async processRecommendations(image){}
	render() {
		const { image } = this.props;

		this.sendImgToParent(image);
		return (
			<div style={{ textAlign: 'center' }}>
				<img src={image} className='img-fluid image_zoom_cls-0' />
			</div>
			// <Popup
			// 	trigger={<img src={image} onClick={this.handleClick} className='img-fluid image_zoom_cls-0' />}
			// 	position='center'
			// 	modal
			// 	closeOnDocumentClick>
			// 	<SimpleSlider mainImg={this.props.image} />
			// </Popup>
		);
	}
}
