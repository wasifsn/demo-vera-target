import React, { useState } from 'react';
import BodyIcon from '../layouts/fashion/body-icon';

export default function MashedImageAvatars({ updateAvatar, targets, gender }) {
	const [ avatarBorder, setAvatarBorder ] = useState([ true, false, false ]);
	const [ avatarSel, setAvatarSel ] = useState(1);
	const selectAvatar = (e, id, index) => {
		e.persist();
		console.log(id, index);
		let img_number = index;
		let all_imgs_states = avatarBorder;
		all_imgs_states = all_imgs_states.map(() => false);
		all_imgs_states[img_number] = true;
		setAvatarBorder(all_imgs_states);
		updateAvatar(index + 1);
		// console.log(document.querySelector('.mashedimg img'));
		// document.querySelector('.mashedimg img').classList.toggle('transparent');
	};

	const isWindows = () => navigator.platform.indexOf('Win') > -1;

	let avatars =
		gender === 'women'
			? targets.slice(0, 3).map((el, index) => {
					// let items = document.querySelectorAll('.mashedimg-avatars-inner-wrapper');
					// if (items.length > 0) items[1].style.display = 'none';
					return (
						<div className='mashedimg-avatars-inner-wrapper' key={index}>
							{/* {index !== 1 ? ( */}
							<span
								data-avatar-img={index}
								onClick={(e) => {
									selectAvatar(e, el.id, index);
									setAvatarSel(index + 1);
								}}
								alt=''>
								<BodyIcon target={index + 1} selected={avatarSel} />
							</span>
							{/* <img
					className={
						avatarBorder[index] ? (
							'ps-inner-container-img-selected rounded-circle'
						) : (
							'rounded-circle avatar-opacity'
						)
					}
					data-avatar-img={index}
					src={
						isWindows() ? (
							require(`../../assets${el.img_url.replace(/\\/g, '/')}`)
						) : (
							require(`../../assets${el.img_url}`)
						)
					}
				
					size='70'
					onClick={(e) => selectAvatar(e, el.id, index)}
					alt=''
				/> */}
							{/* ) : (
					<div style={{ display: 'none' }} />
				)} */}
						</div>
					);
				})
			: targets.slice(0, 2).map((el, index) => {
					// let items = document.querySelectorAll('.mashedimg-avatars-inner-wrapper');
					// if (items.length > 0) items[1].style.display = 'none';
					return (
						<div className='mashedimg-avatars-inner-wrapper' key={index}>
							{/* {index !== 1 ? ( */}
							<span
								data-avatar-img={index}
								onClick={(e) => {
									selectAvatar(e, el.id, index);
									setAvatarSel(index + 1);
								}}
								alt=''>
								<BodyIcon target={index + 1} selected={avatarSel} />
							</span>
							{/* <img
					className={
						avatarBorder[index] ? (
							'ps-inner-container-img-selected rounded-circle'
						) : (
							'rounded-circle avatar-opacity'
						)
					}
					data-avatar-img={index}
					src={
						isWindows() ? (
							require(`../../assets${el.img_url.replace(/\\/g, '/')}`)
						) : (
							require(`../../assets${el.img_url}`)
						)
					}
				
					size='70'
					onClick={(e) => selectAvatar(e, el.id, index)}
					alt=''
				/> */}
							{/* ) : (
					<div style={{ display: 'none' }} />
				)} */}
						</div>
					);
				});

	return <div className='mashedimg-avatars-flex-wrapper'>{avatars}</div>;
}
