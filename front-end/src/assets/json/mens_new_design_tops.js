export const mens_tops = [
	{
		id: 1,
		main_url:
			'https://n.nordstrommedia.com/id/sr3/883cf024-8231-4cb2-a88b-a1a0ebc35dd6.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
		similar: [
			{
				id: 1,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/7dac1d04-e62b-4cbb-9e3a-15a62f17c184.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 2,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/f60f07bf-efa1-42b9-b79a-fd8d5f285bdf.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 3,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/3a1ef6e0-d7f7-450d-91c2-1c7285f3caeb.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 4,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/8667feee-ad47-46da-90da-d70b87bf320e.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			}
		],
		pair_with: [
			{
				id: 1,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/9e056b55-5d33-4a60-b844-d4de7e01d911.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 2,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/366ca271-3274-4c34-bcca-15d8a198d9a9.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 3,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/3482f913-91ca-4634-956a-5cbfb8a7ef70.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{ id: 4, img_url: 'https://n.nordstrommedia.com/id/sr3/257fda5f-1927-4b80-a484-f0fb1c3e92e9.jpeg?w=205' }
		],
		apparel_name: 'Blue Tshirt',
		apparel_price: 22
	},
	{
		id: 2,
		main_url:
			'https://n.nordstrommedia.com/id/sr3/f73ed3ae-5507-49fd-a2cc-772002629545.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
		similar: [
			{
				id: 1,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/8ba2e721-f238-4571-8097-69b4c420d048.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 2,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/b67181dd-3d6c-479e-bb2a-3ca6fdceebc0.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 3,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/e4d8fb4d-b32c-4676-a315-4edac2b717bb.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 4,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/16506764-ec34-44a4-a070-86d5ae0de0ef.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			}
		],
		pair_with: [
			{
				id: 1,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/3482f913-91ca-4634-956a-5cbfb8a7ef70.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{ id: 2, img_url: 'https://n.nordstrommedia.com/id/sr3/257fda5f-1927-4b80-a484-f0fb1c3e92e9.jpeg?w=205' },
			{
				id: 3,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/8a599378-ec99-4aa4-8f43-2be521722f6f.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 4,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/e134a9d3-3980-4698-9500-b747d85fae15.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			}
		],
		apparel_name: 'Black Tshirt',
		apparel_price: 32
	},
	{
		id: 3,
		main_url:
			'https://n.nordstrommedia.com/id/sr3/4adec333-a883-434d-8256-ab871cf3cd34.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
		similar: [
			{
				id: 1,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/5e70b2ee-bbf6-40c5-9284-50c27608100f.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 2,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/d5d1c06d-d55b-4ec9-a0f3-55b2ef405b20.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 3,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/8e9a046f-4aa7-4efe-bbe7-6dbac689825f.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 4,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/bf81b674-e3ef-412d-97e1-83b2a7f726c8.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			}
		],
		pair_with: [
			{
				id: 1,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/9e056b55-5d33-4a60-b844-d4de7e01d911.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 2,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/366ca271-3274-4c34-bcca-15d8a198d9a9.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{ id: 3, img_url: 'https://n.nordstrommedia.com/id/sr3/257fda5f-1927-4b80-a484-f0fb1c3e92e9.jpeg?w=205' },
			{
				id: 4,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/3482f913-91ca-4634-956a-5cbfb8a7ef70.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			}
		],
		apparel_name: 'Light Tshirt',
		apparel_price: 34
	},
	{
		id: 4,
		main_url:
			'https://n.nordstrommedia.com/id/sr3/20863eb4-38c6-4e57-9369-953f0d4933fa.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
		similar: [
			{
				id: 1,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/7380e3cf-7841-4255-9db1-3e7de2bd8720.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 2,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/97b9567e-8e18-4981-8c44-4651dc256a83.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 3,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/4fcb5b4b-bb79-4949-9f3a-d5d70bc9827d.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 4,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/6530aec5-b6cd-4784-a741-39cf2f73fed8.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			}
		],
		pair_with: [
			{
				id: 1,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/3482f913-91ca-4634-956a-5cbfb8a7ef70.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 2,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/366ca271-3274-4c34-bcca-15d8a198d9a9.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 3,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/8a599378-ec99-4aa4-8f43-2be521722f6f.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 4,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/e134a9d3-3980-4698-9500-b747d85fae15.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			}
		],
		apparel_name: 'FullSleeve Tshirt',
		apparel_price: 28
	},
	{
		id: 5,
		main_url:
			'https://n.nordstrommedia.com/id/sr3/4443d6e9-5059-43d4-b8fd-59c8ed8c5b18.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
		similar: [
			{
				id: 1,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/bf81b674-e3ef-412d-97e1-83b2a7f726c8.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 2,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/3a234c4e-94e4-4a78-9d2c-3aa5ce098f80.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 3,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/3f78b823-26f9-4b8e-9a08-33629bc40d31.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 4,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/81929ad0-3ec7-410a-9418-0a41a1cb2a31.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			}
		],
		pair_with: [
			{
				id: 1,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/366ca271-3274-4c34-bcca-15d8a198d9a9.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 2,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/d3532e4a-eddc-45a4-9a99-076b52029a1e.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{ id: 3, img_url: 'https://n.nordstrommedia.com/id/sr3/257fda5f-1927-4b80-a484-f0fb1c3e92e9.jpeg?w=205' },
			{
				id: 4,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/3482f913-91ca-4634-956a-5cbfb8a7ef70.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			}
		],
		apparel_name: 'Beige Tshirt',
		apparel_price: 59
	},
	{
		id: 6,
		main_url:
			'https://n.nordstrommedia.com/id/sr3/c9615c98-2499-4f99-95e8-60b9dd32c0d0.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
		similar: [
			{
				id: 1,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/7d8d9e9c-cdb5-46cf-945b-8bd635ea5ee9.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 2,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/f77cdbc2-0846-45b5-9221-e8327d59c646.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 3,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/20863eb4-38c6-4e57-9369-953f0d4933fa.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 4,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/919dd215-06fd-4bab-b49e-5e0daf67eca2.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			}
		],
		pair_with: [
			{
				id: 1,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/9e056b55-5d33-4a60-b844-d4de7e01d911.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{ id: 2, img_url: 'https://n.nordstrommedia.com/id/sr3/257fda5f-1927-4b80-a484-f0fb1c3e92e9.jpeg?w=205' },
			{
				id: 3,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/e134a9d3-3980-4698-9500-b747d85fae15.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 4,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/8c16b980-e950-4c9a-8854-34a8eb29eb2c.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			}
		],
		apparel_name: 'Dark Grey Tshirt',
		apparel_price: 58
	},
	{
		id: 7,
		main_url:
			'https://n.nordstrommedia.com/id/sr3/95ea2ca6-76f7-40dc-92eb-583bc6d96970.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
		similar: [ { id: 1, img_url: '' }, { id: 2, img_url: '' }, { id: 3, img_url: '' }, { id: 4, img_url: '' } ],
		pair_with: [
			{
				id: 1,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/e489f061-bf2a-4d50-9839-e4a8f31452ba.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 2,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/08fa77c9-3306-4133-aa95-18478f6d1296.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 3,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/4a64d910-8215-4e9f-8118-085a2b23311e.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 4,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/6de51e0a-d17c-4e73-b14e-9d0479da0f35.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			}
		],
		apparel_name: 'Check Shirt',
		apparel_price: 29
	},
	{
		id: 8,
		main_url:
			'https://n.nordstrommedia.com/id/sr3/57dcd0a3-98cf-4fea-be7a-b7f7797aadf3.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
		similar: [
			{ id: 1, img_url: 'https://n.nordstrommedia.com/id/sr3/ebc8fe65-fc87-44df-8d21-87a5a4fffc5f.jpeg?w=205' },
			{ id: 2, img_url: '' },
			{ id: 3, img_url: '' },
			{ id: 4, img_url: '' }
		],
		pair_with: [
			{
				id: 1,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/e489f061-bf2a-4d50-9839-e4a8f31452ba.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 2,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/08fa77c9-3306-4133-aa95-18478f6d1296.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 3,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/4a64d910-8215-4e9f-8118-085a2b23311e.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 4,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/6de51e0a-d17c-4e73-b14e-9d0479da0f35.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			}
		],
		apparel_name: 'Blue shirt',
		apparel_price: 37
	},
	{
		id: 9,
		main_url:
			'https://n.nordstrommedia.com/id/sr3/b2a1958c-3b73-4e69-b56a-8a74cc8d28fa.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
		similar: [ { id: 1, img_url: '' }, { id: 2, img_url: '' }, { id: 3, img_url: '' }, { id: 4, img_url: '' } ],
		pair_with: [
			{
				id: 1,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/9e056b55-5d33-4a60-b844-d4de7e01d911.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 2,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/d3532e4a-eddc-45a4-9a99-076b52029a1e.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 3,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/8a599378-ec99-4aa4-8f43-2be521722f6f.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 4,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/8c16b980-e950-4c9a-8854-34a8eb29eb2c.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			}
		],
		apparel_name: 'Brown Shirt',
		apparel_price: 51
	},
	{
		id: 10,
		main_url:
			'https://n.nordstrommedia.com/id/sr3/9bc52e96-2d69-426e-9675-cd4ff7069b55.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
		similar: [ { id: 1, img_url: '' }, { id: 2, img_url: '' }, { id: 3, img_url: '' }, { id: 4, img_url: '' } ],
		pair_with: [
			{
				id: 1,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/e489f061-bf2a-4d50-9839-e4a8f31452ba.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 2,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/d3532e4a-eddc-45a4-9a99-076b52029a1e.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 3,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/5c1e0bd1-e01c-4b6e-bb14-62b0de12f3c3.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 4,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/362be171-e170-441c-8b33-3dc5c130d1c3.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			}
		],
		apparel_name: 'White Shirt',
		apparel_price: 31
	},
	{
		id: 11,
		main_url:
			'https://n.nordstrommedia.com/id/sr3/a8d28f00-c02f-4732-a1f5-0791ed331ac6.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
		similar: [ { id: 1, img_url: '' }, { id: 2, img_url: '' }, { id: 3, img_url: '' }, { id: 4, img_url: '' } ],
		pair_with: [
			{
				id: 1,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/366ca271-3274-4c34-bcca-15d8a198d9a9.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 2,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/d3532e4a-eddc-45a4-9a99-076b52029a1e.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{ id: 3, img_url: 'https://n.nordstrommedia.com/id/sr3/257fda5f-1927-4b80-a484-f0fb1c3e92e9.jpeg?w=205' },
			{
				id: 4,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/35133956-22d1-4243-8512-8b81c59bb7b6.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			}
		],
		apparel_name: 'Blue Shirt',
		apparel_price: 20
	}
];
