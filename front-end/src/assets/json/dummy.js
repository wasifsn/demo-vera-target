// ------------------TOP-----------------------------

let top = {
	recommendationImages: {
		'1040011462_outerwear_1': [
			{
				category: 'bottom',
				similar_items: [
					{
						id: 'zalando3_TP721N0D3-K12@2',
						category: 'bottom',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/TP721N0D3-K12@2.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_GOM21N00J-K11@2.1',
						category: 'bottom',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/GOM21N00J-K11@2.1.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_TP721N0BW-A11@8',
						category: 'bottom',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/TP721N0BW-A11@8.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_DR121N031-K11@10',
						category: 'bottom',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/DR121N031-K11@10.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_C1Q21N005-A11@17.1',
						category: 'bottom',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/C1Q21N005-A11@17.1.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_TP721N0CZ-K13@12',
						category: 'bottom',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/TP721N0CZ-K13@12.jpg',
						source: 'zalando'
					}
				]
			},
			{
				category: 'footwear',
				similar_items: [
					{
						id: 'zalando3_AN611NA6W-B11@14.1',
						category: 'footwear',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/AN611NA6W-B11@14.1.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_M3211N01V-J11@9',
						category: 'footwear',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/M3211N01V-J11@9.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_GA111N0J2-B11@13',
						category: 'footwear',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/GA111N0J2-B11@13.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_PI911N05S-O11@3.1',
						category: 'footwear',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/PI911N05S-O11@3.1.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_M3111N0KP-B11@11',
						category: 'footwear',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/M3111N0KP-B11@11.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_GAJ11N000-O11@13',
						category: 'footwear',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/GAJ11N000-O11@13.jpg',
						source: 'zalando'
					}
				]
			}
		],
		'1040010664_outerwear_1': [
			{
				category: 'footwear',
				similar_items: [
					{
						id: 'zalando3_NL011A0NU-Q11@12',
						category: 'footwear',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/NL011A0NU-Q11@12.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_TP711N0A6-Q11@8',
						category: 'footwear',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/TP711N0A6-Q11@8.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_MF911N034-Q11@11',
						category: 'footwear',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/MF911N034-Q11@11.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_MAJ11N01D-Q11@12',
						category: 'footwear',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/MAJ11N01D-Q11@12.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_EV411N096-Q11@12',
						category: 'footwear',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/EV411N096-Q11@12.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_AI111N076-O11@13',
						category: 'footwear',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/AI111N076-O11@13.jpg',
						source: 'zalando'
					}
				]
			},
			{
				category: 'bag',
				similar_items: [
					{
						id: 'zalando3_ZI151H06F-Q11@10',
						category: 'bag',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/ZI151H06F-Q11@10.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_AN651H0KA-Q11@8',
						category: 'bag',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/AN651H0KA-Q11@8.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_EV451H0MI-Q11@11',
						category: 'bag',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/EV451H0MI-Q11@11.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_AN651H0J4-Q11@10',
						category: 'bag',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/AN651H0J4-Q11@10.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_AN651H0JR-Q11@14',
						category: 'bag',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/AN651H0JR-Q11@14.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_ST351H039-Q11@17',
						category: 'bag',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/ST351H039-Q11@17.jpg',
						source: 'zalando'
					}
				]
			},
			{
				category: 'bottom',
				similar_items: [
					{
						id: 'zalando3_QX721N00E-Q11@9.1',
						category: 'bottom',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/QX721N00E-Q11@9.1.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_LE221N062-C11@19.1',
						category: 'bottom',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/LE221N062-C11@19.1.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_LE221N061-K11@10',
						category: 'bottom',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/LE221N061-K11@10.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_WR121N04H-K11@22',
						category: 'bottom',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/WR121N04H-K11@22.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_QX721N008-K11@14',
						category: 'bottom',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/QX721N008-K11@14.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_TP721N0B9-C11@7',
						category: 'bottom',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/TP721N0B9-C11@7.jpg',
						source: 'zalando'
					}
				]
			},
			{
				category: 'eyewear',
				similar_items: [
					{
						id: 'zalando3_DO751K002-C11@8',
						category: 'eyewear',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/DO751K002-C11@8.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_PRB54K002-Q12@6',
						category: 'eyewear',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/PRB54K002-Q12@6.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_MJ451K00J-O11@6',
						category: 'eyewear',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/MJ451K00J-O11@6.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_MC154K001-Q12@10',
						category: 'eyewear',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/MC154K001-Q12@10.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_MJ451K00J-Q11@6',
						category: 'eyewear',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/MJ451K00J-Q11@6.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_1VE51K00X-Q11@8',
						category: 'eyewear',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/1VE51K00X-Q11@8.jpg',
						source: 'zalando'
					}
				]
			}
		],
		'1040016165_outerwear_1': [
			{
				category: 'bottom',
				similar_items: [
					{
						id: 'zalando3_M9121B121-M11@13',
						category: 'bottom',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/M9121B121-M11@13.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_BA221B001-C12@9',
						category: 'bottom',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/BA221B001-C12@9.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_M9121B12L-M11@11',
						category: 'bottom',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/M9121B12L-M11@11.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_VE121B0KS-Q11@14',
						category: 'bottom',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/VE121B0KS-Q11@14.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_M9121B126-E11@14',
						category: 'bottom',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/M9121B126-E11@14.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_RD521B01O-I11@16',
						category: 'bottom',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/RD521B01O-I11@16.jpg',
						source: 'zalando'
					}
				]
			},
			{
				category: 'bag',
				similar_items: [
					{
						id: 'zalando3_GL951H084-Q11@9',
						category: 'bag',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/GL951H084-Q11@9.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_LI351H0R2-Q11@8',
						category: 'bag',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/LI351H0R2-Q11@8.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_RO451H038-Q11@7',
						category: 'bag',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/RO451H038-Q11@7.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_CO351H0DT-Q11@10',
						category: 'bag',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/CO351H0DT-Q11@10.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_LY351H0CJ-Q11@2',
						category: 'bag',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/LY351H0CJ-Q11@2.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_AN651H0KA-Q11@8',
						category: 'bag',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/AN651H0KA-Q11@8.jpg',
						source: 'zalando'
					}
				]
			},
			{
				category: 'footwear',
				similar_items: [
					{
						id: 'zalando3_M3211A03I-G11@9',
						category: 'footwear',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/M3211A03I-G11@9.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_LA911N00U-G11@10',
						category: 'footwear',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/LA911N00U-G11@10.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_SA541A0BO-G11@10',
						category: 'footwear',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/SA541A0BO-G11@10.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_CL111N09F-G11@9',
						category: 'footwear',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/CL111N09F-G11@9.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_BR711N04M-Q11@14',
						category: 'footwear',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/BR711N04M-Q11@14.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_AX911N01X-Q11@2.1',
						category: 'footwear',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/AX911N01X-Q11@2.1.jpg',
						source: 'zalando'
					}
				]
			}
		],
		'1040018355_outerwear_1': [
			{
				category: 'footwear',
				similar_items: [
					{
						id: 'zalando3_K4411N07C-O11@10.1',
						category: 'footwear',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/K4411N07C-O11@10.1.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_DP511N07X-B11@11',
						category: 'footwear',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/DP511N07X-B11@11.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_AN611N0A8-O11@14',
						category: 'footwear',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/AN611N0A8-O11@14.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_AP211N015-O11@9',
						category: 'footwear',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/AP211N015-O11@9.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_UN111N046-O11@2',
						category: 'footwear',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/UN111N046-O11@2.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_P0G11N00X-B11@10',
						category: 'footwear',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/P0G11N00X-B11@10.jpg',
						source: 'zalando'
					}
				]
			},
			{
				category: 'bottom',
				similar_items: [
					{
						id: 'zalando3_GID21N01L-K12@13',
						category: 'bottom',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/GID21N01L-K12@13.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_WEB21N01M-A11@8',
						category: 'bottom',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/WEB21N01M-A11@8.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_SC321N00I-A11@13',
						category: 'bottom',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/SC321N00I-A11@13.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_L1221A03Q-A11@5',
						category: 'bottom',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/L1221A03Q-A11@5.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_WHC21N00E-K12@9',
						category: 'bottom',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/WHC21N00E-K12@9.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_QX721N008-A11@8',
						category: 'bottom',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/QX721N008-A11@8.jpg',
						source: 'zalando'
					}
				]
			},
			{
				category: 'neck_accessory',
				similar_items: [
					{
						id: 'zalando3_PRH51G009-O11@8',
						category: 'neck_accessory',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/PRH51G009-O11@8.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_B1251G0AZ-B11@5.1',
						category: 'neck_accessory',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/B1251G0AZ-B11@5.1.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_PRH51G00A-O11@10',
						category: 'neck_accessory',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/PRH51G00A-O11@10.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_ON351G08P-J11@2.1',
						category: 'neck_accessory',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/ON351G08P-J11@2.1.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_EL051L0OM-F11@35',
						category: 'neck_accessory',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/EL051L0OM-F11@35.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_FU151G00A-A11@6',
						category: 'neck_accessory',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/FU151G00A-A11@6.jpg',
						source: 'zalando'
					}
				]
			}
		],
		'1040012029_jacket_1': [
			{
				category: 'bottom',
				similar_items: [
					{
						id: 'zalando3_KT041I01D-N11@9',
						category: 'bottom',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/KT041I01D-N11@9.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_VE121A0TB-J11@2',
						category: 'bottom',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/VE121A0TB-J11@2.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_GP021S03D-K14@8',
						category: 'bottom',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/GP021S03D-K14@8.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_M9121A1QE-M11@12',
						category: 'bottom',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/M9121A1QE-M11@12.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_AD541E16D-Q11@10',
						category: 'bottom',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/AD541E16D-Q11@10.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_VM421N0FR-A11@15',
						category: 'bottom',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/VM421N0FR-A11@15.jpg',
						source: 'zalando'
					}
				]
			},
			{
				category: 'footwear',
				similar_items: [
					{
						id: 'zalando3_CHU11A00T-K11@13',
						category: 'footwear',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/CHU11A00T-K11@13.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_FAK11B008-M11@2.1',
						category: 'footwear',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/FAK11B008-M11@2.1.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_EV411B06H-M11@11',
						category: 'footwear',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/EV411B06H-M11@11.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_PU211B03C-K12@8',
						category: 'footwear',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/PU211B03C-K12@8.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_EV411A0G7-Q12@10',
						category: 'footwear',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/EV411A0G7-Q12@10.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_RUE11A00Q-M11@11',
						category: 'footwear',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/RUE11A00Q-M11@11.jpg',
						source: 'zalando'
					}
				]
			}
		],
		'1040009513_outerwear_1': [
			{
				category: 'bottom',
				similar_items: [
					{
						id: 'zalando3_S0821B01A-Q11@9',
						category: 'bottom',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/S0821B01A-Q11@9.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_F1421B01S-Q11@7',
						category: 'bottom',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/F1421B01S-Q11@7.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_MOQ21B01R-Q11@8.png',
						category: 'bottom',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/MOQ21B01R-Q11@8.png',
						source: 'zalando'
					},
					{
						id: 'zalando3_IV321B01S-K11@15',
						category: 'bottom',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/IV321B01S-K11@15.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_EV421B08O-Q11@13',
						category: 'bottom',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/EV421B08O-Q11@13.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_ES421B09L-Q11@11',
						category: 'bottom',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/ES421B09L-Q11@11.jpg',
						source: 'zalando'
					}
				]
			},
			{
				category: 'footwear',
				similar_items: [
					{
						id: 'zalando3_PRH11A09R-C11@12',
						category: 'footwear',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/PRH11A09R-C11@12.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_PRH11A09R-F11@13',
						category: 'footwear',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/PRH11A09R-F11@13.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_BJ011B03I-Q11@19',
						category: 'footwear',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/BJ011B03I-Q11@19.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_RAI11B00J-Q11@11',
						category: 'footwear',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/RAI11B00J-Q11@11.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_DOB11B024-C11@11',
						category: 'footwear',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/DOB11B024-C11@11.jpg',
						source: 'zalando'
					},
					{
						id: 'zalando3_OS411B006-Q11@7',
						category: 'footwear',
						gender: 'women',
						img_url:
							'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/OS411B006-Q11@7.jpg',
						source: 'zalando'
					}
				]
			}
		]
	},
	mainImgUrl: 'https://i.pinimg.com/236x/57/42/d4/5742d45e4e0c760b426915f222906ab6--style-outfits-style-clothes.jpg'
};

export default top;

let itemID_top = '1040009513_outerwear_1';

let mainImgUrl = 'https://i.pinimg.com/236x/57/42/d4/5742d45e4e0c760b426915f222906ab6--style-outfits-style-clothes.jpg';

let full_image_bbbox_values = {
	bottom: { left: 93, top: 179, width: 61, height: 122, centerFromLeft: 0, centerFromTop: 0 },
	outerwear: { left: 73, top: 83, width: 103, height: 98, centerFromLeft: 0, centerFromTop: 0 }
};

// ------------------BOTTOM-----------------------------

let recommendationImages_bottom = {
	'1040011973_pants_1': [
		{
			category: 'top',
			similar_items: [
				{
					id: 'zalando3_M3I21I08B-B11@18',
					category: 'top',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/M3I21I08B-B11@18.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_MYD21I001-J11@2',
					category: 'top',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/MYD21I001-J11@2.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_CL321I020-J11@10',
					category: 'top',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/CL321I020-J11@10.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_M1X21I00E-J11@22',
					category: 'top',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/M1X21I00E-J11@22.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_BY121I03G-I11@9',
					category: 'top',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/BY121I03G-I11@9.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_PE121D0GY-C11@17',
					category: 'top',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/PE121D0GY-C11@17.jpg',
					source: 'zalando'
				}
			]
		},
		{
			category: 'footwear',
			similar_items: [
				{
					id: 'zalando3_M4X11X001-C11@13',
					category: 'footwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/M4X11X001-C11@13.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_M3211N024-Q11@16',
					category: 'footwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/M3211N024-Q11@16.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_EV411N09J-Q11@10',
					category: 'footwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/EV411N09J-Q11@10.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_PI911N07W-Q11@9',
					category: 'footwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/PI911N07W-Q11@9.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_M3211N01V-Q11@5',
					category: 'footwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/M3211N01V-Q11@5.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_AN611N09Q-C11@2',
					category: 'footwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/AN611N09Q-C11@2.jpg',
					source: 'zalando'
				}
			]
		},
		{
			category: 'bag',
			similar_items: [
				{
					id: 'zalando3_CO351H0HW-Q11@4',
					category: 'bag',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/CO351H0HW-Q11@4.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_DE351H00S-Q11@13',
					category: 'bag',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/DE351H00S-Q11@13.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_PI351H0VD-Q11@6',
					category: 'bag',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/PI351H0VD-Q11@6.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_CO351H0HW-N11@2',
					category: 'bag',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/CO351H0HW-N11@2.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_JO551H05C-Q11@7',
					category: 'bag',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/JO551H05C-Q11@7.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_CO351H0EZ-Q11@13',
					category: 'bag',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/CO351H0EZ-Q11@13.jpg',
					source: 'zalando'
				}
			]
		}
	],
	'1040007040_pants_1': [
		{
			category: 'outerwear',
			similar_items: [
				{
					id: 'zalando3_VM421U06I-B11@24.2',
					category: 'outerwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/VM421U06I-B11@24.2.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_TO121U03B-B11@4.png',
					category: 'outerwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/TO121U03B-B11@4.png',
					source: 'zalando'
				},
				{
					id: 'zalando3_WP021U00J-C11@10.png',
					category: 'outerwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/WP021U00J-C11@10.png',
					source: 'zalando'
				},
				{
					id: 'zalando3_WL521U025-C11@5.png',
					category: 'outerwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/WL521U025-C11@5.png',
					source: 'zalando'
				},
				{
					id: 'zalando3_VE121U07S-C11@18',
					category: 'outerwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/VE121U07S-C11@18.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_TOB21U01L-Q11@15.1',
					category: 'outerwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/TOB21U01L-Q11@15.1.jpg',
					source: 'zalando'
				}
			]
		},
		{
			category: 'footwear',
			similar_items: [
				{
					id: 'zalando3_CO411A0ZM-A11@12',
					category: 'footwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/CO411A0ZM-A11@12.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_DI111A06A-Q11@12',
					category: 'footwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/DI111A06A-Q11@12.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_DP511A0H9-Q11@2',
					category: 'footwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/DP511A0H9-Q11@2.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_DP511A0H7-Q11@12',
					category: 'footwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/DP511A0H7-Q11@12.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_ST311N05W-A11@8',
					category: 'footwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/ST311N05W-A11@8.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_SK211A05E-Q11@10',
					category: 'footwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/SK211A05E-Q11@10.jpg',
					source: 'zalando'
				}
			]
		},
		{
			category: 'bag',
			similar_items: [
				{
					id: 'zalando3_DP551H0Q2-Q11@13',
					category: 'bag',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/DP551H0Q2-Q11@13.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_SV051Q007-Q11@12.1',
					category: 'bag',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/SV051Q007-Q11@12.1.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_TO251H0G3-C11@55092',
					category: 'bag',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/TO251H0G3-C11@55092.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_LI351H0QT-Q11@6',
					category: 'bag',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/LI351H0QT-Q11@6.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_TO251H0G8-Q11@14',
					category: 'bag',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/TO251H0G8-Q11@14.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_M0951H03N-Q11@16',
					category: 'bag',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/M0951H03N-Q11@16.jpg',
					source: 'zalando'
				}
			]
		}
	],
	'1040005109_pants_1': [
		{
			category: 'outerwear',
			similar_items: [
				{
					id: 'zalando3_SO821I02B-Q11@17',
					category: 'outerwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/SO821I02B-Q11@17.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_OB121G02R-K11@9',
					category: 'outerwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/OB121G02R-K11@9.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_VE121G0R0-Q11@22',
					category: 'outerwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/VE121G0R0-Q11@22.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_KA321G01N-K13@6.png',
					category: 'outerwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/KA321G01N-K13@6.png',
					source: 'zalando'
				},
				{
					id: 'zalando3_VE121U053-Q12@12.png',
					category: 'outerwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/VE121U053-Q12@12.png',
					source: 'zalando'
				},
				{
					id: 'zalando3_1FN21L005-Q11@2.1',
					category: 'outerwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/1FN21L005-Q11@2.1.jpg',
					source: 'zalando'
				}
			]
		},
		{
			category: 'footwear',
			similar_items: [
				{
					id: 'zalando3_GA111N0J9-Q11@4',
					category: 'footwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/GA111N0J9-Q11@4.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_H0511N01J-Q11@5',
					category: 'footwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/H0511N01J-Q11@5.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_SO211N09D-Q11@14',
					category: 'footwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/SO211N09D-Q11@14.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_RI111X00I-Q11@16.1',
					category: 'footwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/RI111X00I-Q11@16.1.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_PE211N04S-K11@7',
					category: 'footwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/PE211N04S-K11@7.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_DP511X00Q-Q11@12.1',
					category: 'footwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/DP511X00Q-Q11@12.1.jpg',
					source: 'zalando'
				}
			]
		},
		{
			category: 'bag',
			similar_items: [
				{
					id: 'zalando3_LI651H0EZ-Q11@8.1',
					category: 'bag',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/LI651H0EZ-Q11@8.1.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_31051H007-Q11@23',
					category: 'bag',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/31051H007-Q11@23.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_TO151H0EC-Q11@12',
					category: 'bag',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/TO151H0EC-Q11@12.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_1KI51H068-Q11@12',
					category: 'bag',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/1KI51H068-Q11@12.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_EV451Q00L-Q11@12',
					category: 'bag',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/EV451Q00L-Q11@12.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_MA351H0G3-Q11@2',
					category: 'bag',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/MA351H0G3-Q11@2.jpg',
					source: 'zalando'
				}
			]
		},
		{
			category: 'bottom',
			similar_items: [
				{
					id: 'zalando3_AN621A04J-Q11@2',
					category: 'bottom',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/AN621A04J-Q11@2.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_DP521N075-Q11@10',
					category: 'bottom',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/DP521N075-Q11@10.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_Z1721N00C-Q11@19',
					category: 'bottom',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/Z1721N00C-Q11@19.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_DP521N07Z-C11@12',
					category: 'bottom',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/DP521N07Z-C11@12.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_MOQ21A02B-Q11@10',
					category: 'bottom',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/MOQ21A02B-Q11@10.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_DP521N07R-Q11@7',
					category: 'bottom',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/DP521N07R-Q11@7.jpg',
					source: 'zalando'
				}
			]
		},
		{
			category: 'top',
			similar_items: [
				{
					id: 'zalando3_JC421E00J-K11@10',
					category: 'top',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/JC421E00J-K11@10.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_MW721D02Y-Q11@2',
					category: 'top',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/MW721D02Y-Q11@2.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_MA321E0RV-K11@14',
					category: 'top',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/MA321E0RV-K11@14.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_JY121E0BK-M11@16',
					category: 'top',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/JY121E0BK-M11@16.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_TOB21E01S-Q11@2.png',
					category: 'top',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/TOB21E01S-Q11@2.png',
					source: 'zalando'
				},
				{
					id: 'zalando3_M9121E2ZE-A11@10',
					category: 'top',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/M9121E2ZE-A11@10.jpg',
					source: 'zalando'
				}
			]
		}
	],
	'1040006744_pants_1': [
		{
			category: 'top',
			similar_items: [
				{
					id: 'zalando3_VE121E1PJ-A11@14.1',
					category: 'top',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/VE121E1PJ-A11@14.1.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_M3I21E0DN-A11@18',
					category: 'top',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/M3I21E0DN-A11@18.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_SE321E01T-A11@8',
					category: 'top',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/SE321E01T-A11@8.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_M9121E2W7-A11@9',
					category: 'top',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/M9121E2W7-A11@9.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_M3I21E0FM-A11@17',
					category: 'top',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/M3I21E0FM-A11@17.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_CO121E0OT-A11@9.png',
					category: 'top',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/CO121E0OT-A11@9.png',
					source: 'zalando'
				}
			]
		},
		{
			category: 'head_accessory',
			similar_items: [
				{
					id: 'zalando3_P1451B00V-A11@10',
					category: 'head_accessory',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/P1451B00V-A11@10.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_PS951B001-C11@10.1',
					category: 'head_accessory',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/PS951B001-C11@10.1.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_S1981M00R-A11@4.1',
					category: 'head_accessory',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/S1981M00R-A11@4.1.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_PS951B005-G11@7',
					category: 'head_accessory',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/PS951B005-G11@7.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_P1451B00V-Q11@10',
					category: 'head_accessory',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/P1451B00V-Q11@10.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_S1951E003-A11@10',
					category: 'head_accessory',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/S1951E003-A11@10.jpg',
					source: 'zalando'
				}
			]
		},
		{
			category: 'footwear',
			similar_items: [
				{
					id: 'zalando3_AN611N0A4-B11@12',
					category: 'footwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/AN611N0A4-B11@12.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_M3211N01V-J11@9',
					category: 'footwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/M3211N01V-J11@9.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_WL511N01S-B11@2',
					category: 'footwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/WL511N01S-B11@2.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_ZI111N0E2-B11@12',
					category: 'footwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/ZI111N0E2-B11@12.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_K4411N075-B11@7',
					category: 'footwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/K4411N075-B11@7.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_AN611N088-A11@7',
					category: 'footwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/AN611N088-A11@7.jpg',
					source: 'zalando'
				}
			]
		},
		{
			category: 'bag',
			similar_items: [
				{
					id: 'zalando3_M9151H141-O11@10',
					category: 'bag',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/M9151H141-O11@10.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_AN651H0J0-O11@10',
					category: 'bag',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/AN651H0J0-O11@10.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_AN651H0L9-O11@12.1',
					category: 'bag',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/AN651H0L9-O11@12.1.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_M9151H141-O12@15',
					category: 'bag',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/M9151H141-O12@15.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_AN651H0HL-O11@12',
					category: 'bag',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/AN651H0HL-O11@12.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_AB251H0F0-O11@8',
					category: 'bag',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/AB251H0F0-O11@8.jpg',
					source: 'zalando'
				}
			]
		}
	],
	'1040003161_pants_1': [
		{
			category: 'top',
			similar_items: [
				{
					id: 'zalando3_VE121E1PJ-A11@14.1',
					category: 'top',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/VE121E1PJ-A11@14.1.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_M3I21E0DN-A11@18',
					category: 'top',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/M3I21E0DN-A11@18.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_SE321E01T-A11@8',
					category: 'top',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/SE321E01T-A11@8.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_M9121E2W7-A11@9',
					category: 'top',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/M9121E2W7-A11@9.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_M3I21E0FM-A11@17',
					category: 'top',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/M3I21E0FM-A11@17.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_CO121E0OT-A11@9.png',
					category: 'top',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/CO121E0OT-A11@9.png',
					source: 'zalando'
				}
			]
		},
		{
			category: 'head_accessory',
			similar_items: [
				{
					id: 'zalando3_P1451B00V-A11@10',
					category: 'head_accessory',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/P1451B00V-A11@10.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_PS951B001-C11@10.1',
					category: 'head_accessory',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/PS951B001-C11@10.1.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_S1981M00R-A11@4.1',
					category: 'head_accessory',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/S1981M00R-A11@4.1.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_PS951B005-G11@7',
					category: 'head_accessory',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/PS951B005-G11@7.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_P1451B00V-Q11@10',
					category: 'head_accessory',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/P1451B00V-Q11@10.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_S1951E003-A11@10',
					category: 'head_accessory',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/S1951E003-A11@10.jpg',
					source: 'zalando'
				}
			]
		},
		{
			category: 'footwear',
			similar_items: [
				{
					id: 'zalando3_AN611N0A4-B11@12',
					category: 'footwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/AN611N0A4-B11@12.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_M3211N01V-J11@9',
					category: 'footwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/M3211N01V-J11@9.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_WL511N01S-B11@2',
					category: 'footwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/WL511N01S-B11@2.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_ZI111N0E2-B11@12',
					category: 'footwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/ZI111N0E2-B11@12.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_K4411N075-B11@7',
					category: 'footwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/K4411N075-B11@7.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_AN611N088-A11@7',
					category: 'footwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/AN611N088-A11@7.jpg',
					source: 'zalando'
				}
			]
		},
		{
			category: 'bag',
			similar_items: [
				{
					id: 'zalando3_M9151H141-O11@10',
					category: 'bag',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/M9151H141-O11@10.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_AN651H0J0-O11@10',
					category: 'bag',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/AN651H0J0-O11@10.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_AN651H0L9-O11@12.1',
					category: 'bag',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/AN651H0L9-O11@12.1.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_M9151H141-O12@15',
					category: 'bag',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/M9151H141-O12@15.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_AN651H0HL-O11@12',
					category: 'bag',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/AN651H0HL-O11@12.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_AB251H0F0-O11@8',
					category: 'bag',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/AB251H0F0-O11@8.jpg',
					source: 'zalando'
				}
			]
		},
		{
			category: 'neck_accessory',
			similar_items: [
				{
					id: 'zalando3_PI851L0C1-F11@5.1',
					category: 'neck_accessory',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/PI851L0C1-F11@5.1.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_L4251G05C-J11@4',
					category: 'neck_accessory',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/L4251G05C-J11@4.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_TO151L04I-J11@19',
					category: 'neck_accessory',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/TO151L04I-J11@19.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_DAS51L00N-F11@12',
					category: 'neck_accessory',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/DAS51L00N-F11@12.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_T0751G004-J11@5',
					category: 'neck_accessory',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/T0751G004-J11@5.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_PI851L07Z-F11@6.1',
					category: 'neck_accessory',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/PI851L07Z-F11@6.1.jpg',
					source: 'zalando'
				}
			]
		},
		{
			category: 'outerwear',
			similar_items: [
				{
					id: 'zalando3_MB121G00S-J11@20.1.png',
					category: 'outerwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/MB121G00S-J11@20.1.png',
					source: 'zalando'
				},
				{
					id: 'zalando3_M9121G0QU-B11@13',
					category: 'outerwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/M9121G0QU-B11@13.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_MB121G00S-M12@16',
					category: 'outerwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/MB121G00S-M12@16.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_VE121G0S3-J11@13',
					category: 'outerwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/VE121G0S3-J11@13.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_VE121G0RP-J11@15',
					category: 'outerwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/VE121G0RP-J11@15.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_M9121G0QK-B11@15',
					category: 'outerwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/M9121G0QK-B11@15.jpg',
					source: 'zalando'
				}
			]
		},
		{
			category: 'arm_accessory',
			similar_items: [
				{
					id: 'zalando3_DID51L00C-F11@6',
					category: 'arm_accessory',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/DID51L00C-F11@6.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_VE051M01E-K11@2.2',
					category: 'arm_accessory',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/VE051M01E-K11@2.2.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_GU151M03N-F11@2.1',
					category: 'arm_accessory',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/GU151M03N-F11@2.1.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_GU151M000-M11@8',
					category: 'arm_accessory',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/GU151M000-M11@8.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_VIG52L00I-F11@10',
					category: 'arm_accessory',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/VIG52L00I-F11@10.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_DID51L00C-D11@8',
					category: 'arm_accessory',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/DID51L00C-D11@8.jpg',
					source: 'zalando'
				}
			]
		}
	],
	'1040010208_pants_1': [
		{
			category: 'footwear',
			similar_items: [
				{
					id: 'zalando3_AD111A0WK-C11@9',
					category: 'footwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/AD111A0WK-C11@9.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_AD541A1C7-A12@2',
					category: 'footwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/AD541A1C7-A12@2.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_AD541A1CW-C11@12',
					category: 'footwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/AD541A1CW-C11@12.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_AD111A0S6-A11@6',
					category: 'footwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/AD111A0S6-A11@6.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_AD111A0SX-A11@12.1',
					category: 'footwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/AD111A0SX-A11@12.1.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_AD111A0YD-A12@9',
					category: 'footwear',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/shoes/images/AD111A0YD-A12@9.jpg',
					source: 'zalando'
				}
			]
		},
		{
			category: 'bag',
			similar_items: [
				{
					id: 'zalando3_PRH51H00J-Q12@12',
					category: 'bag',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/PRH51H00J-Q12@12.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_TO251A00J-802@1.1',
					category: 'bag',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/TO251A00J-802@1.1.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_AN651H0JP-Q11@9',
					category: 'bag',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/AN651H0JP-Q11@9.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_DE151H0XY-Q11@10',
					category: 'bag',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/DE151H0XY-Q11@10.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_TO151H0OD-Q11@2',
					category: 'bag',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/TO151H0OD-Q11@2.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_F1151H0QI-Q11@9',
					category: 'bag',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/accessories/images/F1151H0QI-Q11@9.jpg',
					source: 'zalando'
				}
			]
		},
		{
			category: 'top',
			similar_items: [
				{
					id: 'zalando3_SO821J00H-B11@7.png',
					category: 'top',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/SO821J00H-B11@7.png',
					source: 'zalando'
				},
				{
					id: 'zalando3_Y0121I05O-N11@14',
					category: 'top',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/Y0121I05O-N11@14.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_M9121I1HZ-C11@14',
					category: 'top',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/M9121I1HZ-C11@14.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_M9121J0AK-C11@11',
					category: 'top',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/M9121J0AK-C11@11.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_R0021I05J-C11@8',
					category: 'top',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/R0021I05J-C11@8.jpg',
					source: 'zalando'
				},
				{
					id: 'zalando3_VM421J03L-C11@13',
					category: 'top',
					gender: 'women',
					img_url:
						'https://storage.googleapis.com/automl-datasets/zalando/women/clothing/images/VM421J03L-C11@13.jpg',
					source: 'zalando'
				}
			]
		}
	]
};

let itemID_bottom = '1040010208_pants_1"';

let full_image_bbbox_values_bottom = {
	bottom: { left: 93, top: 179, width: 61, height: 122, centerFromLeft: 0, centerFromTop: 0 },
	outerwear: { left: 73, top: 83, width: 103, height: 98, centerFromLeft: 0, centerFromTop: 0 }
};
