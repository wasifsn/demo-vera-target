export const mens_bottoms = [
	{
		id: 1,
		main_url:
			'https://n.nordstrommedia.com/id/sr3/9e056b55-5d33-4a60-b844-d4de7e01d911.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
		similar: [ { id: 1, img_url: '' }, { id: 2, img_url: '' }, { id: 3, img_url: '' }, { id: 4, img_url: '' } ],
		pair_with: [ { id: 1, img_url: '' }, { id: 2, img_url: '' }, { id: 3, img_url: '' }, { id: 4, img_url: '' } ],
		apparel_name: 'Grey Pant',
		apparel_price: 56
	},
	{
		id: 2,
		main_url:
			'https://n.nordstrommedia.com/id/sr3/366ca271-3274-4c34-bcca-15d8a198d9a9.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
		similar: [ { id: 1, img_url: '' }, { id: 2, img_url: '' }, { id: 3, img_url: '' }, { id: 4, img_url: '' } ],
		pair_with: [ { id: 1, img_url: '' }, { id: 2, img_url: '' }, { id: 3, img_url: '' }, { id: 4, img_url: '' } ],
		apparel_name: 'White Pant',
		apparel_price: 40
	},
	{
		id: 3,
		main_url:
			'https://n.nordstrommedia.com/id/sr3/3482f913-91ca-4634-956a-5cbfb8a7ef70.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
		similar: [ { id: 1, img_url: '' }, { id: 2, img_url: '' }, { id: 3, img_url: '' }, { id: 4, img_url: '' } ],
		pair_with: [ { id: 1, img_url: '' }, { id: 2, img_url: '' }, { id: 3, img_url: '' }, { id: 4, img_url: '' } ],
		apparel_name: 'Blue Short',
		apparel_price: 53
	},
	{
		id: 4,
		main_url: 'https://n.nordstrommedia.com/id/sr3/257fda5f-1927-4b80-a484-f0fb1c3e92e9.jpeg?w=205',
		similar: [ { id: 1, img_url: '' }, { id: 2, img_url: '' }, { id: 3, img_url: '' }, { id: 4, img_url: '' } ],
		pair_with: [ { id: 1, img_url: '' }, { id: 2, img_url: '' }, { id: 3, img_url: '' }, { id: 4, img_url: '' } ],
		apparel_name: 'OffWhite Shorts',
		apparel_price: 45
	},
	{
		id: 5,
		main_url:
			'https://n.nordstrommedia.com/id/sr3/8a599378-ec99-4aa4-8f43-2be521722f6f.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
		similar: [ { id: 1, img_url: '' }, { id: 2, img_url: '' }, { id: 3, img_url: '' }, { id: 4, img_url: '' } ],
		pair_with: [ { id: 1, img_url: '' }, { id: 2, img_url: '' }, { id: 3, img_url: '' }, { id: 4, img_url: '' } ],
		apparel_name: 'Denim Pant',
		apparel_price: 24
	},
	{
		id: 6,
		main_url:
			'https://n.nordstrommedia.com/id/sr3/e134a9d3-3980-4698-9500-b747d85fae15.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
		similar: [ { id: 1, img_url: '' }, { id: 2, img_url: '' }, { id: 3, img_url: '' }, { id: 4, img_url: '' } ],
		pair_with: [ { id: 1, img_url: '' }, { id: 2, img_url: '' }, { id: 3, img_url: '' }, { id: 4, img_url: '' } ],
		apparel_name: 'Denim pant',
		apparel_price: 28
	},
	{
		id: 7,
		main_url:
			'https://n.nordstrommedia.com/id/sr3/6de51e0a-d17c-4e73-b14e-9d0479da0f35.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
		similar: [ { id: 1, img_url: '' }, { id: 2, img_url: '' }, { id: 3, img_url: '' }, { id: 4, img_url: '' } ],
		pair_with: [ { id: 1, img_url: '' }, { id: 2, img_url: '' }, { id: 3, img_url: '' }, { id: 4, img_url: '' } ],
		apparel_name: 'Beige Pant',
		apparel_price: 57
	},
	{
		id: 8,
		main_url:
			'https://n.nordstrommedia.com/id/sr3/e489f061-bf2a-4d50-9839-e4a8f31452ba.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
		similar: [ { id: 1, img_url: '' }, { id: 2, img_url: '' }, { id: 3, img_url: '' }, { id: 4, img_url: '' } ],
		pair_with: [ { id: 1, img_url: '' }, { id: 2, img_url: '' }, { id: 3, img_url: '' }, { id: 4, img_url: '' } ],
		apparel_name: 'Grey Pant',
		apparel_price: 34
	},
	{
		id: 9,
		main_url:
			'https://n.nordstrommedia.com/id/sr3/08fa77c9-3306-4133-aa95-18478f6d1296.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
		similar: [ { id: 1, img_url: '' }, { id: 2, img_url: '' }, { id: 3, img_url: '' }, { id: 4, img_url: '' } ],
		pair_with: [ { id: 1, img_url: '' }, { id: 2, img_url: '' }, { id: 3, img_url: '' }, { id: 4, img_url: '' } ],
		apparel_name: 'Black Pant',
		apparel_price: 56
	}
];
