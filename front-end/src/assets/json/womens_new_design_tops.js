export const womens_tops = [
	{
		id: 1,
		main_url:
			'https://n.nordstrommedia.com/id/sr3/2359334d-6693-4158-a791-3cf11f1c7e66.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
		similar: [
			{
				id: 1,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/c981cf77-81f5-4a5a-9593-db7c963fcdb4.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 2,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/c6149556-8e41-4de5-a99c-04dbbcfc5520.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 3,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/f5ecd184-c7b8-411e-a448-3884a62055ec.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 4,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/2359334d-6693-4158-a791-3cf11f1c7e66.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			}
		],
		pair_with: [
			{
				id: 1,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/cc082b52-8e07-4566-85d1-55c932e798bb.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{ id: 2, img_url: 'https://n.nordstrommedia.com/id/sr3/a0d3337c-1be2-4177-b9c2-1620f0271697.jpeg?w=205' },
			{
				id: 3,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/47719dd3-df6c-4e69-b9b3-9d9d88fd7680.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 4,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/3f10b06e-e104-4304-95fd-9fff1fdef89c.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			}
		],
		apparel_name: 'Pink Top',
		apparel_price: 40
	},
	{
		id: 2,
		main_url:
			'https://n.nordstrommedia.com/id/sr3/71d12ff0-c70f-41f5-af9c-19d7f24b6050.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
		similar: [
			{ id: 1, img_url: 'https://n.nordstrommedia.com/id/sr3/72cb366f-368e-424e-ae39-fb9405101f06.jpeg?w=205' },
			{
				id: 2,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/315a70b1-4d9c-4ac3-8569-f562f6dde473.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 3,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/b0e2c4f0-aa32-4d14-99a8-e7a18ad971eb.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 4,
				img_url: 'https://n.nordstrommedia.com/id/sr3/72cb366f-368e-424e-ae39-fb9405101f06.jpeg?w=1000'
			}
		],
		pair_with: [
			{ id: 1, img_url: 'https://n.nordstrommedia.com/id/sr3/a1c59060-a459-4537-b404-5cc405d1c0c0.jpeg?w=205' },
			{
				id: 2,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/f19d0b9d-232e-478f-8cf2-d8eac7113e2c.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 3,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/3f10b06e-e104-4304-95fd-9fff1fdef89c.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 4,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/f0909e54-f621-411c-bda1-4fa92bb26c0c.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			}
		],
		apparel_name: 'Black Top',
		apparel_price: 45
	},
	{
		id: 3,
		main_url:
			'https://n.nordstrommedia.com/id/sr3/1bc109eb-8f88-463e-978e-f1cad7c52e97.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
		similar: [
			{ id: 1, img_url: 'https://n.nordstrommedia.com/id/sr3/fd204308-7d2d-4aa6-b936-63eeb111169d.jpeg?w=205' },
			{
				id: 2,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/750eb411-c82b-4383-9ba0-c71875da4806.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 3,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/3281f35e-5c24-4b99-9671-02bc11274fa4.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 4,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/f5646e13-6cf3-44d0-9d05-150b6574e0e2.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			}
		],
		pair_with: [
			{
				id: 1,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/88431eb6-3985-4438-a1b4-5cc82b4b8265.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 2,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/47719dd3-df6c-4e69-b9b3-9d9d88fd7680.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 3,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/9e54ae68-2987-43d8-b811-b52b7fa95c46.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 4,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/f0909e54-f621-411c-bda1-4fa92bb26c0c.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			}
		],
		apparel_name: 'dotted Top',
		apparel_price: 23
	},
	{
		id: 4,
		main_url:
			'https://n.nordstrommedia.com/id/sr3/4838e59a-6bb8-4242-b2ea-7318adc83490.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
		similar: [
			{
				id: 1,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/51165a0b-0a52-422b-bb3e-f8f3a9906469.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 2,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/c7629ebc-a077-4cf4-b8e0-6e9daeb80c6e.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 3,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/4a4bb25e-9d6c-4a9c-ba9b-e9c6857e3fb2.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 4,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/1215f670-f8ef-458e-9123-421ec67b432d.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			}
		],
		pair_with: [
			{
				id: 1,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/88431eb6-3985-4438-a1b4-5cc82b4b8265.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 2,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/9e54ae68-2987-43d8-b811-b52b7fa95c46.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 3,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/a78c55cd-b12c-432c-a69f-d39ab847f841.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 4,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/f0909e54-f621-411c-bda1-4fa92bb26c0c.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			}
		],
		apparel_name: 'black Tshirt',
		apparel_price: 46
	},
	{
		id: 5,
		main_url:
			'https://n.nordstrommedia.com/id/sr3/07bddc54-0f28-4ff9-ac82-084fb5eddbc1.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
		similar: [
			{ id: 1, img_url: 'https://n.nordstrommedia.com/id/sr3/46a4f18f-a647-4233-83b8-4b3e795682be.jpeg?w=205' },
			{
				id: 2,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/dd5dea92-80ac-4a59-89bf-9f3671200f66.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 3,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/12c51394-b763-48c8-af87-e3259e879707.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 4,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/864f220b-803e-4160-b1db-05f823c178ba.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			}
		],
		pair_with: [
			{ id: 1, img_url: 'https://n.nordstrommedia.com/id/sr3/a0d3337c-1be2-4177-b9c2-1620f0271697.jpeg?w=205' },
			{ id: 2, img_url: 'https://n.nordstrommedia.com/id/sr3/482f567e-4541-4890-a3ea-2424c45151a8.jpeg?w=205' },
			{
				id: 3,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/e72b8cf3-f9c0-4f33-933b-718493d6327f.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 4,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/9e54ae68-2987-43d8-b811-b52b7fa95c46.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			}
		],
		apparel_name: 'White Tshirt',
		apparel_price: 34
	},
	{
		id: 6,
		main_url:
			'https://n.nordstrommedia.com/id/sr3/e81df747-747d-4b77-930e-826801c66dda.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
		similar: [
			{ id: 1, img_url: 'https://n.nordstrommedia.com/id/sr3/7f8adb8b-d453-4271-a8ab-0d5f60196a81.jpeg?w=205' },
			{ id: 2, img_url: 'https://n.nordstrommedia.com/id/sr3/770a764d-01f6-4b7c-9a00-f09abd74531e.jpeg?w=205' },
			{
				id: 3,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/d5eb5be7-e73e-464f-8641-f26011854e78.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 4,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/cf1fddac-9644-40fd-be90-c350ea4ce8f7.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			}
		],
		pair_with: [
			{ id: 1, img_url: 'https://n.nordstrommedia.com/id/sr3/a0d3337c-1be2-4177-b9c2-1620f0271697.jpeg?w=205' },
			{
				id: 2,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/47719dd3-df6c-4e69-b9b3-9d9d88fd7680.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 3,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/9e54ae68-2987-43d8-b811-b52b7fa95c46.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 4,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/f0909e54-f621-411c-bda1-4fa92bb26c0c.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			}
		],
		apparel_name: 'Sleeveless Top',
		apparel_price: 25
	},
	{
		id: 7,
		main_url:
			'https://n.nordstrommedia.com/id/sr3/8cb75133-b3b1-40e4-a854-90e4e8183b56.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
		similar: [
			{ id: 1, img_url: 'https://n.nordstrommedia.com/id/sr3/9d6b059f-a986-4564-8ea1-ca74f3ae70ac.jpeg?w=205' },
			{
				id: 2,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/9db1e439-3480-4b8e-8ae7-e332a6d56c31.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 3,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/50515934-cdd2-43b0-9dfe-39447eeba380.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 4,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/6511d4ea-dafd-46e1-a66b-96a4861a7eb1.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			}
		],
		pair_with: [
			{ id: 1, img_url: 'https://n.nordstrommedia.com/id/sr3/a0d3337c-1be2-4177-b9c2-1620f0271697.jpeg?w=205' },
			{ id: 2, img_url: 'https://n.nordstrommedia.com/id/sr3/482f567e-4541-4890-a3ea-2424c45151a8.jpeg?w=205' },
			{
				id: 3,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/3f10b06e-e104-4304-95fd-9fff1fdef89c.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 4,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/fd1f8cd1-db63-45d2-a644-94620d750f82.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			}
		],
		apparel_name: 'White Tops',
		apparel_price: 24
	},
	{
		id: 8,
		main_url:
			'https://n.nordstrommedia.com/id/sr3/1a2516e5-b944-45cc-801a-0348b6039026.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
		similar: [
			{ id: 1, img_url: 'https://n.nordstrommedia.com/id/sr3/021703e5-c120-44fc-9d1e-6880820a72b1.jpeg?w=205' },
			{
				id: 2,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/dcb26ce7-6f38-4427-a087-3116971a290c.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 3,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/1438194a-ddb2-4bfd-b326-be49cc541aa7.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 4,
				img_url: ''
			}
		],
		pair_with: [
			{
				id: 1,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/cc082b52-8e07-4566-85d1-55c932e798bb.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 2,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/47719dd3-df6c-4e69-b9b3-9d9d88fd7680.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 3,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/3f10b06e-e104-4304-95fd-9fff1fdef89c.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 4,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/fd1f8cd1-db63-45d2-a644-94620d750f82.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			}
		],
		apparel_name: 'OffWhite Top',
		apparel_price: 58
	}
];
