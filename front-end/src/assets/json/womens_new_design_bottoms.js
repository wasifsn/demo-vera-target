export const womens_bottoms = [
	{
		id: 1,
		main_url:
			'https://n.nordstrommedia.com/id/sr3/cc082b52-8e07-4566-85d1-55c932e798bb.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
		similar: [
			{
				id: 1,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/8d054ada-d7e9-4aab-9ac8-4806d7750704.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 2,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/f59fa3a7-18f1-4b22-9364-a93359b4170b.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 3,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/b9ad9d55-849c-466e-bc08-451add3810bc.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 4,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/c65451b7-fd3d-4a62-bb79-ee3ce2de10c2.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			}
		],
		pair_with: [
			{
				id: 1,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/22aaf571-7b8d-4a45-9bbe-00ac735ee5ca.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 2,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/315a70b1-4d9c-4ac3-8569-f562f6dde473.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 3,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/e81df747-747d-4b77-930e-826801c66dda.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{ id: 4, img_url: 'https://n.nordstrommedia.com/id/sr3/021703e5-c120-44fc-9d1e-6880820a72b1.jpeg?w=205' }
		],
		apparel_name: 'white Short',
		apparel_price: 23
	},
	{
		id: 2,
		main_url: 'https://n.nordstrommedia.com/id/sr3/a0d3337c-1be2-4177-b9c2-1620f0271697.jpeg?w=205',
		similar: [
			{
				id: 1,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/79d7d0c0-75f2-45a5-83f0-73741cb8fcd7.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196'
			},
			{
				id: 2,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/cef02a48-d8fe-446b-8b77-049a280f1c38.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 3,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/a34b5174-b55f-4a2d-8e21-a59428bb6e5c.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 4,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/9c429e89-e1f0-4ce3-b39b-19ae6d89b75d.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			}
		],
		pair_with: [
			{
				id: 1,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/ea9c9bb3-62a2-4387-96bd-35eb9180eba4.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=1660&h=2546'
			},
			{
				id: 2,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/8cb75133-b3b1-40e4-a854-90e4e8183b56.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{ id: 3, img_url: 'https://n.nordstrommedia.com/id/sr3/770a764d-01f6-4b7c-9a00-f09abd74531e.jpeg?w=205' },
			{
				id: 4,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/07bddc54-0f28-4ff9-ac82-084fb5eddbc1.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			}
		],
		apparel_name: 'Denim Shorts',
		apparel_price: 54
	},
	{
		id: 3,
		main_url:
			'https://n.nordstrommedia.com/id/sr3/47719dd3-df6c-4e69-b9b3-9d9d88fd7680.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
		similar: [
			{
				id: 1,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/8f306173-14c0-47c6-8969-ed3684ee23c8.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 2,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/623d89cd-9f23-481f-b705-0c4027e41bec.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 3,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/4a7c8f99-494d-44d0-944f-51e633787249.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 4,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/7fd79daf-2357-4d60-a694-79778ce7e826.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			}
		],
		pair_with: [
			{
				id: 1,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/22aaf571-7b8d-4a45-9bbe-00ac735ee5ca.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 2,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/2359334d-6693-4158-a791-3cf11f1c7e66.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 3,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/71d12ff0-c70f-41f5-af9c-19d7f24b6050.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 4,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/750eb411-c82b-4383-9ba0-c71875da4806.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			}
		],
		apparel_name: 'Long Skirt',
		apparel_price: 20
	},
	{
		id: 4,
		main_url:
			'https://n.nordstrommedia.com/id/sr3/3f10b06e-e104-4304-95fd-9fff1fdef89c.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
		similar: [
			{
				id: 1,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/0ee6dd84-3360-42d0-9ca4-b6fc7e06faeb.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 2,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/4636ed12-7c5d-4fd3-a157-bcadeba08b95.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 3,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/2ae7112f-dba6-4a11-ba20-6dd2bbcf9d8e.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 4,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/1b9ecaa1-d328-439e-9a32-abdb6abc6113.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			}
		],
		pair_with: [
			{
				id: 1,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/1bc109eb-8f88-463e-978e-f1cad7c52e97.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 2,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/4838e59a-6bb8-4242-b2ea-7318adc83490.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{ id: 3, img_url: 'https://n.nordstrommedia.com/id/sr3/46a4f18f-a647-4233-83b8-4b3e795682be.jpeg?w=205' },
			{ id: 4, img_url: 'https://n.nordstrommedia.com/id/sr3/770a764d-01f6-4b7c-9a00-f09abd74531e.jpeg?w=205' }
		],
		apparel_name: 'Denim Jeans',
		apparel_price: 43
	},
	{
		id: 5,
		main_url: 'https://n.nordstrommedia.com/id/sr3/a1c59060-a459-4537-b404-5cc405d1c0c0.jpeg?w=205',
		similar: [
			{
				id: 1,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/5385dbec-4d93-4895-8920-69c927fe5745.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 2,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/9ee57bdd-d4fc-4270-a24c-4e27242a2f7d.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 3,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/c564638c-e85c-40dd-be92-0d30b97d1ac9.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 4,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/d00089af-87c4-4e9f-b955-e5b648021c83.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			}
		],
		pair_with: [
			{
				id: 1,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/8cb75133-b3b1-40e4-a854-90e4e8183b56.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 2,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/6511d4ea-dafd-46e1-a66b-96a4861a7eb1.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 3,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/864f220b-803e-4160-b1db-05f823c178ba.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{ id: 4, img_url: 'https://n.nordstrommedia.com/id/sr3/72cb366f-368e-424e-ae39-fb9405101f06.jpeg?w=205' }
		],
		apparel_name: 'Black Shorts',
		apparel_price: 45
	},
	{
		id: 6,
		main_url:
			'https://n.nordstrommedia.com/id/sr3/f19d0b9d-232e-478f-8cf2-d8eac7113e2c.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
		similar: [
			{
				id: 1,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/efd59909-21fd-4c94-bc62-69bada9a852d.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 2,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/252bcbfb-07fb-43d6-b95e-a9e6b72eda28.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 3,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/5edd145b-8862-4aa3-93be-5c3c28713af4.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 4,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/087a8c55-5f5e-4459-9520-3c11b4029be0.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			}
		],
		pair_with: [
			{ id: 1, img_url: 'https://n.nordstrommedia.com/id/sr3/770a764d-01f6-4b7c-9a00-f09abd74531e.jpeg?w=205' },
			{
				id: 2,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/1438194a-ddb2-4bfd-b326-be49cc541aa7.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 3,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/c6149556-8e41-4de5-a99c-04dbbcfc5520.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 4,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/2359334d-6693-4158-a791-3cf11f1c7e66.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			}
		],
		apparel_name: 'Long Skirt',
		apparel_price: 55
	},
	{
		id: 7,
		main_url:
			'https://n.nordstrommedia.com/id/sr3/f0909e54-f621-411c-bda1-4fa92bb26c0c.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
		similar: [
			{
				id: 1,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/f0909e54-f621-411c-bda1-4fa92bb26c0c.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 2,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/f2f9aa5d-168a-4ef3-98fc-f52327d178b2.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 3,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/fd1f8cd1-db63-45d2-a644-94620d750f82.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 4,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/822d9b9b-a046-4571-8698-8f171feaf0b3.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			}
		],
		pair_with: [
			{
				id: 1,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/71d12ff0-c70f-41f5-af9c-19d7f24b6050.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 2,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/3281f35e-5c24-4b99-9671-02bc11274fa4.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 3,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/4a4bb25e-9d6c-4a9c-ba9b-e9c6857e3fb2.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 4,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/e81df747-747d-4b77-930e-826801c66dda.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			}
		],
		apparel_name: 'Black Pant',
		apparel_price: 42
	},
	{
		id: 8,
		main_url:
			'https://n.nordstrommedia.com/id/sr3/f52edd7a-1666-4efb-abcd-3e4fd559dae2.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
		similar: [
			{
				id: 1,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/6db84c17-d552-4fae-aa90-2d91048f8bb7.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 2,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/88f8963b-d1cb-49a8-bf41-32990ceaee40.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 3,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/16262fd7-cce6-4ab9-b37f-56f3b47446e7.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 4,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/82958022-c4ed-49d1-9972-7c947cc8e18b.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			}
		],
		pair_with: [
			{
				id: 1,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/6511d4ea-dafd-46e1-a66b-96a4861a7eb1.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 2,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/97fa8fb9-f6e2-4918-bb74-6fdfd6dcf1a7.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{ id: 3, img_url: 'https://n.nordstrommedia.com/id/sr3/021703e5-c120-44fc-9d1e-6880820a72b1.jpeg?w=205' },
			{
				id: 4,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/dcb26ce7-6f38-4427-a087-3116971a290c.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			}
		],
		apparel_name: 'Beige Pants',
		apparel_price: 47
	},
	{
		id: 9,
		main_url:
			'https://n.nordstrommedia.com/id/sr3/88431eb6-3985-4438-a1b4-5cc82b4b8265.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
		similar: [
			{
				id: 1,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/ad4aa2de-f671-46ef-aad7-122b1f4a76b6.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 2,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/5e755a7a-2708-4aa9-a14d-3535d499ec00.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 3,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/1f0badff-5ba3-46d2-86b5-32462bbe6200.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 4,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/e401a366-f7e0-427c-97f8-529df914772e.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			}
		],
		pair_with: [
			{
				id: 1,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/8cb75133-b3b1-40e4-a854-90e4e8183b56.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{
				id: 2,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/97fa8fb9-f6e2-4918-bb74-6fdfd6dcf1a7.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			},
			{ id: 3, img_url: 'https://n.nordstrommedia.com/id/sr3/021703e5-c120-44fc-9d1e-6880820a72b1.jpeg?w=205' },
			{
				id: 4,
				img_url:
					'https://n.nordstrommedia.com/id/sr3/dcb26ce7-6f38-4427-a087-3116971a290c.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
			}
		],
		apparel_name: 'Denim Shorts',
		apparel_price: 58
	}
];
