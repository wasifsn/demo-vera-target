export const womens_bottomNew = [
	{
		id: 'cc082b52-8e07-4566-85d1-55c932e798bb',
		img_url: '/static/data/women/bottoms/cc082b52-8e07-4566-85d1-55c932e798bb/image.jpg',
		apparel_name: 'White Shorts',
		apparel_price: '34',
		pair: [ 'cream full sleve', 'black tee', 'white tee', 'floral shirt' ],
		similar: [ 'white shorts', 'white shorts', 'white shorts', 'white shorts' ],
		similars: [
			'https://n.nordstrommedia.com/id/sr3/8d054ada-d7e9-4aab-9ac8-4806d7750704.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
			'https://n.nordstrommedia.com/id/sr3/f59fa3a7-18f1-4b22-9364-a93359b4170b.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
			'https://n.nordstrommedia.com/id/sr3/b9ad9d55-849c-466e-bc08-451add3810bc.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
			'https://n.nordstrommedia.com/id/sr3/c65451b7-fd3d-4a62-bb79-ee3ce2de10c2.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
		],
		pairwith: [
			{
				id: '22aaf571-7b8d-4a45-9bbe-00ac735ee5ca',
				url: '/static/data/women/tops/22aaf571-7b8d-4a45-9bbe-00ac735ee5ca/image.jpg'
			},
			{
				id: '315a70b1-4d9c-4ac3-8569-f562f6dde473',
				url: '/static/data/women/tops/315a70b1-4d9c-4ac3-8569-f562f6dde473/image.jpg'
			},
			{
				id: 'e81df747-747d-4b77-930e-826801c66dda',
				url: '/static/data/women/tops/e81df747-747d-4b77-930e-826801c66dda/image.jpg'
			},
			{
				id: '021703e5-c120-44fc-9d1e-6880820a72b1',
				url: '/static/data/women/tops/021703e5-c120-44fc-9d1e-6880820a72b1/image.jpg'
			}
		]
	},

	{
		id: 'a0d3337c-1be2-4177-b9c2-1620f0271697',
		img_url: '/static/data/women/bottoms/a0d3337c-1be2-4177-b9c2-1620f0271697/image.jpg',
		apparel_name: 'Denim Shorts',
		apparel_price: '46',
		pair: [ 'black sleeveless', 'white sleeveless', 'grey sleeveless', 'white tee' ],
		similar: [ 'denim shorts', 'denim shorts', 'denim shorts', 'denim shorts' ],
		similars: [
			'https://n.nordstrommedia.com/id/sr3/79d7d0c0-75f2-45a5-83f0-73741cb8fcd7.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=1660&h=2546',
			'https://n.nordstrommedia.com/id/sr3/cef02a48-d8fe-446b-8b77-049a280f1c38.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
			'https://n.nordstrommedia.com/id/sr3/a34b5174-b55f-4a2d-8e21-a59428bb6e5c.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
			'https://n.nordstrommedia.com/id/sr3/9c429e89-e1f0-4ce3-b39b-19ae6d89b75d.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
		],
		pairwith: [
			{
				id: 'ea9c9bb3-62a2-4387-96bd-35eb9180eba4',
				url: '/static/data/women/tops/ea9c9bb3-62a2-4387-96bd-35eb9180eba4/image.jpg'
			},
			{
				id: '8cb75133-b3b1-40e4-a854-90e4e8183b56',
				url: '/static/data/women/tops/8cb75133-b3b1-40e4-a854-90e4e8183b56/image.jpg'
			},
			{
				id: '770a764d-01f6-4b7c-9a00-f09abd74531e',
				url: '/static/data/women/tops/770a764d-01f6-4b7c-9a00-f09abd74531e/image.jpg'
			},
			{
				id: '07bddc54-0f28-4ff9-ac82-084fb5eddbc1',
				url: '/static/data/women/tops/07bddc54-0f28-4ff9-ac82-084fb5eddbc1/image.jpg'
			}
		]
	},
	{
		id: '47719dd3-df6c-4e69-b9b3-9d9d88fd7680',
		img_url: '/static/data/women/bottoms/47719dd3-df6c-4e69-b9b3-9d9d88fd7680/image.jpg',
		apparel_name: 'Long Skirt',
		apparel_price: '50',
		pair: [ 'cream full sleve', 'cream tee', 'black half sleve', 'pink half sleve' ],
		similar: [ 'black skirt', 'black skirt', 'black skirt', 'black skirt' ],
		similars: [
			'https://n.nordstrommedia.com/id/sr3/8f306173-14c0-47c6-8969-ed3684ee23c8.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
			'https://n.nordstrommedia.com/id/sr3/623d89cd-9f23-481f-b705-0c4027e41bec.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
			'https://n.nordstrommedia.com/id/sr3/4a7c8f99-494d-44d0-944f-51e633787249.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
			'https://n.nordstrommedia.com/id/sr3/7fd79daf-2357-4d60-a694-79778ce7e826.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
		],
		pairwith: [
			{
				id: '22aaf571-7b8d-4a45-9bbe-00ac735ee5ca',
				url: '/static/data/women/tops/22aaf571-7b8d-4a45-9bbe-00ac735ee5ca/image.jpg'
			},
			{
				id: '2359334d-6693-4158-a791-3cf11f1c7e66',
				url: '/static/data/women/tops/2359334d-6693-4158-a791-3cf11f1c7e66/image.jpg'
			},
			{
				id: '71d12ff0-c70f-41f5-af9c-19d7f24b6050',
				url: '/static/data/women/tops/71d12ff0-c70f-41f5-af9c-19d7f24b6050/image.jpg'
			},
			{
				id: '750eb411-c82b-4383-9ba0-c71875da4806',
				url: '/static/data/women/tops/750eb411-c82b-4383-9ba0-c71875da4806/image.jpg'
			}
		]
	},
	{
		id: '3f10b06e-e104-4304-95fd-9fff1fdef89c',
		img_url: '/static/data/women/bottoms/3f10b06e-e104-4304-95fd-9fff1fdef89c/image.jpg',
		apparel_name: 'Denim Jeans',
		apparel_price: '58',
		pair: [ 'dotted shirt', 'black tee', 'white tee', 'grey tee' ],
		similar: [ 'denim pant', 'denim pant', 'denim pant', 'denim pant' ],
		similars: [
			'https://n.nordstrommedia.com/id/sr3/0ee6dd84-3360-42d0-9ca4-b6fc7e06faeb.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
			'https://n.nordstrommedia.com/id/sr3/4636ed12-7c5d-4fd3-a157-bcadeba08b95.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
			'https://n.nordstrommedia.com/id/sr3/2ae7112f-dba6-4a11-ba20-6dd2bbcf9d8e.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
			'https://n.nordstrommedia.com/id/sr3/1b9ecaa1-d328-439e-9a32-abdb6abc6113.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
		],
		pairwith: [
			{
				id: '1bc109eb-8f88-463e-978e-f1cad7c52e97',
				url: '/static/data/women/tops/1bc109eb-8f88-463e-978e-f1cad7c52e97/image.jpg'
			},
			{
				id: '4838e59a-6bb8-4242-b2ea-7318adc83490',
				url: '/static/data/women/tops/4838e59a-6bb8-4242-b2ea-7318adc83490/image.jpg'
			},
			{
				id: '46a4f18f-a647-4233-83b8-4b3e795682be',
				url: '/static/data/women/tops/46a4f18f-a647-4233-83b8-4b3e795682be/image.jpg'
			},
			{
				id: '770a764d-01f6-4b7c-9a00-f09abd74531e',
				url: '/static/data/women/tops/770a764d-01f6-4b7c-9a00-f09abd74531e/image.jpg'
			}
		]
	}
];

export const womens_topsNew = [
	{
		id: '2359334d-6693-4158-a791-3cf11f1c7e66',
		img_url: '/static/data/women/tops/2359334d-6693-4158-a791-3cf11f1c7e66/image.jpg',
		apparel_name: 'Peach Top',
		apparel_price: '23',
		pair: [ 'white shorts', 'denim shorts', 'black skirt', 'denim pant' ],
		similar: [ 'cream full sleve', 'cream half sleve', 'cream tee', 'cream full sleve' ],
		similars: [
			'https://n.nordstrommedia.com/id/sr3/c981cf77-81f5-4a5a-9593-db7c963fcdb4.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
			'https://n.nordstrommedia.com/id/sr3/c6149556-8e41-4de5-a99c-04dbbcfc5520.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
			'https://n.nordstrommedia.com/id/sr3/f5ecd184-c7b8-411e-a448-3884a62055ec.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
			'https://n.nordstrommedia.com/id/sr3/22aaf571-7b8d-4a45-9bbe-00ac735ee5ca.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
		],
		pairwith: [
			{
				id: 'cc082b52-8e07-4566-85d1-55c932e798bb',
				url: '/static/data/women/bottoms/cc082b52-8e07-4566-85d1-55c932e798bb/image.jpg'
			},
			{
				id: 'a0d3337c-1be2-4177-b9c2-1620f0271697',
				url: '/static/data/women/bottoms/a0d3337c-1be2-4177-b9c2-1620f0271697/image.jpg'
			},
			{
				id: '47719dd3-df6c-4e69-b9b3-9d9d88fd7680',
				url: '/static/data/women/bottoms/47719dd3-df6c-4e69-b9b3-9d9d88fd7680/image.jpg'
			},
			{
				id: '3f10b06e-e104-4304-95fd-9fff1fdef89c',
				url: '/static/data/women/bottoms/3f10b06e-e104-4304-95fd-9fff1fdef89c/image.jpg'
			}
		]
	},

	{
		id: '71d12ff0-c70f-41f5-af9c-19d7f24b6050',
		img_url: '/static/data/women/tops/71d12ff0-c70f-41f5-af9c-19d7f24b6050/image.jpg',
		apparel_name: 'Black Top',
		apparel_price: '46',
		pair: [ 'black shorts', 'black skirt', 'denim pant', 'black pant' ],
		similar: [ 'black top', 'black half sleeve', 'black tee', 'black dress' ],
		similars: [
			'https://n.nordstrommedia.com/id/sr3/72cb366f-368e-424e-ae39-fb9405101f06.jpeg?w=205',
			'https://n.nordstrommedia.com/id/sr3/315a70b1-4d9c-4ac3-8569-f562f6dde473.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
			'https://n.nordstrommedia.com/id/sr3/b0e2c4f0-aa32-4d14-99a8-e7a18ad971eb.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
			'https://n.nordstrommedia.com/id/sr3/2da76041-8fcf-484b-93d1-773ebdb76efa.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
		],
		pairwith: [
			{
				id: 'a1c59060-a459-4537-b404-5cc405d1c0c0',
				url: '/static/data/women/bottoms/a1c59060-a459-4537-b404-5cc405d1c0c0/image.jpg'
			},
			{
				id: '3f10b06e-e104-4304-95fd-9fff1fdef89c',
				url: '/static/data/women/bottoms/3f10b06e-e104-4304-95fd-9fff1fdef89c/image.jpg'
			},
			{
				id: 'f0909e54-f621-411c-bda1-4fa92bb26c0c',
				url: '/static/data/women/bottoms/f0909e54-f621-411c-bda1-4fa92bb26c0c/image.jpg'
			},
			{
				id: 'f52edd7a-1666-4efb-abcd-3e4fd559dae2',
				url: '/static/data/women/bottoms/f52edd7a-1666-4efb-abcd-3e4fd559dae2/image.jpg'
			}
		]
	},
	{
		id: '1bc109eb-8f88-463e-978e-f1cad7c52e97',
		img_url: '/static/data/women/tops/1bc109eb-8f88-463e-978e-f1cad7c52e97/image.jpg',
		apparel_name: 'dotted Top',
		apparel_price: '55',
		pair: [ 'denim shorts', 'black skirt', 'denim pant', 'black pant' ],
		similar: [ 'pink top', 'pink half sleeve', 'dotted tee', 'dotted full sleeve' ],
		similars: [
			'https://n.nordstrommedia.com/id/sr3/fd204308-7d2d-4aa6-b936-63eeb111169d.jpeg?w=205',
			'https://n.nordstrommedia.com/id/sr3/750eb411-c82b-4383-9ba0-c71875da4806.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
			'https://n.nordstrommedia.com/id/sr3/3281f35e-5c24-4b99-9671-02bc11274fa4.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
			'https://n.nordstrommedia.com/id/sr3/f5646e13-6cf3-44d0-9d05-150b6574e0e2.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
		],
		pairwith: [
			{
				id: '88431eb6-3985-4438-a1b4-5cc82b4b8265',
				url: '/static/data/women/bottoms/88431eb6-3985-4438-a1b4-5cc82b4b8265/image.jpg'
			},
			{
				id: '47719dd3-df6c-4e69-b9b3-9d9d88fd7680',
				url: '/static/data/women/bottoms/47719dd3-df6c-4e69-b9b3-9d9d88fd7680/image.jpg'
			},
			{
				id: '9e54ae68-2987-43d8-b811-b52b7fa95c46',
				url: '/static/data/women/bottoms/9e54ae68-2987-43d8-b811-b52b7fa95c46/image.jpg'
			},
			{
				id: 'f0909e54-f621-411c-bda1-4fa92bb26c0c',
				url: '/static/data/women/bottoms/f0909e54-f621-411c-bda1-4fa92bb26c0c/image.jpg'
			}
		]
	},
	{
		id: '4838e59a-6bb8-4242-b2ea-7318adc83490',
		img_url: '/static/data/women/tops/4838e59a-6bb8-4242-b2ea-7318adc83490/image.jpg',
		apparel_name: 'Black Top',
		apparel_price: '33',
		pair: [ 'denim shorts', 'denim pant', 'white pant', 'black pant' ],
		similar: [ 'black tee', 'black tee', 'black tee', 'black v-neck' ],
		similars: [
			'https://n.nordstrommedia.com/id/sr3/51165a0b-0a52-422b-bb3e-f8f3a9906469.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
			'https://n.nordstrommedia.com/id/sr3/c7629ebc-a077-4cf4-b8e0-6e9daeb80c6e.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
			'https://n.nordstrommedia.com/id/sr3/4a4bb25e-9d6c-4a9c-ba9b-e9c6857e3fb2.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2',
			'https://n.nordstrommedia.com/id/sr3/1215f670-f8ef-458e-9123-421ec67b432d.jpeg?crop=pad&pad_color=FFF&format=jpeg&w=780&h=1196&dpr=2'
		],
		pairwith: [
			{
				id: '88431eb6-3985-4438-a1b4-5cc82b4b8265',
				url: '/static/data/women/bottoms/88431eb6-3985-4438-a1b4-5cc82b4b8265/image.jpg'
			},
			{
				id: '9e54ae68-2987-43d8-b811-b52b7fa95c46',
				url: '/static/data/women/bottoms/9e54ae68-2987-43d8-b811-b52b7fa95c46/image.jpg'
			},
			{
				id: 'a78c55cd-b12c-432c-a69f-d39ab847f841',
				url: '/static/data/women/bottoms/a78c55cd-b12c-432c-a69f-d39ab847f841/image.jpg'
			},
			{
				id: 'f0909e54-f621-411c-bda1-4fa92bb26c0c',
				url: '/static/data/women/bottoms/f0909e54-f621-411c-bda1-4fa92bb26c0c/image.jpg'
			}
		]
	}
];

export const women_targetsNew = [
	{
		id: '1',
		img_url: '/static/data/women/targets/1/avatar.jpg'
	},
	{
		id: '2',
		img_url: '/static/data/women/targets/2/avatar.jpg'
	},
	{
		id: '3',
		img_url: '/static/data/women/targets/3/avatar.jpg'
	},
	{
		id: '4',
		img_url: '/static/data/women/targets/4/avatar.jpg'
	},
	{
		id: '5',
		img_url: '/static/data/women/targets/5/avatar.jpg'
	}
];
