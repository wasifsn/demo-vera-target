# Virtual try-on


## Install 
```
pip install fastapi
pip install aiofiles 
pip install uvicorn
pip install pillow
pip install toml
```

## Launch by
`uvicorn main:app --reload`
or 
`run.sh`
## Urls
http://127.0.0.1:8000/
http://127.0.0.1:8000/docs for api documentation



# Original Virtual Tryon app

To launch the original virtual tryon with apis that dont use graph.json for similars.
`uvicorn original:app`
or 
`run_original.sh`