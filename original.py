from fastapi import FastAPI, Response, Depends, HTTPException, status
from fastapi.staticfiles import StaticFiles
from starlette.responses import StreamingResponse, HTMLResponse
from fastapi.templating import Jinja2Templates
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from glob import glob
from PIL import Image
import os
from io import BytesIO
import secrets

import aiofiles

app = FastAPI()
app.mount("/static", StaticFiles(directory="static"), name="static")
security = HTTPBasic()


def get_current_username(credentials: HTTPBasicCredentials = Depends(security)):
    correct_username = secrets.compare_digest(credentials.username, "vera")
    correct_password = secrets.compare_digest(credentials.password, "swordfish")
    if not (correct_username and correct_password):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect email or password",
            headers={"WWW-Authenticate": "Basic"},
        )
    return credentials.username

@app.get("/", response_class=HTMLResponse)
async def send_root(username: str = Depends(get_current_username)):
    async with aiofiles.open('original.html', mode='r') as f:
        html_content = await f.read()
    return HTMLResponse(content=html_content, status_code=200)
 

@app.get("/{sex}/tops")
async def list_tops(sex: str, username: str = Depends(get_current_username)):
    tops = glob(f'./static/data/{sex}/tops/*/')
    tops = sorted([os.path.dirname(top).split('/')[-1] for top in tops])
    return [{'id': top, 'img_url': os.path.join('/static/data/', sex, 'tops', top, 'image.jpg')}for top in tops]


@app.get("/{sex}/bottoms")
async def list_bottoms(sex: str, username: str = Depends(get_current_username)):
    bottoms = glob(f'./static/data/{sex}/bottoms/*/')
    bottoms = sorted([os.path.dirname(btm).split('/')[-1] for btm in bottoms])
    return [{'id': btm, 'img_url': os.path.join('/static/data/', sex, 'bottoms', btm, 'image.jpg')}for btm in bottoms]


@app.get("/{sex}/targets")
async def list_targets(sex: str, username: str = Depends(get_current_username)):
    targets = glob(f'./static/data/{sex}/targets/*/')
    targets = sorted([os.path.dirname(tgt).split('/')[-1] for tgt in targets])
    return [{'id': tgt, 'img_url': os.path.join('/static/data/', sex, 'targets', tgt, 'avatar.jpg')}for tgt in targets]


@app.get("/{sex}/try")
def try_on(sex: str, top='1', bottom='1', target='1',username: str = Depends(get_current_username)):
    rendered = Image.open(os.path.join('./static/data', sex, 'targets', target, 'image.jpg'))
    target_arm_im = Image.open(os.path.join('./static/data', sex, 'targets', target,'arm_mask.png'))
    rendered.thumbnail((600,600))
    target_arm_im.thumbnail((600,600))
    warped_torso = Image.open(os.path.join('./static/data',sex, 'tops', top, target, 'warped_torso.png'))
    warped_arms = Image.open(os.path.join(f'./static/data', sex, 'tops', top, target, 'warped_arms.png'))
    warped_bottom = Image.open(os.path.join(f'./static/data',sex,'bottoms', bottom, target, 'warped_bottom.png'))
    rendered.thumbnail((600,600))
    target_arm_im.thumbnail((600,600))
    
    rendered.paste(warped_bottom, warped_bottom)
    rendered.paste(target_arm_im, target_arm_im)
    rendered.paste(warped_torso, warped_torso)

    rendered.paste(warped_arms, warped_arms)

    buffer = BytesIO()
    rendered.convert('RGB').save(buffer, 'JPEG', optimize=True,quality=90)
    buffer.seek(0)
    return StreamingResponse(buffer, media_type="image/jpeg")
